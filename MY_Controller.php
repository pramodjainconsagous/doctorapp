<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller {

   
	function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,version,lang,user_id,token");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		parent::__construct();
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS")
		{
			die();
		}
		
		$this->load->helper('language');
		
		if($_SERVER['HTTP_HOST'] == 'localhost'){
			error_reporting(E_ALL);
		}else{
			error_reporting(0);	
		}
        
       /* $header = getallheaders();
      	$lang =  @$header['lang'] ?  @$header['lang'] : 'english';*/
      	$lang =  @$_GET['lang'] ?  @$_GET['lang'] : 'english';
      	$lang = strtolower($lang);
		if($lang == 'english'){
			$this->lang->load('api','english');	
		}else{
			$this->lang->load('api','hindi');	
		}
	}

/**
* token check
*/
function token($user_id,$token,$logout=NULL){

	/*$header  = getallheaders();
	$user_id = $header['user_id'];
	$token   = $header['token'];*/


	if(empty($user_id)){
		$manage = array('status' => 0, 'error' => __LINE__,
			'message' => $this->lang->line('validation_user_id'));
		echo json_encode($manage); die;
	}else if(empty($token)){
		$manage = array('status' => 0, 'error' => __LINE__,
			'message' => $this->lang->line('validation_token'));
		echo json_encode($manage); die;
	}

	if(!empty($user_id) && !empty($token)){
		$where = " where user_id = '".$user_id."' && token = '".$token."'";
		$result = $this->Common->select('user_token',$where);
		if($result){
			$data_token = array('token' => $token,
				'user_id' => $user_id);


			if(!empty($logout)){

				$this->Common->delete('user_token', $user_id, 'user_id');
				$message = array('status' => 1, 
					             'message' => $this->lang->line('successfully').' logout');	
				return $message;

			}else{
				return true;
			}


		}else{
			$message = array('status' => 0, 'error' => 461, 'message' => $this->lang->line('validation_token_user_id'));
			echo json_encode($message); die;
		}
	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => $this->lang->line('validation_send_token_user_id'));
		echo json_encode($message); die;
	}
}
/* end token */

/**
* device token check
*/
function device_token_check($device_token=NULL){

	if(!empty($device_token)){
		$where = " where device_token = '".$device_token."'";
		$result = $this->Common->select('user',$where);
		if($result){

			$insertData = array('device_token' => '');

			$where = array('device_token' => $device_token);
			$id =  $this->Common->update('user', $insertData, $where);


		}
	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send device token.');
		echo json_encode($message); die;
	}
	return true;
}
/* end token */

/** 
* app cersion check
*/
function version_check(){
	/*$header = getallheaders();
	$version_code = $header['version'];*/
	$version_code =  @$_GET['version'] ?  @$_GET['version'] : '1';
	$api_version =  config_item('api_version'); 
	if($version_code != $api_version){
		$message = array('status' => 0, 'error' => 460, "message" => $this->lang->line('update_version'));
		echo json_encode($message); die;
	}
}

/**
*is_numeric check validation
*/
function validation_numeric($str){

	if(is_numeric($str)){ 
		return true; 
	}else{ 
		$message = array('status' => 0, 'error' => __LINE__, "message" => "mobile number only accept numeric value.");
		echo json_encode($message); die;
	}

}

/* end*/
}