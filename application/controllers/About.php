<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	var $user_id = '';
    public function __construct() {
    	parent::__construct();
		//error_reporting(E_ALL);
    	check_user_login();
		
		$this->user_id = $this->session->userdata('user_id');
	   // $this->output->enable_profiler(TRUE);	
        
	}

  
	public function index()
	{ 
	   $data['title'] = 'About';
	   $data['result'] = $this->Common->select('about');

	   $this->load->view('about/about',$data);
	}


	function editAbout(){

		$id = $this->uri->segment(3);
		$data['id'] = $id;
		$id = safe_b64decode($id);
		$data['title'] = 'About';
		$post = $this->input->post();
		if($post && !empty($post))
		{

		$target_dir = $this->config->item('root_path');   
		$imgName = '';
		if($_FILES){
		  $target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
		  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		  $imgName = time().'.'.$imageFileType;
		  $target_file = $target_dir . $imgName;
		  move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
		}  


		 	$data = array('name' => $post['name'],
		 			   'click_name' => $post['click_name'],
					   'about_us' => $post['about_us'],
					   'picture_url' => $imgName,
					   );

		 	$where = array('id'=>$id);
			if($this->Common->update('about', $data, $where))
			{
				$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
		 	}
		 	else
		 	{
				$this->session->set_flashdata('error', UPDATE_FAIL);
		 	}
		}
		$data['result'] = $this->Common->select('about');
		$this->load->view('about/edit_about',$data);
	}
    
	
  
	
}