<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	var $user_id = '';
    public function __construct() {
    	parent::__construct();
		$this->load->helper(array());
		$this->user_id = $this->session->userdata('user_id');
	    //$this->output->enable_profiler(TRUE);	
    }

 /**
   * get item 
   */
	public function get_state()
	{ 
	$id = $this->input->post('id');
	$where = " where countryid = $id";
	   $result= $this->Common->select('country_regions',$where);
	   $dopt = '<option value="">Select item</option>';
	   if(!empty($result)){
	   foreach($result as $states){
		  $sel = '';
		  
	     $dopt .= '<option '.$sel.'  value="'.$states['regionid'].'">'.ucwords($states['region']).'</option>';
	    }
	  }
	   echo $dopt;
	}	
	


	public function search_patient()
	{ 
	$mobile_no = $this->input->post('mobile_no');
	$where = " where  phone_number = '".$mobile_no."' &&  user_role = 3";
	$result= $this->Common->select('user',$where);
		if(!empty($result)){
			$img = image_check($result[0]['picture_url'],USER_IMAGE);
			$data = array('patient_id' => $result[0]['id'],
						  'patient_name' => ucwords($result[0]['full_name']),
						  'patient_phone_number' => $result[0]['phone_number'],
						  'patient_picture_url' => $img,
						 );
        	$message = array('status' => 1, 'result' => $data);		
		}else{
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Not found');
	    }
	echo json_encode($message); die;	
	}
	

	public function addNewPatient(){
     

    $name = $this->input->post('name');
    $mobile_no = $this->input->post('mobile_no');

    if(empty($name)){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please fill patient name');
			echo json_encode($message); die;
	}

	if(strlen($mobile_no) < 10){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => MIN_MOBILE_LENGTH);
			echo json_encode($message); die;
	}
        

    if(strlen($mobile_no) > 15){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_MOBILE);
			echo json_encode($message); die;
	}
        

	$where = " where  phone_number = '".$mobile_no."'";
	$result= $this->Common->select('user',$where);
    if(empty($result)){
    	    $token = mt_rand();
    	    $time = time();
            $insertData = array('user_role' => 3,
	                            'full_name' => $name,
	                            'phone_number' => $mobile_no,
	                            'password' => $token,
	                            'notifications' => 1,
	                            'activation_code' => $token, 
	                            'status' => 'Active',
	                            'create_dt' => $time,
	                            'update_dt' => $time,
	                            );
     
     $id =  $this->Common->insert('user', $insertData);
     $this->sendOpt($mobile_no, $token);
     $message = array('status' => 1, 'error' => __LINE__, 'message' => 'succesfully create');
           
    }else{
    	 $message = array('status' => 0, 'error' => __LINE__, 'message' => 'This mobile number already exits.');
    }
     echo json_encode($message); die;	
	}
	

/**
* send opt
*/
private function sendOpt($phone,$otp, $case=NULL){
	return true;
	if(!empty($phone) && !empty($otp)){
	    $ch = curl_init();
    	
    	$message = 'Your doctor app password is : ';
    	$message = rawurlencode(utf8_encode($message));
    	 $url="https://control.msg91.com/api/sendhttp.php?authkey=167017AeiaeT42JEr659783c0c&mobiles=".$phone."&message=".$message.' '.$otp."&sender=DoctorApp&route=4";

	    curl_setopt($ch, CURLOPT_URL,$url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HEADER, 0);  
	    $output=curl_exec($ch);
	    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);
	    if($httpcode=="200"){
	        return true;
	    }else{
	        return false;
	        }
	}else{
	    return false;
	}
}


	
}