<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	function __construct(){
		parent::__construct();
		error_reporting(0);
		if($this->input->server('REQUEST_METHOD') != 'POST'){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'invalid http request method');
			echo json_encode($message); die;
		} 


	}

/**
* token check
*/
private function token($logout=NULL){

	$header = getallheaders();
	$user_id = $header['user_id'];
	$token = $header['token'];


	if(empty($user_id)){
		$manage = array('status' => 0, 'error' => __LINE__,
			'message' => 'Please send user id');
		echo json_encode($manage); die;
	}else if(empty($token)){
		$manage = array('status' => 0, 'error' => __LINE__,
			'message' => 'Please send token no.');
		echo json_encode($manage); die;
	}

	if(!empty($user_id) && !empty($token)){
		$where = " where user_id = '".$user_id."' && token = '".$token."'";
		$result = $this->Common->select('user_token',$where);
		if($result){
			$data_token = array('token' => $token,
				'user_id' => $user_id);


			if(!empty($logout)){
                
                $insertData = array('device_token' => '');
			    $where = array('id' => $user_id);
			    $this->Common->update('user', $insertData, $where);

				$this->Common->delete('user_token', $user_id, 'user_id');
				$message = array('status' => 1, 'message' => SUCCESSFULLY.' logout');	
				return $message;

			}else{
				return true;
			}


		}else{
			$message = array('status' => 0, 'error' => 461, 'message' => 'Your session expire, please login again.');
			echo json_encode($message); die;
		}
	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send valid user id && token no.');
		echo json_encode($message); die;
	}
}
/* end token */

/**
* device token check
*/
private function device_token_check($device_token=NULL){

	if(!empty($device_token)){
		$where = " where device_token = '".$device_token."'";
		$result = $this->Common->select('user',$where);
		if($result){

			$insertData = array('device_token' => '');

			$where = array('device_token' => $device_token);
			$id =  $this->Common->update('user', $insertData, $where);


		}
	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send device token.');
		echo json_encode($message); die;
	}
	return true;
}
/* end token */

/** 
* app cersion check
*/
private function version_check(){
	$header = getallheaders();
	$version_code = $header['version'];
	$api_version =  config_item('api_version'); 
	if($version_code != $api_version){
		$message = array('status' => 0, 'error' => 460, "message" => "please update your application with a new version available.");
		echo json_encode($message); die;
	}
}

/**
*is_numeric check validation
*/
private function validation_numeric($str){

	if(is_numeric($str)){ 
		return true; 
	}else{ 
		$message = array('status' => 0, 'error' => __LINE__, "message" => "mobile number only accept numeric value.");
		echo json_encode($message); die;
	}

}

/**
* login
*/
public function login(){ 


	$this->version_check();
	$post = file_get_contents('php://input');
	if($post){
		$post = json_decode($post, TRUE);
		$phone_number = $post['phone_number'] ? $post['phone_number'] : '';
		$password = $post['password'] ? $post['password'] : '';
		$device_token = $post['device_token'] ? $post['device_token'] : '';
		$device_type = $post['device_type'] ? $post['device_type'] : '';
		
		if(strlen($password) < 5){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => MIN_PSW_LENGTH);
			echo json_encode($message); die;
		}

		if(strlen($phone_number) < 10){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => MIN_MOBILE_LENGTH);
			echo json_encode($message); die;
		}


		if(strlen($phone_number) > 12){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_MOBILE);
			echo json_encode($message); die;
		}

		$this->validation_numeric($phone_number);

		/*if(empty($device_token)){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send device token');
			echo json_encode($message); die;
		}*/


		if(!empty($phone_number) && !empty($password)){


			$where = " where phone_number = '".$phone_number."' && password = '".$password."'";
			$result = $this->Common->select('user',$where);
			if($result){

				if($result[0]['status'] == 'Active'){

					//$this->device_token_check($device_token);
					$picture_url = image_check($result[0]['picture_url'],USER_IMAGE, 'user');
					$token = mt_rand();
					$data_token = array('token' => $token,
										'user_id' => $result[0]['id']);
					$this->Common->delete('user_token', $result[0]['id'], 'user_id');
					$this->Common->insert('user_token', $data_token);


					$insertData = array('device_token' => $device_token,
						'device_type' => $device_type);

					$where = array('id' => $result[0]['id']);
					$this->Common->update('user', $insertData, $where);



					$data = array('token' => "$token",
						'user_id' => $result[0]['id'],
						'full_name' => $result[0]['full_name'],
						'phone_number' => $result[0]['phone_number'],
						'email_address' => $result[0]['email_address'],
						'img_name' => $result[0]['picture_url'],
						'device_token' => $device_token,
						'picture_url' => $picture_url);	
					$message = array('status' => 1, 'message' => SUCCESSFULLY, 'result' => $data);	
				}else{
					/* mail send */

					/* end mail */
					$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please verify your phone number after that you can login.');	
				}		
			}else{
				$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Sorry, incorrect phone no or password');
			}
		}else{
			$message = array('status' => 0, 'error' => __LINE__, 'message' => PLEASE_FILL_VALID_FIELDS);
		}
	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
	}
	echo json_encode($message); die;
}
/* end login */

/**
* mobile opt send
*/
public function mobile_verification(){
	$this->version_check();
	$post = file_get_contents('php://input');
	if($post){
		$post = json_decode($post, TRUE);
		$blanckArray = array("phone_number" => "");
		$post = array_merge($blanckArray,$post);


		$phone_number = $post['phone_number'];

		if(strlen($phone_number) < 10){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => MIN_MOBILE_LENGTH);
			echo json_encode($message); die;
		}


		if(strlen($phone_number) > 12){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_MOBILE);
			echo json_encode($message); die;
		}

		$this->validation_numeric($phone_number);

		$where = " where phone_number = '".$post['phone_number']."'"; 
		$result = $this->Common->select('user',$where);
		if(empty($result)){
			$activation_code = mt_rand(100000, 999999);
			$activation_code = '123456';
			$insertData = array('phone_number' => $post['phone_number'],
				'activation_code' => $activation_code,   	
				'user_role' => 3,
				'status' => 'inActive',
				'create_dt' => time()
			);

			$this->sendOpt($post['phone_number'], $activation_code);
			$id =  $this->Common->insert('user', $insertData);
			$message = array('status'=> 1, 'user_id' => "$id", 'otp' => "$activation_code", 'message'=> 'successfully send OTP.'); 

		}else{

			if($result[0]['status'] == 'inActive'){

				$activation_code = $result[0]['activation_code'];
				$this->sendOpt($result[0]['phone_number'], $activation_code);
				$id =  $result[0]['id'];
				$message = array('status'=> 1, 'user_id' => "$id", 'otp' => "$activation_code", 'message'=> 'again successfully send OTP.'); 

			}else{

				$message = array('status'=> 0,  'error' => __LINE__, 'message'=> MOBILE_ERROR_MSG); 	
			}  
		}
	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
	}
	echo json_encode($message); die;	
}

/**
* send opt
*/
private function sendOpt($phone,$otp, $case=NULL){
	//return true;
	if(!empty($phone) && !empty($otp)){

		$ch = curl_init();


		$url="https://control.msg91.com/api/sendhttp.php?authkey=167017AeiaeT42JEr659783c0c&mobiles=".$phone."&message=Your%20DoctorApp%20OTP%20is%20".$otp."&sender=DoctorApp&route=4";

		if($case == 'forget_password'){
			$message = 'Your new password is : ';
			$message = rawurlencode(utf8_encode($message));
			$url="https://control.msg91.com/api/sendhttp.php?authkey=167017AeiaeT42JEr659783c0c&mobiles=".$phone."&message=".$message.' '.$otp."&sender=DoctorApp&route=4";
		}

		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);  
		$output=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		//dd($output);
		if($httpcode=="200"){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}

/**
* registration
*/
public function registration(){ 

	$this->version_check();
	$post = file_get_contents('php://input');
	if($post){
		$post = json_decode($post, TRUE);

		$blanckArray = array("full_name" => "",
			'email_address' => '',
			'password' => '',
			'user_id' => '',
			'device_token' => '',
			'device_type' => ''
		);
		$post = array_merge($blanckArray,$post);

		if(empty($post['user_id'])){
			$manage = array('status' => 0,
				'message' => 'Please enter valid user id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}



		$password = $post['password'];
		$device_token = $post['device_token'] ? $post['device_token'] : '';
		$device_type = $post['device_type'] ? $post['device_type'] : '';

		if(empty($post['full_name'])){
			$message = array('status'=> 0,  'error' => __LINE__, 'message'=> 'Please enter name'); 
			echo json_encode($message); die;	
		}

		if(strlen($password) < 5){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => MIN_PSW_LENGTH);
			echo json_encode($message); die;
		}


		if(strlen($password) > 40){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Password is too long');
			echo json_encode($message); die;
		}


		if(!empty($post['email_address'])){
			if (!filter_var($post['email_address'], FILTER_VALIDATE_EMAIL)) {
				$message = array('status'=> 0,  'error' => __LINE__, 'message'=> WRONG_EMAIL); 
				echo json_encode($message); die;	
			}
		}


		if(empty($device_token)){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send device token');
			echo json_encode($message); die;
		}

		$this->device_token_check($device_token);


		$where = " where email_address = '".$post['email_address']."'"; 
		$result = $this->Common->select('user',$where);
		if(empty($result)){

			$insertData = array('full_name' => $post['full_name'],
				'password' => $post['password'],
				'email_address' => $post['email_address'],
				'device_token' => $post['device_token'],
				'device_type' => $post['device_type'],
				'status' => 'Active',
			);

			$where = array('id' => $post['user_id']);
			$id =  $this->Common->update('user', $insertData, $where);

			/* mail send */

			/* end mail */


			$token = mt_rand();

			$data_token = array('token' => $token,
				'user_id' => $post['user_id']);
			$this->Common->delete('user_token', $post['user_id'], 'user_id');
			$this->Common->insert('user_token', $data_token);


			$where = " where id = '".$post['user_id']."'"; 
			$result = $this->Common->select('user',$where);

			$picture_url = image_check($result[0]['picture_url'],USER_IMAGE,'user');

			$data = array('token' => "$token",
				'user_id' => $post['user_id'],
				'full_name' => $post['full_name'],
				'phone_number' => $post['phone_number'],
				'email_address' => $post['email_address'],
				'device_token' => $post['device_token'],
				'device_type' => $post['device_type'],
				'picture_url' => $picture_url);	

			$message = array('status'=> 1, 'result' => $data, 'message'=> 'Thank you. You have been registered to DoctorApp.'); 
		}else{
			$message = array('status'=> 0,  'error' => __LINE__, 'message'=> EMAIL_ERROR_MSG); 
		}

	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
	}
	echo json_encode($message); die;	
}
/* end registration */

/**
* logout  
*/
public function logout(){

	$this->version_check();
	$st =  $this->token('logout');
	echo json_encode($st); die;	
}
/* end */

/**
*  opt verify
*/
public function otp_verify(){
	$this->version_check();
	$post = file_get_contents('php://input');
	if($post){
		$post = json_decode($post, TRUE);
		$phone_number = $post['phone_number'];
		$otp = $post['otp'];


		if(empty($otp)){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send otp.');
			echo json_encode($message); die;
		}

		if(strlen($phone_number) < 10){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => MIN_MOBILE_LENGTH);
			echo json_encode($message); die;
		}


		if(strlen($phone_number) > 12){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_MOBILE);
			echo json_encode($message); die;
		}

		$this->validation_numeric($phone_number);

		$where = "where phone_number = '".$phone_number."' && activation_code = '".$otp."'";
		$result = $this->Common->select('user',$where);
		
		if(!empty($result)){
			$id = $result[0]['id'];
			$this->Common->update('user',array('activation_code' => '','status'=> 'Active'),array('id' => $id));
			$manage = array('status' => 1,'message'=>'Otp successfully changes.');
		}else{
			$manage = array('status' => 0, 'error' => __LINE__, 'message'=>'Oops!! This mobile no and otp is not registered with us');
		}
	}else{
		$manage = array('status' => 0, 'error' => __LINE__, 'message'=>'Please send require parameter.');
	}
	echo json_encode($manage);
	exit;
}
/* end opt verify */

/**
* forget password
*/
public function forget_password(){
	$this->version_check();
	$post = file_get_contents('php://input');
	if($post){
		$post = json_decode($post, TRUE);
		$phone_number = $post['phone_number'];


		if(strlen($phone_number) < 10){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => MIN_MOBILE_LENGTH);
			echo json_encode($message); die;
		}


		if(strlen($phone_number) > 12){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_MOBILE);
			echo json_encode($message); die;
		}

		$this->validation_numeric($phone_number);

		$where = "where phone_number = '".$phone_number."'";
		$result = $this->Common->select('user',$where);
		
		if(!empty($result)){

			if($result[0]['status'] == 'Active'){
				$id = $result[0]['id'];
				$password = mt_rand(100000, 999999);

				$update_data = array('password' => $password);
				$this->Common->update('user',array('password'=>$password),array('id' => $id));
				$this->sendOpt($result[0]['phone_number'],$password,'forget_password');
				$manage = array('status' => 1,'message'=>'Thank you ! your password has been sent to registered mobile no');
				echo json_encode($manage);
				exit;
			}else{
				$manage = array('status' => 0, 'error' => __LINE__, 'message'=>'Your account is not Active.');
			}
		}else{
			$manage = array('status' => 0, 'error' => __LINE__, 'message'=>'Oops!! This mobile no is not registered with us');
		}
	}else{
		$manage = array('status' => 0, 'error' => __LINE__, 'message'=>'Please send require parameter.');
	}
	echo json_encode($manage);
	exit;
}
/*	forget password end */

/**
* user profile
*/
public function profile(){

	$this->version_check();
	$this->token();

	$header = getallheaders();
	$user_id = $header['user_id'];

	if($user_id){
		if(empty($user_id)){
			$manage = array('status' => 0,
				'message' => 'Please enter valid user id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}
		$where = " WHERE id = ".$user_id;
		$result = $this->Common->select('user',$where);
		
		if(!empty($result)){
			$picture_url = image_check($result[0]['picture_url'],USER_IMAGE,'user');
			$data = array('user_id' => $result[0]['id'],
				'full_name' => $result[0]['full_name'],
				'phone_number' => $result[0]['phone_number'],
				'email_address' => $result[0]['email_address'],
				'picture_url' => $picture_url);	
			$manage = array('status' => 1, 'message' => SUCCESSFULLY, 'result' => $data);
		}else{			
			$manage = array('status' => 0,'message' => 'No record found for this user');
		}
	}else{
		$manage = array('status' => 0, 'error' => __LINE__, 'message'=>'Please send require parameter.');
	}
	$rs =  json_encode($manage); 
	print_r($rs);die; 
}
/* user info end */

/**
* profile update
*/
public function profile_update(){
	$post = $this->input->post();
	if($post){

		$blanckArray = array('user_id' => '',
			'full_name' => '',
			'email_address' => '',
			'password' => '',
		);
		$post = array_merge($blanckArray,$post);
		$user_id = $post['user_id'];

		if(empty($user_id)){
			$manage = array('status' => 0, 'error' => __LINE__,
				'message' => 'Please send user id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}
	
		$where = " where id = '".$user_id."'";
		$result = $this->Common->select('user',$where);
		if(!$result){
			$manage = array('status' => 0, 'error' => __LINE__,
				'message' => 'user id not exist');
			$rs =  json_encode($manage); 
			print_r($rs);die; 		
		}

		$target_dir = $this->config->item('root_path');   
		$imgName = '';
		if($_FILES){
			if(!empty($_FILES["profile_image"]["name"])){
				$target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				$imgName = time().'.'.$imageFileType;
				$target_file = $target_dir . $imgName;
				@unlink($target_dir.$result[0]['picture_url']);
				move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
			}

		}  
		$activation_code = mt_rand();
		$insertData = array('full_name' => $post['full_name'],
							'email_address' => $post['email_address'],
							'update_dt' => time()
		);
		
		$password = $post['password'];
        if(!empty($password)){
			$array3 = array('password' => $password);
			$insertData = array_merge($insertData, $array3);
		}

		if(!empty($imgName)){
			$array2 = array('picture_url' => $imgName);
			$insertData = array_merge($insertData, $array2);
		}
    
		$where = array('id' => $user_id);
		$id =  $this->Common->update('user', $insertData, $where);

		$picture_url = image_check($imgName,USER_IMAGE,'user');	
		$newData = array('picture_url' => $picture_url,'user_id' => $post['user_id']);			
		$insertData = array_merge($insertData, $newData);

		$message = array('status'=> 1, 'message'=> 'Your profile has been updated successfully.', 'result' => $insertData); 

	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
	}
	echo json_encode($message); die;	
}
/* end profile */

/**
* change password
*/
public function password_change(){
	$this->version_check();
	$this->token();
	$post = file_get_contents('php://input');
	if($post){
		$post = json_decode($post, TRUE);

		$header = getallheaders();
		$user_id = $header['user_id'];

		$old_password = $post['old_password'] ? $post['old_password'] : '';
		$new_password = $post['new_password'] ? $post['new_password'] : '';

		if(empty($user_id)){
			$manage = array('status' => 0, 'error' => __LINE__,
				'message' => 'Please send user id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}else if(empty($old_password)){
			$manage = array('status' => 0, 'error' => __LINE__,
				'message' => 'Please send old password');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}else if(empty($new_password)){
			$manage = array('status' => 0, 'error' => __LINE__,
				'message' => 'Please send new password');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}

		if(strlen($old_password) < 5){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => MIN_PSW_LENGTH);
			echo json_encode($message); die;
		}

		if(strlen($new_password) < 5){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => MIN_PSW_LENGTH);
			echo json_encode($message); die;
		}

		if(strlen($new_password) > 40){
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Password is too long');
			echo json_encode($message); die;
		}


		if(!empty($old_password) && !empty($new_password) && !empty($user_id)){		    

			$uCheck = $this->db->get_where('user', array('id' => $user_id))->result_array();
			$db_password = $uCheck[0]['password'];

			if($old_password == $db_password)
			{
				$where = array('id'=> $user_id);
				$newPass = array('password'=> $new_password);
				$record = $this->db->update('user', $newPass, $where);
				if(!empty($record)){
					$message = array('status' => 1, 'message' => 'Your Password has been changed successfully!');
				}else{
					$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Password changing faild, Please try again!');
				}
			}else{
				$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Password does not match the old password!');
			}
		}else{
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send user id, old password and new password');
		}
	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
	}
	echo json_encode($message); die;	
}
/* end change password */

/**
* ads 
*/
/*public function ads(){
	$this->version_check();


	$where = " WHERE status = 'Active' order by RAND() limit 1";
	$result = $this->Common->select('ads',$where);

	if(!empty($result)){
		$ads_img = image_check($result[0]['ads_img'],ADS_IMAGE,'ads');
		$data = array('ads_id' => $result[0]['id'],
			'ads_title' => $result[0]['ads_title'],
			'ads_description' => $result[0]['ads_description'],
			'ads_img' => $ads_img,
			'ads_link' => $result[0]['ads_link'],
			'status' => $result[0]['status']
			
		);	
		$manage = array('status' => 1, 'message' => SUCCESSFULLY,'booking_type' => 'AD', 'result' => $data);
	}else{			
		$manage = array('status' => 0,'message' => 'No record found');
	}

	$rs =  json_encode($manage); 
	print_r($rs);die; 
}*/
public function past_ads(){
	/*get  apponitment */
	$this->version_check();
	$header  = getallheaders();
	//$user_id = 117;
	$user_id = $header['user_id'];
	$today               = date('Y-m-d');
	$where               = "WHERE patient_id = $user_id and booking_date <= '".$today."'  order by RAND() limit 1";

	//$where               = "WHERE patient_id = $user_id order by RAND() limit 1";
	$appointments_result = $this->Common->select('appointments',$where);
	/*get doctor id */
	if (!empty($appointments_result))
	{
		$dortor_id =  $appointments_result[0]['clinic_id'];
	}
	
	if (!empty($dortor_id))
	{
		/*doctor_specialties*/
		$getDoctor_specialties     = $this->getDoctor_specialties($dortor_id);
		$getDoctor_specialties     = implode(",",$getDoctor_specialties);
		
		/*ads_associates_doctor specialties*/
		$where                     = "WHERE specialties_id in ($getDoctor_specialties) order by RAND() limit 1";
		$specialties_doctor_result = $this->Common->select('ads_associates_specialties',$where);
		
		/*ads_associates_doctor*/
		$where                    = "WHERE doctor_id = $dortor_id order by RAND() limit 1";
		$associates_doctor_result = $this->Common->select('ads_associates_doctor',$where);
	}

	/*check ads in exits for specialties_doctor */
	if (!empty($specialties_doctor_result))
	{
		$ads_ids = $specialties_doctor_result[0]['ads_id'];
		$where   = "WHERE status = 'Active' and id =$ads_ids order by RAND() limit 1";
		$result  = $this->Common->select('ads',$where);
	}	
	/*check ads in exits for associates_doctor */
	else if (!empty($associates_doctor_result))
	{
		$ads_ids = $associates_doctor_result[0]['ads_id'];
		$where   = "WHERE status = 'Active' and id =$ads_ids order by RAND() limit 1";
		$result  = $this->Common->select('ads',$where);
	}
	else
	{
		////No appointment match
		$sql = "SELECT a.* FROM ads as a LEFT JOIN ads_associates_specialties AS s on a.id = s.ads_id where s.specialties_id = 1 && status = 'Active' group by a.id order by RAND() limit 1";
		$query = $this->db->query($sql);
		$result = $query->result_array();
	}
	

	if(empty($result)){
	//do not have any matching Ads to show
	$sql = "SELECT a.* FROM ads as a LEFT JOIN ads_associates_specialties AS s on a.id = s.ads_id where s.specialties_id = 1 && status = 'Active' group by a.id order by RAND() limit 1";
		$query = $this->db->query($sql);
		$result = $query->result_array();
	}


	if(!empty($result))
	{
		$ads_img = image_check($result[0]['ads_img'],ADS_IMAGE,'ads');
		$data = array(
			'ads_id'          => $result[0]['id'],
			'ads_title'       => $result[0]['ads_title'],
			'ads_description' => $result[0]['ads_description'],
			'ads_img'         => $ads_img,
			'ads_link'        => $result[0]['ads_link'],
			'status'          => $result[0]['status'],
			'height' => 112,
			'pxheight' => 150,
		);	
		$manage = array('status' => 1, 'message' => SUCCESSFULLY,'booking_type' => 'pastBooking','event_type'=>'past_ads','result' => $data);
	}
	else
	{			
		$manage = array('status' => 0,'message' => 'No record found');
	}

	$rs =  json_encode($manage); 
	print_r($rs);die; 
}
/* end ads */
public function live_ads(){ 

$this->version_check();
$header  = getallheaders();
$user_id = $header['user_id'];
//$user_id = 246;

$today               = date('Y-m-d');
$where               = "WHERE patient_id = $user_id and booking_date = '".$today."'  order by RAND() limit 1";
$appointments_result = $this->Common->select('appointments',$where);

/*get doctor id */
if (!empty($appointments_result))
{
	$dortor_id =  $appointments_result[0]['clinic_id'];
}

if(!empty($dortor_id))
{
	/*doctor_specialties*/
	$getDoctor_specialties     = $this->getDoctor_specialties($dortor_id);
	$getDoctor_specialties     = implode(",",$getDoctor_specialties);

	/*ads_associates_doctor specialties*/
	$where                     = "WHERE specialties_id in ($getDoctor_specialties) order by RAND() limit 1";
	$specialties_doctor_result = $this->Common->select('ads_associates_specialties',$where);

}

/*check ads in exits for specialties_doctor */
if (!empty($specialties_doctor_result))
{
	$ads_ids = $specialties_doctor_result[0]['ads_id'];
	$where   = "WHERE status = 'Active' and id =$ads_ids order by RAND() limit 1";
	$result  = $this->Common->select('ads',$where);
}	
else
{  
	//No appointment match
	$sql = "SELECT a.* FROM ads as a LEFT JOIN ads_associates_specialties AS s on a.id = s.ads_id where s.specialties_id = 1 && status = 'Active' group by a.id order by RAND() limit 1";
	$query = $this->db->query($sql);
	$result = $query->result_array();
}

if(empty($result)){
	//do not have any matching Ads to show
$sql = "SELECT a.* FROM ads as a LEFT JOIN ads_associates_specialties AS s on a.id = s.ads_id where s.specialties_id = 1 && status = 'Active' group by a.id order by RAND() limit 1";
	$query = $this->db->query($sql);
	$result = $query->result_array(); //no data found
}

if(!empty($result))
{
$ads_img = image_check($result[0]['ads_img'],ADS_IMAGE,'ads');
$data = array(
'ads_id'          => $result[0]['id'],
'ads_title'       => $result[0]['ads_title'],
'ads_description' => $result[0]['ads_description'],
'ads_img'         => $ads_img,
'ads_link'        => $result[0]['ads_link'],
'status'          => $result[0]['status'],
'height' => 112,
'pxheight' => 150,
);	
$manage = array('status' => 1, 'message' => SUCCESSFULLY,'booking_type' => 'todayBooking','event_type'=>'live_ads','result' => $data);
}
else
{	
$manage = array('status' => 0,'message' => 'No record found');
}

$rs =  json_encode($manage); 
print_r($rs);die; 
}

/**
* Today Appointments
*/
public function today_appointments(){
	// $this->version_check();
	// $this->token();
	$header = getallheaders();
	if($header){
		$user_id = $header['user_id'];
		if(empty($user_id)){
			$manage = array('status' => 0, 'error' => __LINE__,
				'message' => 'Please send user id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}

		$this->db->select('a.*,u.id as clinicId, u.full_name as clinic_name, u.phone_number as clinic_phone_number,u.picture_url as clinic_logo,d.doctor_name,d.doctor_phone_no',false);
		$this->db->from('appointments a');
		$this->db->join('user u', 'u.id = a.clinic_id', 'LEFT');

		$this->db->join('clinic_doctor_management d', 'u.id = d.user_id', 'LEFT');


		
		$this->db->where('a.patient_id', $user_id);
     
    
		$today = date('Y-m-d');
		$this->db->where('a.booking_date >=', $today);
		$this->db->order_by("a.time_slote", "asc");
		
		$query = $this->db->get();	
		if($query->num_rows()>0){
			$k = 0;
			foreach($query->result_array() as $val){ 
                $getBoolen = $this->checkAppointment($val['clinic_id'],$val['patient_id']);
                if(!empty($getBoolen)){
	                $where = " where clinic_id = '".$val['clinic_id']."' && booking_date = '".$today."'";
	               	$total_appointment =  select('appointments',$where);

					
					$img = image_check($val['clinic_logo'],USER_IMAGE, 'user');
	                    
	                    $mt = getAppointmentNumber($user_id, $val['clinic_id']);
	                    $current_number = $mt['current_number'];
	                    $my_number = $mt['my_number'];
	                
	                $room_id = $this->get_room_id($val['clinic_id'], $val['patient_id']);
	                
	                $booking_date = $val['booking_date'];
					$timestamp = strtotime($booking_date);
					$booking_date = date('d-M-Y', $timestamp);

					$data[] = array('clinic_name' => ucwords($val['doctor_name']),
						'clinicName' => ucwords($val['clinic_name']),
						'doctor_name' => ucwords($val['doctor_name']),
						'doctor_phone_no' => ucwords($val['doctor_phone_no']),
						'time_slote' => $val['time_slote'],
						'booking_status' => ucwords($val['status']),
						'clinic_phone_number' => $val['clinic_phone_number'],
						'booking_date' => $booking_date,
						'clinic_logo' => $img,
						'current_number' => "$current_number",
						'my_number' => "$my_number",
						'clinic_id' => $val['clinicId'],
						'chat_room_id' => "$room_id",
						'total_appointment' => "$total_appointment"
						
					);
				
				$k++;	
				}
			}
            if($k == 0){
			$data = array();
			$message = array('status' => 1, 'booking_type' => 'todayBooking', 'message' => 'No live booking.','event_type'=>'today_appointments', 'result' => $data);
            }else{
			$message = array('status'=> 1, 'booking_type' => 'todayBooking', 'message'=> 'live booking.','event_type'=>'today_appointments', 'result' => $data);
            }
		}else{
			$data = array();
			$message = array('status' => 1, 'booking_type' => 'todayBooking', 'message' => 'No live booking.','event_type'=>'today_appointments', 'result' => $data);
		}
	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
	}
	echo json_encode($message); die;	
}

public function checkAppointment($user_id,$friend_id){
	$where = " where (user_id = '".$user_id."' && friend_id='".$friend_id."') || (user_id = '".$friend_id."' && friend_id='".$user_id."')";
    $appointment =  $this->Common->select('chat_user_friend',$where);
    if($appointment[0]['create_dt'] != 0)
    {
    	return true;
    }
    return false;    
}
/* end today appointments */

/**
* Live Appointments
*/
public function live_appointments(){
	$this->version_check();
	$this->token();
	$header = getallheaders();
	if($header){
		$user_id = $header['user_id'];
		if(empty($user_id)){
			$manage = array('status' => 0, 'error' => __LINE__,
				'message' => 'Please send user id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}
        
        $post = file_get_contents('php://input');
	    if($post){
			$post = json_decode($post, TRUE);
			$clinic_id = $post['clinic_id'];
			if(empty($user_id)){
				$manage = array('status' => 0, 'error' => __LINE__,
					'message' => 'Please send clinic id');
				$rs =  json_encode($manage); 
				print_r($rs);die; 
		    }


		     $mt = getAppointmentNumber($user_id, $clinic_id);
                    $current_number = $mt['current_number'];
                    $my_number = $mt['my_number'];


           /* $where = " WHERE id = '".$clinic_id."'";
	        $result = $this->Common->select('user',$where);*/
 /* start */
	    $this->db->select('u.*,c.clinic_name,c.doctor_name,c.doctor_phone_no',false);
		$this->db->from('user u');
		$this->db->join('clinic_doctor_management c', 'u.id = c.user_id', 'LEFT');
		$this->db->where('u.id', $clinic_id); 
		
		
		/* end */
		       $query = $this->db->get();	
		       if(!empty($query)){
		       	$result = $query->result_array();
		        	/*$clinic_data = array('clinic_id' => $result[0]['id'],
		        						 'clinic_name' => ucwords($result[0]['full_name']),
										 'clinic_phone_number' => $result[0]['phone_number'],
										 'current_number' => "$current_number",
					                     'my_number' => "$my_number",
										 );*/
		            $clinic_data = array('clinic_name' => ucwords($result[0]['doctor_name']),
		            	  'clinicName' => ucwords($result[0]['full_name']),
				          'clinic_id' => $result[0]['id'],
				          'clinic_phone_number' => $result[0]['phone_number'],
				          'doctor_name' => ucwords($result[0]['doctor_name']),
			              'doctor_phone_no' => $result[0]['doctor_phone_no'],
			              'current_number' => "$current_number",
					      'my_number' => "$my_number",
				         );
		        }
		$this->db->select('a.*,u.id as patient_id, u.full_name as patient_name, u.phone_number as patient_phone_number, u.picture_url as patient_logo, m.name as treatment_type',false);
		$this->db->from('appointments a');
		$this->db->join('user u', 'u.id = a.patient_id', 'LEFT');
		$this->db->join('manage_treatment_type m', 'm.id = a.description', 'LEFT');
		$this->db->where('a.clinic_id', $clinic_id);

		$today = date('Y-m-d');
		$this->db->where('a.booking_date', $today);
		$this->db->order_by("a.time_slote", "asc");
		//$data = '';
		$query = $this->db->get();	
		if($query->num_rows()>0){
			$i = 1;
			foreach($query->result_array() as $val){
                 if($val['patient_id'] == $user_id){
					break;
				}
				$img = image_check($val['clinic_logo'],USER_IMAGE,'user');
                //'scheduled','confirmed','attended','cancel'

                $status = $val['status'];
                if($val['status'] == 'attended'){
                	$status = 'Done';
                }
				$data[] = array('patient_name' => 'Patient '.$i,
					            'patient_original_name' => ucwords($val['patient_name']),
								'time_slote' => $val['time_slote'],
								'description' => $val['treatment_type'],
								'booking_status' => ucwords($status),
								);
				

				$i++;
			}
			if(!empty($data)){
              $data = $data;
			}else{
              $data = array();
			}
			$message = array('status'=> 1, 'my_token_number' => '1', 'message'=> 'live booking.', 'clinic' => $clinic_data, 'result' => $data); 
		}else{
			$message = array('status' => 1, 'my_token_number' => '0', 'message' => 'No live booking.', 'clinic' => $clinic_data,'result' => '');
		}
      
       }else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
	    }

	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
	}
	echo json_encode($message); die;	
}
/* end live appointments */

/**
* past Appointments
*/
public function past_appointments(){
	$this->version_check();
	$this->token();
	$header = getallheaders();
	if($header){
		$user_id = $header['user_id'];
		if(empty($user_id)){
			$manage = array('status' => 0, 'error' => __LINE__,
				'message' => 'Please send user id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}

		$this->db->select('a.*,u.id as clinicId, u.full_name as clinic_name, u.phone_number as clinic_phone_number,u.picture_url as clinic_logo,d.doctor_name,d.doctor_phone_no',false);
		$this->db->from('appointments a');
		$this->db->join('user u', 'u.id = a.clinic_id', 'LEFT');
		$this->db->join('clinic_doctor_management d', 'u.id = d.user_id', 'LEFT');
		$this->db->where('a.patient_id', $user_id); 


		$today = date('Y-m-d');
		$this->db->where('a.booking_date !=', $today);
		$this->db->where('a.booking_date <=', $today);
		
		$this->db->order_by("a.id", "DESC");

		$query = $this->db->get();	
		if($query->num_rows()>0){
			foreach($query->result_array() as $val){

				$img = image_check($val['clinic_logo'],USER_IMAGE,'user');
                  
                  if($val['status'] == 'cancel'){
                  	$status = 'cancelled';
                  }else{
                  	$status = 'attended';
                  }
                
                $booking_date = $val['booking_date'];
				$timestamp = strtotime($booking_date);
				$booking_date = date('d-M-Y', $timestamp);
				
                $room_id = $this->get_room_id($val['clinic_id'], $val['patient_id']);
				$data[] = array('clinic_name' => ucwords($val['doctor_name']),
					'doctor_name' => ucwords($val['doctor_name']),
					'doctor_phone_no' => ucwords($val['doctor_phone_no']),
					'time_slote' => $val['time_slote'],
					'booking_status' => ucwords($status),
					'clinic_phone_number' => $val['clinic_phone_number'],
					'booking_date' => $booking_date,
					'clinic_logo' => $img,
					'clinic_id' => $val['clinicId'],
					'chat_room_id' => $room_id
				);
				$message = array('status'=> 1, 'booking_type' => 'pastBooking', 'message'=> 'past booking.','event_type'=>'past_appointments', 'result' => $data); 
			}
		}else{
			$message = array('status' => 1, 'booking_type' => 'pastBooking', 'message' => 'past booking.','event_type'=>'past_appointments', 'result' => '');
		}
	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
	}
	echo json_encode($message); die;	
}
/* end appointments */

/**
* clinic infomation
*/
public function clinic_infomation(){

	$this->version_check();
	$this->token();


	$post = file_get_contents('php://input');
	if($post){
		$post = json_decode($post, TRUE);
		$clinic_id = $post['clinic_id'] ? $post['clinic_id'] : '';

		if(empty($clinic_id)){
			$manage = array('status' => 0,
				'message' => 'Please send clinic id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}

		//$where = " WHERE id = ".$clinic_id;
		//$result = $this->Common->select('user',$where);



		$this->db->select('u.*,c.clinic_address,c.doctor_name,c.doctor_phone_no',false);
		$this->db->from('user u');
		$this->db->join('clinic_doctor_management c', 'u.id = c.user_id', 'LEFT');

		$this->db->where('u.id', $clinic_id); 
		
		$query = $this->db->get();	


		
		if(!empty($query)){

			$result = $query->result_array();

			$picture_url = image_check($result[0]['picture_url'],USER_IMAGE, 'user');
			$data = array('clinic_id' => $result[0]['id'],
				'clinic_name' => ucwords($result[0]['full_name']),
				'clinic_phone_number' => $result[0]['phone_number'],
				'clinic_email_address' => $result[0]['email_address'],
				'clinic_specialist' => $result[0]['clinical_specialist'], 
				'clinic_picture_url' => $picture_url,
                'clinic_address' => $result[0]['clinic_address'],
                'doctor_name' => ucwords($result[0]['doctor_name']),
				'doctor_phone_no' => $result[0]['doctor_phone_no'],
			    );	
			$manage = array('status' => 1, 'message' => SUCCESSFULLY, 'result' => $data);
		}else{			
			$manage = array('status' => 0, 'error' => __LINE__, 'message' => 'No record found for this user');
		}
	}else{
		$manage = array('status' => 0, 'error' => __LINE__, 'message'=>'Please send require parameter.');
	}
	$rs =  json_encode($manage); 
	print_r($rs);die; 
}
/* end clinic infomation */

/**
* appointments clinic
*/
public function appointments_clinic(){
	$this->version_check();
	$this->token();

	$header = getallheaders();
	if($header){
		$user_id = $header['user_id'];
		if(empty($user_id)){
			$manage = array('status' => 0,
				'message' => 'Please send user id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}


		$this->db->select('a.*,u.id as clinic_id, u.full_name as clinic_name, u.phone_number as clinic_phone_number,u.email_address as clinic_email_address, u.picture_url as clicnic_picture_url,u.clinical_specialist,c.doctor_name,c.doctor_phone_no',false);
		$this->db->from('appointments a');
		$this->db->join('user u', 'u.id = a.clinic_id', 'LEFT');
		$this->db->join('clinic_doctor_management c', 'u.id = c.user_id', 'LEFT');
		$this->db->where('a.patient_id', $user_id);
		$this->db->group_by('a.clinic_id'); 

		$query = $this->db->get();	
		if($query->num_rows()>0){
			foreach($query->result_array() as $val){

				$room_id = $this->get_room_id($val['clinic_id'], $val['patient_id']);

				$picture_url = image_check($val['clicnic_picture_url'],USER_IMAGE, 'user');
				$data[] = array(
					'clinic_name' => ucwords($val['doctor_name']),
					'clinicName' => ucwords($val['clinic_name']),
					'clinic_phone_number' => $val['clinic_phone_number'],
					'clinic_email_address' => $val['clinic_email_address'],
					'clinic_specialist' => $val['clinical_specialist'],
					'clinic_picture_url' => $picture_url,
					'clinic_id' => $val['clinic_id'],
				    'chat_room_id' => $room_id,

				    'doctor_name' => ucwords($val['doctor_name']),
					'doctor_phone_no' => $val['doctor_phone_no']
				);	

			}
			
			$manage = array('status' => 1, 'message' => SUCCESSFULLY, 'result' => $data);
		}else{
			$manage = array('status' => 1,  'message' => 'No record found.', 'result' => '');
		}
	}else{
		$manage = array('status' => 0, 'error' => __LINE__, 'message'=>'Please send require parameter.');
	}
	$rs =  json_encode($manage); 
	print_r($rs);die; 
}
/*End of file*/

/**
* chat send message   
*/
public function sendMessage(){
	$this->version_check();
	$this->token();

	$post = file_get_contents('php://input');
	if($post){
		$post = json_decode($post, TRUE);
		$sender_id = $post['sender_id'] ? $post['sender_id'] : '';
		$recipient_id = $post['recipient_id'] ? $post['recipient_id'] : '';
		$message = $post['message'] ? $post['message'] : '';

		if(empty($sender_id)){
			$manage = array('status' => 0,
				'message' => 'Please send sender id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}

		if(empty($recipient_id)){
			$manage = array('status' => 0,
				'message' => 'Please send recipient id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}


		if(empty($message)){
			$manage = array('status' => 0,
				'message' => 'Please send message');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}

        $where = ' WHERE (user_id = "'.$sender_id.'" || friend_id = "'.$sender_id.'") && (user_id = "'.$recipient_id.'" || friend_id = "'.$recipient_id.'")';

       	$result = $this->Common->select('chat_user_friend',$where);
		if(empty($result)){
            $ins_data = array('user_id' => $sender_id,
                                'friend_id' => $recipient_id
                                );
            $room_id = $this->Common->insert('chat_user_friend',$ins_data);
		}else{

			$room_id = $result[0]['id'];
		}


        $ins_data = array('sender_id' => $sender_id,
        				  'recipient_id' => $recipient_id,
        				  'message' => $message,
        				  'room_id' => $room_id,
        				  'create_dt' => time(),
        				  'chat_status' => 0
        				  );

		$this->Common->insert('chat_user_message',$ins_data);
		$manage = array('status' => 1, 'message' => SUCCESSFULLY, 'result' => $ins_data);
	}else{			
			$manage = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send require parameter.');
		}
	
	$rs =  json_encode($manage); 
	print_r($rs);die; 
}
/*End of file*/


private function get_room_id($sender_id,$recipient_id){
	$where = ' WHERE (user_id = "'.$sender_id.'" || friend_id = "'.$sender_id.'") && (user_id = "'.$recipient_id.'" || friend_id = "'.$recipient_id.'")';
    $result = $this->Common->select('chat_user_friend',$where);
    if(empty($result)){
    	return false;
    }else{
    	$room_id = $result[0]['id'];
    	return $room_id;
    }
}
/**
* Book an appointment
*/
public function book_appointment(){
	$this->version_check();
	$this->token();
	$post = file_get_contents('php://input');
	if($post){
		$post = json_decode($post, TRUE);

		$header = getallheaders();
		$user_id = $header['user_id'];

		$token = $post['appointment_token'] ? $post['appointment_token'] : '';
		
		if(empty($user_id)){
			$manage = array('status' => 0, 'error' => __LINE__,
				'message' => 'Please send user id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}else if(empty($token)){
			$manage = array('status' => 0, 'error' => __LINE__,
				'message' => 'Please send token number');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}

		if(!empty($token) && !empty($user_id)){		    

			$uCheck = $this->db->get_where('chat_user_friend', array('friend_code' => $token))->result_array();
			if(!empty($uCheck))
			{
				$create_dt = $uCheck[0]['create_dt'];
				if($create_dt > 0){
                   /*  $message = array('status' => 0, 'message' => 'You have already verify.', 'booking_type' => '');*/

                    $message = array('status' => 0, 'message' => 'You have already verify.','error' => __LINE__, );
				}else{
					$where = array('id'=>$uCheck[0]['id'],'friend_code'=> $token);
					$newPass = array('create_dt'=> time());
					$record = $this->Common->update('chat_user_friend', $newPass, $where);
					$message = array('status' => 1, 'message' => 'Doctor connect successfully.', 'booking_type' => '');
				}
			}else{
				$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Invalid token number.');
			}
		}else{
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send user id and token number');
		}
	}else{
		$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
	}
	echo json_encode($message); die;	
}
/* end change password */



/**
* clinic infomation
*/
public function notification(){
    $this->version_check();
	$this->token();

	$header = getallheaders();
	if($header){
		$user_id = $header['user_id'];
		if(empty($user_id)){
			$manage = array('status' => 0,
				'message' => 'Please send user id');
			$rs =  json_encode($manage); 
			print_r($rs);die; 
		}

	    $this->db->select('n.*, u.full_name,c.doctor_name',false);
		$this->db->from('notification n');
		$this->db->join('appointments a', 'a.id = n.appointment_id', 'LEFT');
		$this->db->join('user u', 'u.id = a.clinic_id', 'LEFT');

		$this->db->join('clinic_doctor_management c', 'c.user_id = u.id', 'LEFT');

		$this->db->where('n.user_id', $user_id); 

		
		
		$query = $this->db->get();
		$data = array();	
		if(!empty($query)){
			foreach($query->result_array() as $val){
			$data[] = array('notification_id' => $val['id'],
				'appointment_id' => $val['appointment_id'],
				'clinic_name' => ucwords($val['doctor_name']),
				'clinicName' => ucwords($val['full_name']),
				'doctor_name' => ucwords($val['doctor_name']),
				'description' => ucwords($val['description']),
				'create_dt' => date('d-M-Y',$val['create_dt']),
				);	
		    }
			$manage = array('status' => 1, 'message' => SUCCESSFULLY,'event_type'=>'notification', 'result' => $data);
		}else{			
			$manage = array('status' => 0, 'error' => __LINE__, 'message' => 'Notification not found.');
		}

	}else{
		$manage = array('status' => 0, 'error' => __LINE__, 'message'=>'Please send require parameter.');
	}
	$rs =  json_encode($manage); 
	print_r($rs);die; 
}
/* end clinic infomation */
  
    function array_check($val, $arr){
    	if (in_array($val, $arr)) {
		    return true;
		}
		return false;
    }



	
	public function getChatHistory(){
		//$this->version_check();
		//$this->token();

		$header = getallheaders();
		$user_id = $header['user_id'];
		$chat_id = array();
		if($user_id){
		 	$where = ' WHERE (user_id = "'.$user_id.'" || friend_id = "'.$user_id.'")';
		 	$result = $this->Common->select('chat_user_friend',$where);
		 	foreach ($result as $value) {
		 		$chat .=$value['id'].","; 
		 	}
		 	$chat = rtrim($chat,",");
		 	$getRId = '';
		 	$getData = $this->postDataCurl($chat);
		 	$arr = array();
			foreach($getData as $data)
			{
               $getRId = $data->chatroom_id;
               $st = $this->array_check($getRId, $arr);
               if(empty($st)){
               	   array_push($arr,$getRId);
               	   
                if($user_id != $data->sender_id){

                    $clinic_data = $this->get_clinic_name($data->sender_id);
                    $uCheck = $this->db->get_where('user_token', array('user_id' => $data->sender_id))->result_array();
                   // $online_status = $data->sender_id;
		 		}else if($user_id != $data->receiver_id){
		 			 $clinic_data = $this->get_clinic_name($data->receiver_id);
		 			 $uCheck = $this->db->get_where('user_token', array('user_id' => $data->receiver_id))->result_array();
		 			// $online_status = $data->receiver_id;
		 		}

		 		
		 		$online_status = 0;
		 		if(!empty($uCheck)){
                  $online_status = 1;
		 		}
		 		/*dd($clinic_name);*/

                  	   $array[] = array('sender_id' => $data->sender_id, 
                  	   				    'text' => $data->text,
                  	   				    'userName' => $data->userName,
                  	   				    'sendtimestamp' => "$data->sendtimestamp",
                  	   				    'receiver_id' => $data->receiver_id,
                  	   				    'chatroom_id' => $data->chatroom_id,
                                        'clinic_name' => $clinic_data['clinic_name'],
                                        'clinic_id' => $clinic_data['clinic_id'],
                                        'phone_number' => $clinic_data['phone_number'],
                                        'online_status' => $online_status 

               						);
               }

			}	
			if(!empty($array)){
				$array = $array;
			}else{
				$array = array();
			}
			$message = array('status'=>1,'message'=>SUCCESSFULLY, 'result' => $array);		
		 	
		}
		else
		{
			$message = array('status' => 0, 'error' => __LINE__, 'message' => INVALID_DATA_TYPE);
		}
		echo json_encode($message); die;
	}

	
	public function postDataCurl($roomid)
	{
		$data = array('roomid'=>$roomid);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://doctorappchatdemo.herokuapp.com/getChats");
		//curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);

		curl_close ($ch);
		return json_decode($server_output);
	}

	function get_clinic_name($user_id){

		$this->db->select('u.*,c.clinic_name,c.doctor_name,c.doctor_phone_no,t.token',false);
		$this->db->from('user u');
		$this->db->join('clinic_doctor_management c', 'u.id = c.user_id', 'LEFT');
		$this->db->join('user_token t', 'u.id = t.user_id', 'LEFT');
		$this->db->where('u.id', $user_id); 
		
		$query = $this->db->get();	
		if(!empty($query)){

			$result = $query->result_array();

			$data = array('clinic_name' => ucwords($result[0]['doctor_name']),
				          'clinic_id' => $result[0]['id'],
				          'phone_number' => $result[0]['phone_number'],
				          'doctor_name' => ucwords($result[0]['doctor_name']),
				          'doctor_phone_no' => $result[0]['doctor_phone_no'],

				         );
			return $data;


		}else{
			return false;
		}
	}
/*
	Function Name:-
		getDoctor_specialties
	Return : - 
		arrray	
	Required :-
		@id
*/
public function getDoctor_specialties($id)
{
    $this->db->select('ds.*');
    $this->db->from('doctor_specialties as ds');
    $this->db->where('ds.user_id',$id);
    $result = $this->db->get()->result_array();
    foreach ($result as $value)
    {
		$data[] = $value['specialties_id'];
    }
    //echo"<pre>";print_r($data);
    return $data;
}

/*
*	Notification delete
*	Param In-Body -> 'type'='false/true','notification_id'
*/ 
	public function del_notification(){
		$this->version_check();
		$this->token();
		$post = file_get_contents('php://input');
		if($post){
			$post = json_decode($post, TRUE);

			/*if(empty($post['type'])){
				$manage = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send type.');
				echo json_encode($manage); die;
			}*/

			$blanckArray = array("type"=>"false",
							"notification_id"=>"");
			$post = array_merge($blanckArray,$post);

			$header = getallheaders();
			$user_id = $header['user_id'];

			if($post['type'] == 'false'){
				if(empty($post['notification_id'])){
					$manage = array('status' => 0, 'error' => __LINE__, 'message' => 'Please send notification id.');
					echo json_encode($manage); die;
				}
				$result = $this->Common->select('notification'," where id='".$post['notification_id']."'");
				if($result[0]['user_id'] == $user_id){
					$this->Common->delete('notification',$post['notification_id'],'id');
					$manage = array('status' => 1, 'message' => 'Successfully delete notification.','event_type'=>'del_notification');
				}
				else{
					$manage = array('status' => 0, 'error'=>__LINE__, 'message' => 'This notification not exist.');
				}
			}
			else if($post['type'] == 'true'){
				$result = $this->Common->select('notification'," where user_id='".$user_id."'");
				if(!empty($result)){
					$this->Common->delete('notification', $user_id, 'user_id');
					$manage = array('status' => 1, 'message' => 'Successfully delete notification.','event_type'=>'del_notification');
				}
				else{
					$manage = array('status' => 0, 'error'=>__LINE__, 'message'=>'Here notifications not exist.');
				}
			}
		}
		$rs =  json_encode($manage); 
		print_r($rs);die; 
	}

	public function chat_setting(){
		$this->version_check();
		$this->token();
		$post = file_get_contents('php://input');
		if($post){
			$post = json_decode($post, TRUE);
            
            $header = getallheaders();
			$user_id = $header['user_id'];
			$doctor_id = $post['doctor_id'];

			if (empty($doctor_id)) {
				$message = array('status' => 0, 'message' => 'Please send doctor_id');
				$rs = json_encode($message);
				print_r($rs);die;
			}
			else{

				$where = ' where id = "'.$doctor_id.'"';
				$result = $this->Common->select('user',$where);
				
				if(!empty($result)){
				
					if ($result[0]['chat_setting'] == 0) {
						$manage = array('status' => 1, 'allow_chat' => 0);
					}
					elseif ($result[0]['chat_setting'] == 1) {
						$today = date('Y-m-d');
						$where = " where patient_id= '".$user_id."' and booking_date= '".$today."'";
						$appointment = $this->Common->select('appointments',$where);
						if(!empty($appointment)){
							$manage = array('status' => 1, 'allow_chat' => 1);
						}else{
                           $manage = array('status' => 1, 'allow_chat' => 0);
						}
					}
					elseif ($result[0]['chat_setting'] == 2) {
						$manage = array('status' => 1, 'allow_chat' => 1);
					}
				}else{
						$manage = array('status' => 0, 'message' => 'Doctor not found.');
				}
            }
		}
			$rs = json_encode($manage);
			print_r($rs);die;
	}


}