<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
    	parent::__construct();
    	check_user_login();
    	$this->load->model(array('UserModel'));
    }


	public function index()
	{
		$data['title'] = 'Dashboard';
		$id = $this->session->userdata('user_id');
		//$data['doctor_result']   = $this->UserModel->getDoctor();
		$data['appointment_result']   = $this->UserModel->getAppointment($id);
		$data['patient_result']  = $this->UserModel->getPatient();
		$data['visiting_result'] = $this->UserModel->getvisiting_status($id);
	    $this->load->view('dashboard',$data);
	}

}