<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
	}
	
	function testmail(){
	$data['first_name'] = 'pramod';
			$data['last_name'] = 'jain';
			$data['email_address'] = 'pramod.jn2@gmail.com';
			$data['password'] = '123456';
	        $mail_message = $this->load->view('mail_template/forget_password', $data, true); 
		  
		    $toEmail = 'pramod.jn2@gmail.com';
			$fromEmail = array('email' => config_item('no_reply'),'name' => config_item('site_name'));
			$subject = 'Reset your password';
			$attachment = array();
			$result = sendEmailCI($toEmail, $fromEmail, $subject, $mail_message, $attachment);
	}

	/**
	 	* CALL FOR VARIFICATION IF USER REGISTERED
	**/
	public function index(){
		
		$verifier = @$_POST['verifier'] ? @$_POST['verifier'] : @$_GET['verifier'];
		if($verifier){
		
		        $where = " where activation_code = '$verifier'"; 
				$result = $this->Common->select('user',$where);
				if(!empty($result)){
					
					$ver_user_id = $result[0]['id'];
					$where = array('id' => $ver_user_id);	
					$update_data = array('activation_code' => '',
					                   'status' => 'Active');
				 	$this->Common->update('user',$update_data,$where);
                    
					$data['msg'] = 'Well done! You have successfully verified your email address';	
					$data['class'] = 'alert alert-success';
					$data['img'] = '';
				}else{
					$data['msg'] = 'Invalid verification code.';
				    $data['class'] = 'alert alert-danger';
					$data['img'] = '';
					}
	      	}else{
				$data['msg'] = 'This link is expired';
				$data['class'] = 'alert alert-danger';
				$data['img'] = '';
			}
			$this->load->view('verification',$data);
			
	}
	
		public function logout(){
		$this->session->sess_destroy();
      	delete_cookie("userID");
		redirect(base_url('signin'));		
	}
	
	
}
