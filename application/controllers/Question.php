<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends CI_Controller {

	var $user_id = '';
    public function __construct() {
    	parent::__construct();
		//error_reporting(E_ALL);
    	check_user_login();
		
		$this->load->model(array());
		$this->user_id = $this->session->userdata('user_id');
	   // $this->output->enable_profiler(TRUE);	
        
	}

   /**
   * country board
   */
	public function board()
	{ 
	  $data['title'] = 'Board doctor specialization';
	  $where = " where parent_id ='0'";
	  $data['result'] = $this->Common->select('category', $where);
	   $this->load->view('question/board',$data);
	}


	public function board_children()
	{ 

	  $id = $this->uri->segment(3);
	  $data['id'] = $id;
	  $id = safe_b64decode($id);

	  $data['title'] = 'Board doctor specialization';


	  $where = " where id ='".$id."'";
	  $data['parent_result'] = $this->Common->select('category', $where);

	  $where = " where parent_id ='".$id."'";
	  $data['result'] = $this->Common->select('category', $where);
	   $this->load->view('question/board_children',$data);
	}
	
	
	
}