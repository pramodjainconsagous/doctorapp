<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	var $user_id = '';
    public function __construct() {
    	parent::__construct();
		error_reporting(E_ALL);
    	check_user_login();
		
		$this->load->model(array('SettingModel'));
		$this->user_id = $this->session->userdata('user_id');
	   // $this->output->enable_profiler(TRUE);	
        
	}

   /**
   * country grid
   */
	public function index()
	{ 
	   $data['title'] = 'Country';
	  $where = " where status = 'Active'";
	  $data['result'] = $this->SettingModel->country();
	   $this->load->view('setting/country',$data);
	}
    
	
   /**
   * country grid
   */
	public function country()
	{ 
	  $data['title'] = 'Country';
	  $where = " where status = 'Active'";
	  $data['result'] = $this->SettingModel->country();
	   $this->load->view('setting/country',$data);
	}
	
	
	public function addCountry(){
	  $data['title'] = 'Add country';
		$post = $this->input->post();

    	if($post && !empty($post))
    	{
    		$where = " where country ='".$post['country']."'";
			$data['result'] = $this->Common->select('country', $where);
			
			if(empty($data['result']))
			{			
	    		$ins_data = array('country' => $post['country'],
							  'internet' => $post['internet'],
							  'isd_code' => $post['isd_code'],
							  'status' => $post['status']
							 );
	    		$this->Common->insert('country', $ins_data);
	         	$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
			}
			else
			{
				$this->session->set_flashdata('error', 'country name already exix');
			}
		}
		
    	$this->load->view('setting/country_add',$data);	
	}
    
	public function editCountry(){
	
		$id = $this->uri->segment(3);
		$data['id'] = $id;
		$id = safe_b64decode($id);
		$data['title'] = 'Edit country';
		$post = $this->input->post();
		if($post && !empty($post)){
    		$data = array('country' => $post['country'],
						  'internet' => $post['internet'],
						  'isd_code' => $post['isd_code'],
						  'status' => $post['status'],
						  );
			
			$where = array('countryid' => $id);
    		$this->Common->update('country', $data, $where);
			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('setting/country');
    	}
		$data['result'] = $this->SettingModel->country($id);
		$this->load->view('setting/country_edit',$data);	
    
	}	
	
	public function addState(){
	
	$stateid = $this->uri->segment(3);
	$data['state_id'] = $stateid;
	$state_id = safe_b64decode($stateid);
		
	  $data['title'] = 'Add state';
		$post = $this->input->post();
		
		
	  $data['country_result'] = $this->Common->select('country');
			

    	if($post && !empty($post))
    	{
    		$where = " where country ='".$post['country']."'";
			$data['result'] = $this->Common->select('country', $where);
			
			if(empty($data['result']))
			{			
	    		$ins_data = array('countryid' => $post['country'],
							  'region' => $post['region'],
							  'code' => $post['code'],
							  'status' => $post['status']
							 );
	    		$this->Common->insert('country_regions', $ins_data);
	         	$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
			}
			else
			{
				$this->session->set_flashdata('error', 'state name already exix');
			}
		}
		
    	$this->load->view('setting/state_add',$data);	
	}
	
	public function editState(){
	
		$countryid = $this->uri->segment(3);
		$data['country_id'] = $countryid;
		$country_id = safe_b64decode($countryid);
		
		$stateid = $this->uri->segment(4);
		$data['state_id'] = $stateid;
		$state_id = safe_b64decode($stateid);
		
		$data['title'] = 'Edit state';
		$post = $this->input->post();
		if($post && !empty($post)){
		//dd($post);
    		$data = array('region' => $post['region'],
						  'code' => $post['code'],
						  'status' => $post['status'],
						  );
			
			$where = array('regionid' => $state_id);
    		$this->Common->update('country_regions', $data, $where);
			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('setting/state/'.$countryid);
    	}
		 $data['result'] = $this->SettingModel->state($country_id, $state_id);
		// dd($data['result']);
		$this->load->view('setting/state_edit',$data);	
    
	}	
	
	
	public function addCity(){
	
		$stateid = $this->uri->segment(3);
		$data['state_id'] = $stateid;
		$state_id = safe_b64decode($stateid);
		 $data['country_result'] = $this->Common->select('country');
			
		$data['title'] = 'Add city';
		$post = $this->input->post();
		if($post && !empty($post)){
		//dd($post);
    		$ins_data = array('countryid' => $post['country'],
						  'regionid' => $post['region'],
						  'city' => $post['city'],
						  'status' => $post['status'],
						  );
			
			$this->Common->insert('country_region_cities', $ins_data);
			$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
			redirect('setting/city/'.$stateid);
    	}
		$this->load->view('setting/city_add',$data);	
    
	}	
	
	public function editCity(){
	
		$stateid = $this->uri->segment(3);
		$data['state_id'] = $stateid;
		$state_id = safe_b64decode($stateid);
		
		$cityid = $this->uri->segment(4);
		$data['city_id'] = $cityid;
		$city_id = safe_b64decode($cityid);
		
		
		$data['title'] = 'Edit city';
		$post = $this->input->post();
		if($post && !empty($post)){
		//dd($post);
    		$data = array('city' => $post['city'],
						  'status' => $post['status'],
						  );
			
			$where = array('cityId' => $city_id);
    		$this->Common->update('country_region_cities', $data, $where);
			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('setting/city/'.$stateid);
    	}
		 $data['result'] = $this->SettingModel->city($state_id, $city_id);
		//dd($data['result']);
		$this->load->view('setting/city_edit',$data);	
    
	}	
	
   /**
   * country region
   */
	public function region()
	{ 
	  $data['title'] = 'State';
	  $where = " where status = 'Active'";
	  $data['result'] = $this->SettingModel->country();
	   $this->load->view('setting/country_state',$data);
	}
	
	
	 /**
   * country state
   */
	public function state()
	{ 
	  $id = $this->uri->segment(3);
	  
	  $data['id'] = $id;
	  $id = safe_b64decode($id); 
		
	  $data['title'] = 'State';
	  $where = " where status = 'Active'";
	  $data['result'] = $this->SettingModel->state($id);
	  $this->load->view('setting/state',$data);
	}
	
	
	 /**
   * country city
   */
	public function city()
	{ 
	  $stateid = $this->uri->segment(3);
	  
	  $data['state_id'] = $stateid;
	  $state_id = safe_b64decode($stateid); 
		
	  $data['title'] = 'City';
	  $where = " where status = 'Active'";
	  $data['result'] = $this->SettingModel->city($state_id);
	  $this->load->view('setting/city',$data);
	}
	
	
	
		
	/************************************************************** category module ***************************************
	* category grid
	*/
	public function category(){
    	$data['title'] = 'Company';
		$data['catResult'] = $this->articleModel->category();
    	$this->load->view('article/category',$data);
   	}
	
	/**
	* Add new category
	*/
	 public function addCategory(){
		$data['title'] = 'Add new company';
		if($_POST){
		$post = $this->input->post();
			
		$where = " where title = '".$post['cat_title']."'";
		$result = $this->Common->select('article_company', $where);
		if(empty($result)){
		
			
    		$data = array('title' => $post['cat_title'],
						  'create_dt' => time()
						 );
    		$this->Common->insert('article_company', $data);
			$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
			redirect('article/category');
		 }else{
		 $this->session->set_flashdata('error', 'This companny name already exists please create different.');
		 }
		}
    	$this->load->view('article/category_add',$data);	
    }
	
	
	
	/**
	* Edit category
	*/
	 public function editCategory(){
		$id = $this->uri->segment(3);
		$data['id'] = $id;
		$id = safe_b64decode($id);
		$data['title'] = 'Edit company';
		$post = $this->input->post();
		if($post && !empty($post)){
    		
    		$data = array('title' => $post['cat_title'],
						  'update_dt' => time()
						 );
			
			$where = array('id' => $id);
    		$this->Common->update('article_company', $data, $where);
			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('article/category');
    	}
		$data['catResult'] = $this->articleModel->category($id);
		$this->load->view('article/category_edit',$data);	
    }

/************************************************************** Brand module ***************************************
	* brand grid
	*/
	public function brand(){
    	$data['title'] = 'Company Brand';
		$data['result'] = $this->articleModel->brand();
		$this->load->view('article/brand',$data);
   	}
	
	/**
	* Add new category
	*/
	 public function addBrand(){
		$data['title'] = 'Add new brand';
		
		$success = '';
		$data['message'] = '';
		$upload_file_name = '';
		$target_dir = DOCUMENT_ROOT."media/article/";
		
		if($_POST){
    		$post = $this->input->post();
			
			$data = array('title' => $post['brand_name'],
						  'description' => $post['description'],
						  'create_dt' => time()
						 );
						 
			if(!empty($_FILES['item_img']['name'])){
				$responce_img = $this->Common->imageUpload($_FILES['item_img'],$target_dir);
				
				$upload_image_name = $responce_img['name'];
				
				$image_upload = array('item_img' => $upload_image_name);	
			    $data = array_merge($data, $image_upload);
			
			}
    		$id = $this->Common->insert('article_brand', $data);
			
			
			  if(!empty($post['company_name'])){
		 		foreach($post['company_name'] as $val){ 
				 $dataComp = array('company_id' => $val,
								'brand_id' => $id);
				$this->Common->insert('article_company_brand_relation', $dataComp);
								
				}
			}
			
			$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
			redirect('article/brand');
		}
    	$this->load->view('article/brand_add',$data);	
    }
	
	
	
	/**
	* Edit category
	*/
	 public function editBrand(){
		$id = $this->uri->segment(3);
		$data['id'] = $id;
		$id = safe_b64decode($id);
		$data['title'] = 'Edit brand';
		
		$target_dir = DOCUMENT_ROOT."media/article/";	
		$success = '';
		$data['message'] = '';
		$upload_file_name = '';



		$post = $this->input->post();
		if($post && !empty($post)){
		
		   
		   if(!empty($post['company_name'])){
		   $this->Common->delete('article_company_brand_relation', $id,'brand_id');
				
				foreach($post['company_name'] as $val){ 
				 $dataComp = array('company_id' => $val,
								'brand_id' => $id);
				$this->Common->insert('article_company_brand_relation', $dataComp);
								
				}
			}
			  $data = array('title' => $post['brand_name'],
						    'description' => $post['description'],
						    'update_dt' => time()
						    );
						 
			
			if(!empty($_FILES['item_img']['name'])){
				$responce_img = $this->Common->imageUpload($_FILES['item_img'],$target_dir);
				$upload_image_name = $responce_img['name'];
				
				
				$where = "where id = $id";
				$uploadResult = $this->Common->select('article_brand', $where);
				if(!empty($uploadResult)){
				 $old_upload_file_name = $uploadResult[0]['item_img'];
					  @unlink($target_dir.$old_upload_file_name);
				}
				
				
				$image_upload = array('item_img' => $upload_image_name);	
			    $data = array_merge($data, $image_upload);
			
			}
			
			$where = array('id' => $id);
    		$this->Common->update('article_brand', $data, $where);
			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('article/brand');
    	}
		$data['result'] = $this->articleModel->brand($id);
		
		$data['relation_company'] = $this->articleModel->relation_company($id);
		
		
		
		$data['company'] = $this->Common->select('article_company');
		//dd($data['company']);
		$this->load->view('article/brand_edit',$data);	
    }
	
	/************************************************************** Item module ***************************************
	* Item grid
	*/
	public function item(){
    	$data['title'] = 'Product Item';
		$data['result'] = $this->articleModel->article_item();
		$this->load->view('article/item',$data);
   	}
	
	/**
	* Add new category
	*/
	 public function addItem(){
		$data['title'] = 'Add new item';
		
		$success = '';
		$data['message'] = '';
		$upload_file_name = '';
		$target_dir = DOCUMENT_ROOT."media/article/";	
		
		if($_POST){
    		$post = $this->input->post();
			$data = array('company_id' => $post['company_name'],
						  'brand_id' => $post['brand_id'],
						  'title' => $post['item_name'],
						  'description' => $post['description'],
						  'create_dt' => time()
						 );
			if(!empty($_FILES['item_img']['name'])){
				$responce_img = $this->Common->imageUpload($_FILES['item_img'],$target_dir);
				
				$upload_image_name = $responce_img['name'];
				
				$image_upload = array('item_img' => $upload_image_name);	
			    $data = array_merge($data, $image_upload);
			
			}
    		$this->Common->insert('article_item', $data);
			$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
			redirect('article/item');
		}
    	$this->load->view('article/item_add',$data);	
    }
	
	
	/**
	* Edit category
	*/
	 public function editItem(){
		$id = $this->uri->segment(3);
		$data['id'] = $id;
		$id = safe_b64decode($id);
		
		$target_dir = DOCUMENT_ROOT."media/article/";	
		$success = '';
		$data['message'] = '';
		$upload_file_name = '';

		$data['title'] = 'Edit brand';
		$post = $this->input->post();
		if($post && !empty($post)){
    		$data = array('company_id' => $post['company_name'],
						  'brand_id' => $post['brand_id'],
						  'title' => $post['item_name'],
						  'description' => $post['description'],
						  'update_dt' => time()
						 );
			
			if(!empty($_FILES['item_img']['name'])){
				$responce_img = $this->Common->imageUpload($_FILES['item_img'],$target_dir);
				$upload_image_name = $responce_img['name'];
				
				
				$where = "where id = $id";
				$uploadResult = $this->Common->select('article', $where);
				if(!empty($uploadResult)){
				 $old_upload_file_name = $uploadResult[0]['item_img'];
					  @unlink($target_dir.$old_upload_file_name);
				}
				
				
				$image_upload = array('item_img' => $upload_image_name);	
			    $data = array_merge($data, $image_upload);
			
			}


			$where = array('id' => $id);
    		$this->Common->update('article_item', $data, $where);
			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('article/item');
    	}
		$data['result'] = $this->articleModel->article_item($id);
		//dd($data['result']);
		$this->load->view('article/item_edit',$data);	
    }
	
	
	
}