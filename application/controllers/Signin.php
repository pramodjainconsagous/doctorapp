<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {

	public function __construct()
    {
		parent::__construct();		
		$this->load->helper(array());
		$this->load->library(array('form_validation'));
		//$this->output->enable_profiler(TRUE);	
    }
	
	public function index()
	{
		$data['msg']['error'] = '';
		$post = $this->input->post();
		if($post && !empty($post)){
       		
       		$email    = $post['username'];
			$password = $post['password'];

			//$where = " where email_address = '".$email."' && password = '".$password."'";
			//$result = $this->Common->select('user',$where);


			$this->db->select('u.*,m.doctor_name',false);
			$this->db->from('user u');
			$this->db->join('clinic_doctor_management m', 'm.user_id = u.id', 'LEFT');
			$this->db->where('u.email_address', $email); 
			$this->db->where('u.password', $password); 
			$query = $this->db->get();	
			if($query->num_rows()>0){
					$result = $query->result_array();
			
			
                if($result[0]['status'] == 'Active'){


                	$token = mt_rand();
					$data_token = array('token' => $token,
										'user_id' => $result[0]['id']);
					$this->Common->delete('user_token', $result[0]['id'], 'user_id');
					$this->Common->insert('user_token', $data_token);

				$this->session->set_userdata('user_id',$result[0]['id']);
				$this->session->set_userdata('user_role',$result[0]['user_role']);
				$this->session->set_userdata('parent_id',$result[0]['parent_id']);
				$this->session->set_userdata('name',$result[0]['doctor_name']);
				$this->session->set_userdata('email',$result[0]['email']);

                
                if($result[0]['parent_id'] == 0){
                	 $clinic_id = $result[0]['id'];
                }else{
                	 $clinic_id = $result[0]['parent_id'];
                }
               

				$this->session->set_userdata('clinic_id',$clinic_id);
				redirect('Dashboard');
			 }else{
			 	 $data['msg']['error'] = 'your a/c is deActive Please contact to administrator';
			 }
			}
			else
			{
             $data['msg']['error'] = LOGIN_UNVALID_DETAIL;
			}
        }
		$data['stylesheet'] = array('assets/vendors/animate/css/animate.min.css');
		$this->load->view('login',$data);
	}

	public function logout(){
        $user_id = $this->session->userdata('user_id');
		$this->Common->delete('user_token', $user_id, 'user_id');
		$this->session->sess_destroy();
      	redirect(base_url('Signin'));
	}
}
