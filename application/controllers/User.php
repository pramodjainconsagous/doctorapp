<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
    	parent::__construct();
    	check_user_login();
		
		$this->load->helper(array());
		$this->load->library(array('form_validation'));
		$this->load->model(array('UserModel'));
		//$this->output->enable_profiler(TRUE);	
    }


	public function index()
	{    $data['title'] = 'User';  
	     $data['userResult'] = $this->UserModel->userInfomation();
		 $this->load->view('user/user',$data);
	}
	
	/**
	* Add New user
	*/    
	  public function addUser(){
		$data['title'] = 'Create New User';  
		$data['message'] = '';
		$post = $this->input->post();

    	if($post && !empty($post)){
		echo '<pre/>';print_r($post); die;
			extract($post);
	        $where = "where email_address = '".$post['email']."'";
			$uploadResult = $this->Common->select('user', $where);
			if(!empty($uploadResult))
			{
				$data['message'] = EMAIL_ERROR_MSG;
			}
			else
			{
			
		$target_dir = $this->config->item('root_path');   
		$imgName = '';
		if($_FILES){
		  $target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
		  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		  $imgName = time().'.'.$imageFileType;
		  $target_file = $target_dir . $imgName;
		  move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
		}  

			    $insertData = array('first_name' => $post['first_name'],
                            'last_name' => $post['last_name'],
                            'phone_number' => $post['phone_number'],
                            'password' => $post['password'],
                            'email_address' => $post['email_address'],
                            'mailing_address1' => $post['mailing_address1'],
                            'mailing_address2' => $post['mailing_address2'],
                            'city' => $post['city'],
                            'state' => $post['state'],
                            'country' => $post['country'],
                            'zipcode' => $post['zipcode'],
                            'school_state' => $post['school_state'],
                            'name_of_school' => $post['name_of_school'],
                            'graduation_month' => $post['graduation_month'],
                            'graduation_year' => $post['graduation_year'],
							'board_month' => $post['board_month'],
							'status' => $post['status'],
							'picture_url' => $imgName,
							'create_dt' => time()
                            );

               $id =  $this->Common->insert('user', $insertData);
	        	$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
				redirect('User', 'refresh');
			}
		}
    	$this->load->view('user/user_add',$data);	
		
    }
	
	
	/**
	* Edit user
	*/    
    public function editUser(){
		$id = $this->uri->segment(3);
    	$data['id'] = $id;
    	$id = safe_b64decode($id);
		$data['title'] = 'Update User Information';  
		$data['message'] = '';
		$post = $this->input->post();

    	if($post && !empty($post))
    	{
    		$post = $this->input->post();
			
		$target_dir = $this->config->item('root_path');   
		$imgName = '';
		if($_FILES){
		  $target_file = $target_dir . basename($_FILES["ads_img"]["name"]);
		  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		  $imgName = time().'.'.$imageFileType;
		  $target_file = $target_dir . $imgName;
		  move_uploaded_file($_FILES["ads_img"]["tmp_name"], $target_file);
		}  
	
			//echo '<pre/>';print_r($post); die;
			 $data = array('first_name' => $post['first_name'],
                            'last_name' => $post['last_name'],
                            'phone_number' => $post['phone_number'],
                            'password' => $post['password'],
                            'email_address' => $post['email_address'],
                            'mailing_address1' => $post['mailing_address1'],
                            'mailing_address2' => $post['mailing_address2'],
                            'city' => $post['city'],
                            'state' => $post['state'],
                            'country' => $post['country'],
                            'zipcode' => $post['zipcode'],
                            'school_state' => $post['school_state'],
                            'name_of_school' => $post['name_of_school'],
                            'graduation_month' => $post['graduation_month'],
                            'graduation_year' => $post['graduation_year'],
							'board_month' => $post['board_month'],
							'status' => $post['status'],
							'picture_url' => $imgName,
							'update_dt' => time()
                            );
    		
			 $where = array('id'=>$id);
             $this->Common->update('user', $data, $where);
		 
		 
         	$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('User', 'refresh');
		}
		$data['result'] = $this->UserModel->userInfomation($id);
		$this->load->view('user/user_edit',$data);
	}
	

/********************************************************* Role ***********************
* Show user seller
*/
	public function seller()
	{    $data['title'] = 'Buyer Infomation';  
	     $data['userResult'] = $this->UserModel->getSellerInfomation();
		 $this->load->view('user/seller',$data);
	}
     
	 
	 /**
	* Add New seller
	*/    
    public function addSeller(){
		$data['title'] = 'Create New Buyer';  
		$data['message'] = '';
		$post = $this->input->post();

    	if($post && !empty($post)){
			extract($post);
				$data = array( 'role_id' => 4,
								'user_name' => $post['user_name'],
								'mobile' => $post['mobile'],
								'gst_no' => $post['gst_no'],
								'address' => $post['address'],
								'create_dt' => time() );
	    		$this->Common->insert('user', $data);
	         	$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
				redirect('user/seller', 'refresh');
		}
    	$this->load->view('user/seller_add',$data);	
		
    }
	
	
	/**
	* Edit seller
	*/    
    public function editSeller(){
		$id = $this->uri->segment(3);
    	$data['id'] = $id;
    	$id = safe_b64decode($id);
		$data['title'] = 'Update Buyer Information';  
		$data['message'] = '';
		$post = $this->input->post();

    	if($post && !empty($post))
    	{
    		$post = $this->input->post();
			$data = array( 'user_name' => $post['user_name'],
							'mobile' => $post['mobile'],
							'gst_no' => $post['gst_no'],
							'address' => $post['address'],
							'update_dt' => time() );
    		
			 $where = array('id'=>$id);
             $this->Common->update('user', $data, $where);

         	$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('user/seller', 'refresh');
		}
		$data['result'] = $this->UserModel->getSellerInfomation($id);
		$this->load->view('user/seller_edit',$data);
	}
	

/********************************************************* Role ***********************
* Show user Buyer
*/
	public function buyer()
	{    $data['title'] = 'Seller Infomation';  
	     $data['userResult'] = $this->UserModel->getBuyerInfomation();
		 $this->load->view('user/buyer',$data);
	}
	
		 /**
	* Add New seller
	*/    
    public function addBuyer(){
		$data['title'] = 'Create New Seller';  
		$data['message'] = '';
		$post = $this->input->post();

    	if($post && !empty($post)){
			extract($post);
				$data = array( 'role_id' => 3,
								'user_name' => $post['user_name'],
								'mobile' => $post['mobile'],
								'address' => $post['address'],
								'fram_name' => $post['fram_name'],
								'propriter_name' => $post['properwrite_name'],
								'gst_no' => $post['gst_no'],
								
								'create_dt' => time() );
	    		$this->Common->insert('user', $data);
	         	$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
				redirect('user/buyer', 'refresh');
		}
    	$this->load->view('user/buyer_add',$data);	
		
    }
	
	
	/**
	* Edit seller
	*/    
    public function editBuyer(){
		$id = $this->uri->segment(3);
    	$data['id'] = $id;
    	$id = safe_b64decode($id);
		$data['title'] = 'Update Seller Information';  
		$data['message'] = '';
		$post = $this->input->post();

    	if($post && !empty($post))
    	{
    		$post = $this->input->post();
			$data = array( 'user_name' => $post['user_name'],
							'mobile' => $post['mobile'],
							'address' => $post['address'],
							'fram_name' => $post['fram_name'],
								'propriter_name' => $post['properwrite_name'],
								'gst_no' => $post['gst_no'],
							'update_dt' => time() );
    		
			 $where = array('id'=>$id);
             $this->Common->update('user', $data, $where);

         	$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('user/buyer', 'refresh');
		}
		$data['result'] = $this->UserModel->getBuyerInfomation($id);
		//dd($data['result']);
		$this->load->view('user/buyer_edit',$data);
	}
	
/********************************************************* Role ***********************
* Show Role Data
*/
public function role(){
	$data['title'] = 'User Role';
	$data['userRole'] = $this->Common->select('user_role');
	$this->load->view('role/role',$data);
}

/**
* Add New Role
*/    
public function addRole(){
	$data['title'] = 'Add New User Role';
	$post = $this->input->post();
	if($post && !empty($post)){
		$data = array( 'role_name'=>$post['role'] );
		$this->Common->insert('user_role', $data);
		$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
		redirect('User/role', 'refresh');
	}
	$this->load->view('role/role_add',$data);
}

/**
* Edit User Role
*/
public function editRole(){
	$id = $this->uri->segment(3);
	$data['id'] = $id;
	$id = safe_b64decode($id);
	$data['title'] = 'Edit User Role';
	$post = $this->input->post();
	if($post && !empty($post))
	{
	   	 $data = array('role_name'=>$post['role']);
	     $where = array('id'=>$post['roleId']);
	     if($this->Common->update('user_role', $data, $where))
	     {
	     	$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
	     	redirect('User/role','refresh');
	     }
	     else
	     {
	     	$this->session->set_flashdata('error', UPDATE_FAIL);
	     }
	}
	$where = " where id='".$id."'";
	$data['result'] = $this->Common->select('user_role', $where);
	$this->load->view('role/role_edit', $data);
}

	/**
	* Db Backup
	*/
	public function dbBackup(){
	
	$data['title'] = 'Export databases';
	$this->load->view('user/dbBackup', $data);
	}

}