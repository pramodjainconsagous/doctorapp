<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor_specialties extends CI_Controller
{
	var $user_id = '';
    public function __construct()
    {
    	parent::__construct();
		//error_reporting(E_ALL);
    	check_user_login();
		
		$this->user_id = $this->session->userdata('user_id');
	   // $this->output->enable_profiler(TRUE);	
	}

	/**
	* function name :- 
		index
	 result :-
	 	page view doctor specialties 
	*/
	public function index()
	{ 
	   $data['title'] = 'Doctor Specialties';
	   $where = "ORDER BY id DESC";
	   $data['result'] = $this->Common->select('manage_doctor_specialties', $where);
	   //$data['count'] = $this->
	   $this->load->view('admin/doctor_specialties/doctor_specialties_view',$data);
	}
	
	/**
	* function name :- 
		doctor_specialties_edit
	 result :-
	 	page view doctor specialties 
	 required parameter :-
	 	@id
	*/
	public function doctor_specialties_edit()
	{ 
		$dsid           = $this->uri->segment(4);
		$data['dsid']   = $dsid;
		$dsid           = safe_b64decode($dsid);
		$data['title']  = 'Edit specialties';
		$where          = "where id = '".$dsid."'";
		$data['result'] = $this->Common->select('manage_doctor_specialties', $where);

		$post = $this->input->post();
		if($post && !empty($post))
		{

			$data = array
			(
				'name'   => $post['name'],
				'status' => $post['status']
			);
			$where = array('id' => $dsid);

    		$this->Common->update('manage_doctor_specialties', $data, $where);
			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('admin/doctor_specialties');
    	}
		$this->load->view('admin/doctor_specialties/doctor_specialties_edit',$data);
	}

	/**
	* function name :- 
		doctor_specialties_edit
	 result :-
	 	page  doctor specialties add
	 required parameter :-
	 	
	*/
   public function doctor_specialties_add()
   {
		$data['title']  = 'Add specialties';
		$post           = $this->input->post();
		if($post && !empty($post))
		{
         	$data = array
         	(
				'name'   => $post['name'],
				'status' => $post['status']
			);

			$this->Common->insert('manage_doctor_specialties', $data);
			$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
			redirect('admin/doctor_specialties');
    	}
		$this->load->view('admin/doctor_specialties/doctor_specialties_add',$data);	
   }
}