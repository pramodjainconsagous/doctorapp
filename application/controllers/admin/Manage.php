<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends CI_Controller {

	var $user_id = '';
    public function __construct() {
    	parent::__construct();
		//error_reporting(E_ALL);
    	check_user_login();
		
		$this->user_id = $this->session->userdata('user_id');
	   // $this->output->enable_profiler(TRUE);	
        
	}

  
	/**
	* ads grid
	*/
	public function ads()
	{ 
	   $data['title'] = 'Manage Ads';

	   $where = "ORDER BY id DESC";
	   $data['result'] = $this->Common->select('ads', $where);

	   $this->load->view('admin/ads/ads_view',$data);
	}
   
   
   /**
   * ads add
   */
   public function adsAdd(){
			$data['title']              = 'Add ads';
			$data['result']             = $this->Common->select('ads');
			$data['doctor_result']      = $this->Common->select('clinic_doctor_management');
			$data['specialties_result'] = $this->Common->select('manage_doctor_specialties');

		$post = $this->input->post();
		if($post && !empty($post)){
			//dd($post);
			$target_dir = DOCUMENT_ROOT.'media/ads/';   
			$imgName = '';
			if($_FILES){
				if(!empty($_FILES["ads_img"]["name"])){
			  $target_file = $target_dir . basename($_FILES["ads_img"]["name"]);
			  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			  $imgName = time().'.'.$imageFileType;
			  $target_file = $target_dir . $imgName;
			  move_uploaded_file($_FILES["ads_img"]["tmp_name"], $target_file);
			}
			}  


			$data = array('ads_title' => $post['ads_title'],
						  'ads_description' => $post['ads_description'],
						  'ads_link' => $post['ads_link'],
						  'status' => $post['status'],
						  'update_dt' => time()
						  );
			
			if(!empty($imgName)){
			$array2 = array('ads_img' => $imgName);
			$data = array_merge($data, $array2);
		    }

			$ads_insert_id = $this->Common->insert('ads', $data);
			/*insert doctor relation to ads */
			if (!empty($post['doctor_name']))
			{
				foreach ($post['doctor_name'] as $key => $value)
				{
					//ads_associates_doctor
					$data = array
					(
						'ads_id'    => $ads_insert_id,
						'doctor_id' => $value
					);
					$this->Common->insert('ads_associates_doctor', $data);
				}
			}
			/*insert specialties relation to ads */
			if (!empty($post['specialties_name']))
			{
				foreach ($post['specialties_name'] as $key => $value)
				{
					//ads_associates_doctor
					$data = array
					(
						'ads_id'         => $ads_insert_id,
						'specialties_id' => $value
					);
					$this->Common->insert('ads_associates_specialties', $data);
				}
			}
			$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
			redirect('admin/manage/ads');
    	}
		
		$this->load->view('admin/ads/ads_add',$data);	
    }

   /**
   * ads edit
   */
   public function adsEdit(){
        $adsid = $this->uri->segment(4);
		$data['adsid'] = $adsid;
		$adsid = safe_b64decode($adsid);

		$data['title'] = 'Edit ads';
		$where = "where id = '".$adsid."'";
	    $data['result'] = $this->Common->select('ads', $where);

		$where       = "where ads_id = '".$adsid."'";
		$data['aas'] = $this->Common->select('ads_associates_specialties', $where);
		$data['aad'] = $this->Common->select('ads_associates_doctor', $where);
		
		$post = $this->input->post();
		if($post && !empty($post)){
		
			$target_dir = DOCUMENT_ROOT.'media/ads/';   
			$imgName = '';
			if($_FILES){
				if(!empty($_FILES["ads_img"]["name"])){
			  $target_file = $target_dir . basename($_FILES["ads_img"]["name"]);
			  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			  $imgName = time().'.'.$imageFileType;
			  $target_file = $target_dir . $imgName;
			  move_uploaded_file($_FILES["ads_img"]["tmp_name"], $target_file);
			  @unlink($target_dir. $data['result'][0]['ads_img']);
			}
			}  


	    		$data = array('ads_title' => $post['ads_title'],
							  'ads_description' => $post['ads_description'],
							  'ads_link' => $post['ads_link'],
							  'status' => $post['status'],
							  'update_dt' => time()
							  );

	    	if(!empty($imgName)){
			$array2 = array('ads_img' => $imgName);
			$data = array_merge($data, $array2);
		    }

				
				$where = array('id' => $adsid);
	    		$this->Common->update('ads', $data, $where);

	    		$this->Common->delete('ads_associates_doctor', $adsid, 'ads_id');
	    		$this->Common->delete('ads_associates_specialties', $adsid, 'ads_id');
	    		/*insert doctor relation to ads */
			if (!empty($post['doctor_name']))
			{
				foreach ($post['doctor_name'] as $key => $value)
				{
					//ads_associates_doctor
					$data = array
					(
						'ads_id'    => $adsid,
						'doctor_id' => $value
					);
					$this->Common->insert('ads_associates_doctor', $data);
				}
			}
			/*insert specialties relation to ads */
			if (!empty($post['specialties_name']))
			{
				foreach ($post['specialties_name'] as $key => $value)
				{
					//ads_associates_doctor
					$data = array
					(
						'ads_id'         => $adsid,
						'specialties_id' => $value
					);
					$this->Common->insert('ads_associates_specialties', $data);
				}
			}
				$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
				redirect('admin/manage/ads');
    	}
		$data['doctor_result']      = $this->Common->select('clinic_doctor_management');
		$data['specialties_result'] = $this->Common->select('manage_doctor_specialties');
		$this->load->view('admin/ads/ads_edit',$data);	
    }
    
    /**
    * ads delete
    */
	public function ads_delete(){
		$post = $this->input->post();
		if($post && !empty($post)){
			$id = safe_b64decode($post['id']);

			$where = "where id = '".$id."'";
	        $result = $this->Common->select('ads', $where);
	        if($result){
	        	$target_dir = DOCUMENT_ROOT.'media/ads/'; 
	        	@unlink($target_dir. $result[0]['ads_img']);
            	$this->Common->delete('ads', $id, 'id');
			    $message = array('status' => 1, 'message' => 'Successfully delete.');
	        }else{
	        	$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Ads not found.');
	        }
		}else{
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please try again something went wrong.');
		}
     	echo json_encode($message); die;
	}



}