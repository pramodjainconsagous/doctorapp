<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_status extends CI_Controller
{
	var $user_id = '';
    public function __construct()
    {
    	parent::__construct();
		//error_reporting(E_ALL);
    	check_user_login();
		$this->user_id = $this->session->userdata('user_id');
	   // $this->output->enable_profiler(TRUE);	
	}

	public function visit_status(){
		$data['title'] = 'Type of Visit Status';

		$data['result'] = $this->Common->select('manage_visit_status');

		$this->load->view('admin/status/visit_status',$data);

	}
	public function visit_status_edit(){
		$data['title'] = 'Edit Visit Status';
		$iid = $this->uri->segment(4);
		$data['id']   = $iid;
		$id = safe_b64decode($iid);
		$where = " where id='".$id."'";
		$data['result'] =$this->Common->select('manage_visit_status',$where);
		$post = $this->input->post();
		if($post && !empty($post)){
			$insertData = array('name'=> $post['visit_name'],'status'=>$post['status']);
			$where = array('id'=>$id);
			$this->Common->update('manage_visit_status',$insertData,$where);
			redirect("admin/Manage_status/visit_status",'refresh');
		}

		$this->load->view('admin/status/visit_status_edit',$data);
	}
	public function visit_status_add(){
		$data['title'] = 'Add Visit Status';
		//$data['result'] =$this->Common->select('manage_appointment_status'," where name = $post['name']");
		$post = $this->input->post();
		if($post && !empty($post)){
			$insertData = array('name'=> $post['name'],'status'=>$post['status']);
			$this->Common->insert('manage_visit_status',$insertData);
			redirect("admin/Manage_status/visit_status",'refresh');
		}

		$this->load->view('admin/status/visit_status_add',$data);
	}
	public function delete_visit(){
        $post = $this->input->post();
            if($post && !empty($post))
            {
                $id = safe_b64decode($post['id']);

                $where = array('id'=>$id);
                $result = $this->Common->select('manage_appointment_status', $where);
                if($result){
                    $this->Common->delete('manage_appointment_status', $id, 'id');
                   
                    $message = array('status' => 1, 'message' => 'Successfully delete.');
                }
                else
                {
                    $message = array('status' => 0, 'error' => __LINE__, 'message' => 'Employ not found.');
                }
            }
            else
            {
                $message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please try again something went wrong.');
            }
        echo json_encode($message); die;
    }

	public function appointment_status(){
		$data['title'] = 'Appointment Status';

		$data['result'] = $this->Common->select('manage_appointment_status');

		$this->load->view('admin/status/appointment_status',$data);

	}
	public function appointment_status_edit(){
		$data['title'] = 'Edit Appointment Status';
		$iid = $this->uri->segment(4);
		$data['id']   = $iid;
		$id = safe_b64decode($iid);
		$where = " where id='".$id."'";
		$data['result'] =$this->Common->select('manage_appointment_status',$where);
		$post = $this->input->post();
		if($post && !empty($post)){
			$insertData = array('name'=> $post['name'],'status'=>$post['status']);
			$where = array('id'=>$id);
			$this->Common->update('manage_appointment_status',$insertData,$where);
			redirect("admin/Manage_status/appointment_status",'refresh');
		}

		$this->load->view('admin/status/appointment_status_edit',$data);
	}
	public function appointment_status_add(){
		$data['title'] = 'Add Appointment Status';
		//$data['result'] =$this->Common->select('manage_appointment_status'," where name = $post['name']");
		$post = $this->input->post();
		if($post && !empty($post)){
			$insertData = array('name'=> $post['name'],'status'=>$post['status']);
			$this->Common->insert('manage_appointment_status',$insertData);
			redirect("admin/Manage_status/appointment_status",'refresh');
		}

		$this->load->view('admin/status/appointment_status_add',$data);
	}

	public function delete_appointment(){
        $post = $this->input->post();
            if($post && !empty($post))
            {
                $id = safe_b64decode($post['id']);

                $where = array('id'=>$id);
                $result = $this->Common->select('manage_appointment_status', $where);
                if($result){
                    $this->Common->delete('manage_appointment_status', $id, 'id');
                   
                    $message = array('status' => 1, 'message' => 'Successfully delete.');
                }
                else
                {
                    $message = array('status' => 0, 'error' => __LINE__, 'message' => 'Employ not found.');
                }
            }
            else
            {
                $message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please try again something went wrong.');
            }
        echo json_encode($message); die;
    }

}