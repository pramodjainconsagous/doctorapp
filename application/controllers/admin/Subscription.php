<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends CI_Controller {

	var $user_id = '';
    public function __construct() {
    	parent::__construct();
		//error_reporting(E_ALL);
    	check_user_login();
		
		$this->user_id = $this->session->userdata('user_id');
	   // $this->output->enable_profiler(TRUE);	
        
	}

  
	/**
	* subscription grid
	*/
	public function index()
	{ 
	   $data['title'] = 'Manage Subscription';

	   $where = "ORDER BY id DESC";
	   $data['result'] = $this->Common->select('manage_subscription_plan', $where);

	   $this->load->view('admin/subscription/subscription_view',$data);
	}
   
   
   /**
   * subscription add
   */
   public function subscriptionAdd(){
     
     	$data['title'] = 'Add Subscription';
	    $data['result'] = $this->Common->select('manage_subscription_plan');

		$post = $this->input->post();
		if($post && !empty($post)){
         	$data = array('plan_name' => $post['plan_name'],
						  'plan_details' => $post['plan_details'],
						  'amount' => $post['amount'],
						  'plan_duration' => $post['plan_duration'],
						  'status' => $post['status'],
						  );

			$this->Common->insert('manage_subscription_plan', $data);
			$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
			redirect('admin/subscription');
    	}
		
		$this->load->view('admin/subscription/subscription_add',$data);	
    }

   /**
   * subscription edit
   */
   public function subscriptionEdit(){
        $adsid = $this->uri->segment(4);
		$data['adsid'] = $adsid;
		$adsid = safe_b64decode($adsid);

		$data['title'] = 'Edit Subscription';
		$where = "where id = '".$adsid."'";
	    $data['result'] = $this->Common->select('manage_subscription_plan', $where);

		$post = $this->input->post();
		if($post && !empty($post)){
		

   	    	$data = array('plan_name' => $post['plan_name'],
						  'plan_details' => $post['plan_details'],
						  'amount' => $post['amount'],
						  'plan_duration' => $post['plan_duration'],
						  'status' => $post['status'],
						  );

				
				$where = array('id' => $adsid);
	    		$this->Common->update('manage_subscription_plan', $data, $where);
				$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
				redirect('admin/subscription');
    	}
		
		$this->load->view('admin/subscription/subscription_edit',$data);	
    }
    
    /**
    * subscription delete
    */
	public function subscription_delete(){
		$post = $this->input->post();
		if($post && !empty($post)){
			$id = safe_b64decode($post['id']);

			$where = "where id = '".$id."'";
	        $result = $this->Common->select('manage_subscription_plan', $where);
	        if($result){
	        	$this->Common->delete('manage_subscription_plan', $id, 'id');
			    $message = array('status' => 1, 'message' => 'Successfully delete.');
	        }else{
	        	$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Subscription plan not found.');
	        }
		}else{
			$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please try again something went wrong.');
		}
     	echo json_encode($message); die;
	}



}