<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		check_user_login();
		
		$this->load->helper(array());
		$this->load->library(array('form_validation'));
		$this->load->model(array('UserModel'));
		//$this->output->enable_profiler(TRUE);	
	}


	public function index(){    
		$data['title'] = 'User';  
		$data['userResult'] = $this->UserModel->userInfomation();
		$this->load->view('user/user',$data);
	}

	/**
	* Clinic Doctor
	*/
	public function doctor()
	{    $data['title'] = 'Doctor Management';  
	$data['result'] = $this->UserModel->getDoctor();
	$this->load->view('admin/user/doctor',$data);
}



    /**
	* Add Clinic Doctor
	*/    
	public function doctorAdd(){
		
		$data['title'] = 'Add Clinic Information';  
		$data['message'] = '';
		$post = $this->input->post();

		if($post && !empty($post))
		{
			$post = $this->input->post();

			$where = " where phone_number = '".$post['phone_number']."' || email_address = '".$post['email_address']."' "; 
			$result = $this->Common->select('user',$where);

			if(empty($result)){


				$time = time();
				$target_dir = $this->config->item('root_path');   
				$imgName = '';
				if($_FILES){
					if(!empty($_FILES["profile_image"]["name"])){
						$target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
						$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
						$imgName = time().'.'.$imageFileType;
						$target_file = $target_dir . $imgName;
						move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
					}
				}  

				$data = array('user_role' => 2,
					'full_name' => $post['clinic_name'],
					'phone_number' => $post['phone_number'],
					'password' => $post['password'],
					'email_address' => $post['email_address'],
					'status' => $post['status'],
					'picture_url' => $imgName,
					'create_dt' => $time,
					'update_dt' => $time
				);

				$id = $this->Common->insert('user', $data);

				$doctor_data = array('user_id' => $id,
					'doctor_name' => $post['doctor_name'],
					'doctor_phone_no' => $post['doctor_phone_no'],
					'doctor_email_id' => $post['doctor_email_id'],
					'doctor_about_us' => $post['doctor_about_us'],
					'clinic_name' => $post['clinic_name'],
					'clinic_address' => $post['clinic_address'],
					'lat' => $post['latitude'],
					'long' => $post['longitude'],
				);

				$this->Common->insert('clinic_doctor_management', $doctor_data);


				$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
				redirect('admin/user/doctor', 'refresh');
			}else{
				$data['message'] = 'Email id or phone number already exits.';
			}
		}
		$data['result'] = $this->UserModel->getDoctor($id);
		$this->load->view('admin/user/doctor_add',$data);
	}


    /**
	* Edit Clinic Doctor
	*/    
	public function doctorEdit(){
		$id = $this->uri->segment(4);
		$data['id'] = $id;
		$id = safe_b64decode($id);
		$data['title'] = 'Update Clinic Information';  
		$data['message'] = '';
		$post = $this->input->post();

		if($post && !empty($post))
		{
			$post = $this->input->post();
			$target_dir = $this->config->item('root_path');   
			$imgName = '';
			if($_FILES){
				if(!empty($_FILES["profile_image"]["name"])){
					$target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
					$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
					$imgName = time().'.'.$imageFileType;
					$target_file = $target_dir . $imgName;
					move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
				}
			}  

			$data = array('full_name' => $post['clinic_name'],
				'phone_number' => $post['phone_number'],
				'password' => $post['password'],
				'email_address' => $post['email_address'],
				'status' => $post['status'],
				'update_dt' => time()
			);

			if(!empty($imgName)){
				$array2 = array('picture_url' => $imgName);
				$data = array_merge($data, $array2);
			}

			$doctor_data = array('doctor_name' => $post['doctor_name'],
				'doctor_phone_no' => $post['doctor_phone_no'],
				'doctor_email_id' => $post['doctor_email_id'],
				'doctor_about_us' => $post['doctor_about_us'],
				'clinic_name' => $post['clinic_name'],
				'clinic_address' => $post['clinic_address'],
				'lat' => $post['latitude'],
				'long' => $post['longitude'],
			);


			$where = array('id'=>$id);
			$this->Common->update('user', $data, $where);


			$where = array('user_id'=>$id);
			$this->Common->update('clinic_doctor_management', $doctor_data, $where);


			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('admin/user/doctor', 'refresh');
		}
		
		$data['result'] = $this->UserModel->getDoctor($id);
		$this->load->view('admin/user/doctor_edit',$data);
	}



	/**
	* Patient
	*/
	public function patient()
	{    $data['title'] = 'Patient Management';  
	$data['result'] = $this->UserModel->getPatient();
	$this->load->view('admin/user/patient',$data);
}


    /**
	* Add Patient
	*/    
	public function patientAdd(){
		$id = $this->uri->segment(4);
		$data['id'] = $id;
		$id = safe_b64decode($id);
		$data['title'] = 'Add Patient';  
		$data['message'] = '';
		$post = $this->input->post();

		if($post && !empty($post))
		{
			$post = $this->input->post();

			$where = " where phone_number = '".$post['phone_number']."'"; 
			$result = $this->Common->select('user',$where);
			if(empty($result)){

				
				$target_dir = $this->config->item('root_path');   
				$imgName = '';
				if($_FILES){
					$target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
					$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
					$imgName = time().'.'.$imageFileType;
					$target_file = $target_dir . $imgName;
					move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
				}  
				$time = time();

				$data = array('user_role' => 3,
					'full_name' => $post['full_name'],
					'phone_number' => $post['phone_number'],
					'password' => $post['password'],
					'email_address' => $post['email_address'],
					'status' => $post['status'],
					'picture_url' => $imgName,
					'create_dt' => $time,
					'update_dt' => $time
				);

				$this->Common->insert('user', $data);

				$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
				redirect('admin/user/patient', 'refresh');

			}else{
				$data['message'] = 'Email id already exits.';
			}

		}
		
		$this->load->view('admin/user/patient_add',$data);
	}


    /**
	* Edit Patient
	*/    
	public function patientEdit(){
		$id = $this->uri->segment(4);
		$data['id'] = $id;
		$id = safe_b64decode($id);
		$data['title'] = 'Update Patient Information';  
		$data['message'] = '';
		$post = $this->input->post();

		if($post && !empty($post))
		{
			$post = $this->input->post();
			
			$target_dir = $this->config->item('root_path');   
			$imgName = '';
			if($_FILES){
				if(!empty($_FILES["profile_image"]["name"])){
					$target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
					$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
					$imgName = time().'.'.$imageFileType;
					$target_file = $target_dir . $imgName;
					move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
				}
			}  

			$data = array('full_name' => $post['full_name'],
				'phone_number' => $post['phone_number'],
				'password' => $post['password'],
				'email_address' => $post['email_address'],
				'status' => $post['status'],
				'update_dt' => time()
			);

			if(!empty($imgName)){
				$array2 = array('picture_url' => $imgName,);
				$data = array_merge($data, $array2);
			}

			$where = array('id'=>$id);
			$this->Common->update('user', $data, $where);

			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('admin/user/patient', 'refresh');
		}
		
		$data['result'] = $this->UserModel->getPatient($id);
		$this->load->view('admin/user/patient_edit',$data);
	}
    
    /**
    *
    */
    private function subscriptionCheck($user_id){
    	$where = " where user_id = '".$user_id."' ORDER BY id DESC";
		$subscription_result = $this->Common->select('user_subscription_plan',$where);
		if(!empty($subscription_result)){
            	 $subscription_end_dt = $subscription_result[0]['subscription_end_dt'];
            	 $today_date = time();
            	// if today date is greater then subscription end date then create new subscription ohter wise message show you have already subscription
            	if($today_date > $subscription_end_dt){
            		return true;
            	}else if($today_date <  $subscription_end_dt){
            		// already subscribe
            		return false;
            	}

        }
        return true;
    }
	/**
	* Add subscription
	*/
	public function addSubscription(){
		$data['title'] = 'Add subscription';  
		$data['message'] = '';
		$post = $this->input->post();
		if($post && !empty($post))
		{
		   $subscription_name = $post['subscription_name'];
           $doctor_name = $post['doctor_name'];
           $time = time();

           $check_subs = $this->subscriptionCheck($doctor_name);
          
           if($check_subs){
             
            $subscription_end_dt = $time; 
            $where = " where id = '".$subscription_name."'";
		    $subscription_result = $this->Common->select('manage_subscription_plan',$where);
            if(!empty($subscription_result)){
            	$subscription_result = $subscription_result[0]['plan_days'];
                if($subscription_result > 0){
	            	$today_date = date('Y-m-d');
			        $date = new DateTime($today_date);
					$date->modify("+".$subscription_result." day");
					$subscription_end_dt = $date->format("Y-m-d");
					$subscription_end_dt = strtotime($subscription_end_dt);
			    }

            }

           $ins_data = array('subscription_id' => $subscription_name,
                             'user_id' => $doctor_name,
                             'create_dt' => $time,
                             'subscription_start_dt' => $time,
                             'subscription_end_dt' => $subscription_end_dt
           					 );
        	$this->Common->insert('user_subscription_plan', $ins_data);
			$data['success'] = 'Successfully subscribe clinic';
           }else{
           	// this clinic already sucscribe
           	$data['message'] = 'This clinic already subscribe';

           }
		}
		
		$where = " where status = 'Active'"; 
		$data['result'] = $this->Common->select('manage_subscription_plan',$where);

		//$where = " where user_role = '2' && status = 'Active'"; 
		$data['clinic_result'] = $this->Common->select('clinic_doctor_management',$where='');
		$this->load->view('admin/user/addSubscription',$data);
	}

    /**
    * subscriber user
    */
    public function subscriber(){
        
        $data['title'] = 'Subscriber user';  
    	$data['result'] = $this->UserModel->getSubscriberUser_doctor();
    	//dd($data['result']);
		$this->load->view('admin/user/subscriber_user',$data);
    }
   
    /**
    * account status change
    */
    public function ac_status_change(){
       $post = $this->input->post();
       if($post && !empty($post)){
        $id = $post['id'];
        $where = " where id = '".$id."'";
		$user_result = $this->Common->select('user',$where);
		if(!empty($user_result)){
            	$status = $user_result[0]['status'];
            	if($status == 'Active'){
            		$status = 'inActive';
            	}else{
            		$status = 'Active';
            	}
             
		$data = array('status'=> $status);
	
		$where = array('id'=>$id);
		$this->Common->update('user', $data, $where);
		$message = array('status' => 1, 'message' => 'Successfully change status.');
	  }else{
	        	$message = array('status' => 0, 'error' => __LINE__, 'message' => 'somthing error found.');
	        }
	}else{
	  	$message = array('status' => 0, 'error' => __LINE__, 'message' => 'Please try again something went wrong.');
	  }
     echo json_encode($message); die;
    }





}