<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends CI_Controller {
  var $user_id = '';
  var $clinic_id = '';

  public function __construct() {
   parent::__construct();
   check_user_login();
   $this->user_id = $this->session->userdata('user_id');
   $this->clinic_id = $this->session->userdata('clinic_id');

   $this->load->helper(array());
   $this->load->library(array('form_validation'));
   $this->load->model(array('UserModel'));
		//$this->output->enable_profiler(TRUE);	

 }

    /**
	* Doctor
	*/
	public function index()
	{    $data['title'] = 'Today Appointment';  
  $data['result'] = $this->UserModel->getAppointments();
  $this->load->view('doctor/appointment/appointment_list_view',$data);
  }

	/**
	*  Appointment list
	*/
 public function appointment_list(){
   $data['title'] = 'Today Appointment';  
   $data['result'] = $this->UserModel->getAppointments();
   $data['clinic_id'] = $this->clinic_id;

   $data['s_date'] =  date('Y-m-d', strtotime("-7 days"));
   $data['e_date'] =  date('Y-m-d', strtotime("-1 days"));

   $s_date = $data['s_date'];
   $e_date = $data['e_date'];

      //$data['result'] = $this->UserModel->appointments_history($s_date, $e_date);

   $this->load->view('doctor/appointment/appointment_list_view',$data);

 
 }
  


   /**
   * Add new Appointment
   */
   public function add(){

    $data['title'] = 'Appointment Add';  
    $data['result'] = $this->UserModel->getAppointments();
    $data['visit_status'] = $this->Common->select('manage_visit_status');

    $post = $this->input->post();
    $time = date('Y-m-d h:i A');
    if($post && !empty($post))
    {
      $check = $post['appointment_date'].' '.$post['time_slote'];
      if(strtotime($time) >= strtotime($check))
      {
        $this->session->set_flashdata('warning','Please select after current time.');
        redirect('doctor/Appointment/add');
      }

      $this->form_validation->set_rules('patient_id', 'Mobile number not exits', 'required');
      $this->form_validation->set_rules('appointment_date', 'Appointment date', 'required');
      $this->form_validation->set_rules('time_slote', 'Time slote', 'required');
      $this->form_validation->set_rules('booking_status', 'Booking status', 'required');
      $this->form_validation->set_rules('visiting_status', 'Visiting status', 'required');

      if($this->form_validation->run() == FALSE)
      {

      }
      else
      {
       
        $booking_date = date('Y-m-d');
        //Don’t allow the same user to register twice on the same day.
        $where = " where booking_date = '".$post['appointment_date']."' && patient_id = '".$post['patient_id']."'";  
        $booking_result = $this->Common->select('appointments',$where);
        if(!empty($booking_result)){
            $this->session->set_flashdata('warning','You have already appointment same day.');
           redirect('doctor/Appointment/add');
        }else{

        }

        $data = array('clinic_id' => $this->session->userdata('clinic_id'),
          'patient_id' => $post['patient_id'],
          'booking_date' => $post['appointment_date'],
          'time_slote' => $post['time_slote'],
          'description' => $post['description'],
          'status' => $post['booking_status'],
          'visiting_status' => $post['visiting_status'],
          'create_dt' => time()
        );

        $appointments_id = $this->Common->insert('appointments', $data);
        $clinic_id = $this->session->userdata('clinic_id');


        $notification_data = array('user_id' => $post['patient_id'],
                             'appointment_id' => $appointments_id,
                             'description' => 'Your appointment has been booked',
                             'create_dt' => time()
                            );
        $this->Common->insert('notification',$notification_data);

        $where = " where id = '".$appointments_id."'"; 
        $notify = $this->Common->select('appointments',$where);
        if(!empty($notify)){
          $this->send_push_notification($notify[0]['patient_id'], $appointments_id);
        }
        

        $where = ' WHERE (user_id = "'.$post['patient_id'].'" || friend_id = "'.$post['patient_id'].'") && (user_id = "'.$clinic_id.'" || friend_id = "'.$clinic_id.'")';
        $result = $this->Common->select('chat_user_friend',$where);
        if(empty($result)){
          $activation_code = mt_rand(1000, 9999);
           $ins_data = array('user_id' => $clinic_id,
                             'friend_code' => $activation_code,
                             'friend_id' => $post['patient_id'],
                            );
            $room_id = $this->Common->insert('chat_user_friend',$ins_data);

  $sender_data = array('room_id' => $room_id,'user_id' => $clinic_id);  
  $reciver_data = array('room_id' => $room_id,'user_id' => $post['patient_id']);         
  $this->Common->insert('chat_user_message_status',$sender_data);
  $this->Common->insert('chat_user_message_status',$reciver_data);


         $this->sendSms($post['patient_id'], $appointments_id,$room_id);
        }

       $this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
       redirect('doctor/appointment/appointment_list', 'refresh');
     }               
     
   }

   $this->load->view('doctor/appointment/appointment_add',$data);
 }

  /**
* send opt
*/
function sendSms($sender_id=NULL, $appointments_id=NULL,$room_id=NULL){

  if(!empty($sender_id)){
    $where = " where id = '".$sender_id."' && phone_number != ''"; 
    $result = $this->Common->select('user',$where);
    if(!empty($result)){
      $phone_number = $result[0]['phone_number'];
      $app_details = $this->getAppointmentsList($appointments_id);
      if(!empty($app_details)){
       $clinic_id = $app_details[0]['clinic_id'];
       $clinic_name = ucwords($app_details[0]['clinic_name']);
       $time_slote = $app_details[0]['time_slote'];
       $booking_date = $app_details[0]['booking_date'];

       $message = 'appointment - '.$booking_date.' and time is '.$time_slote.' clinic name '.$clinic_name;

       $otp = $room_id;
       $ch = curl_init();
       $message .= 'Token no is - ';
       $message = rawurlencode(utf8_encode($message));

       $url="https://control.msg91.com/api/sendhttp.php?authkey=167017AeiaeT42JEr659783c0c&mobiles=".$phone_number."&message=".$message.' '.$otp."&sender=DoctorApp&route=4";

       curl_setopt($ch, CURLOPT_URL,$url);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_HEADER, 0);  
       $output=curl_exec($ch);
       $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
       curl_close($ch);
       if($httpcode=="200"){
          return true;
        }else{
          return false;
        }
      }
    }  
  }
}


   /**
   * Add new Appointment
   */
   public function edit(){

    $id = $this->uri->segment(4);
    $data['id'] = $id;
    $id = safe_b64decode($id);
    $data['title'] = 'Appointment Edit';
    $data['visit_status'] = $this->Common->select('manage_visit_status');

    $post = $this->input->post();
    if($post && !empty($post))
    {

     $data = array('booking_date' => $post['appointment_date'],
      'time_slote' => $post['time_slote'],
      'description' => $post['description'],
      'status' => $post['booking_status'],
      'visiting_status'=>$post['visiting_status'],
      'update_dt' => time()
    );

     $where = array('id'=>$id);
     $this->Common->update('appointments', $data, $where);

     /* send push notification to user */

     $where = " where id = '".$id."'"; 
     $result = $this->Common->select('appointments',$where);
     if(!empty($result)){
      $this->send_push_notification($result[0]['patient_id'], $id);
    }

    $this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
    redirect('doctor/appointment/appointment_list', 'refresh');
  }

  $data['result'] = $this->UserModel->getAppointments($id);
  $this->load->view('doctor/appointment/appointment_edit',$data);
}

 /*
*  Appointment Delete
*/ 
public function appointment_delete(){
$id = $this->uri->segment(4);
 $id = safe_b64decode($id);
 $where = "where id = '".$id."'";
 $result = $this->Common->select('appointments',$where);
 if ($result) {
   $this->Common->delete('appointments',$id);
    $this->session->set_flashdata('success', APPOINTMENT_DELETE_SUCCESS_MSG);
   redirect('doctor/appointment/appointment_list');
 }else{
   echo "false";
 }
} 
 

public function getAppointmentsList($appointments_id = NULL){

  $this->db->select('a.*,u.id as clinic_id, u.full_name as clinic_name, u.phone_number as clinic_phone_number, u.picture_url as clinic_picture_url',false);
  $this->db->from('appointments a');
  $this->db->join('user u', 'u.id = a.clinic_id', 'LEFT');

  $this->db->where('a.id', $appointments_id);

  $query = $this->db->get();  
  if($query->num_rows()>0){
    return $query->result_array();
  }else{
    return false;
  }
}


   /**
   * send push notification 
   */
   private function send_push_notification($sender_id=NULL, $appointments_id=NULL){
     if(!empty($sender_id)){
      $where = " where id = '".$sender_id."' && device_token != ''"; 
      $result = $this->Common->select('user',$where);

      if(!empty($result)){
       $device_token = $result[0]['device_token'];
       $device_type = strtolower($result[0]['device_type']);
       $app_details = $this->getAppointmentsList($appointments_id);
       //if(!empty($app_details)){
         $clinic_id = $app_details[0]['clinic_id'];
         $clinic_name = ucwords($app_details[0]['clinic_name']);
         $time_slote = $app_details[0]['time_slote'];
         $booking_date = $app_details[0]['booking_date'];

         $message = 'your appointment date id '.$booking_date.' and time is '.$time_slote.' clinic name '.$clinic_name;
         if($device_type == 'android'){

          $this->androidPush($message, $device_token, 'Appointment detail');

        }else if($device_type == 'ios'){

          $this->sendIosPush($message, $device_token, 'Appointment detail');

        } 
      }
    }  
  }


   /**
   * send push notification for change status
   */
   private function send_status_notify($sender_id=NULL, $appointments_id=NULL){
     if(!empty($sender_id)){
      $where = " where id = '".$sender_id."' && device_token != ''"; 
      $result = $this->Common->select('user',$where);

      if(!empty($result)){
       $device_token = $result[0]['device_token'];
       $device_type = strtolower($result[0]['device_type']);
       $app_details = $this->getAppointmentsList($appointments_id);
       //if(!empty($app_details)){
         $clinic_id = $app_details[0]['clinic_id'];
         $clinic_name = ucwords($app_details[0]['clinic_name']);
         $time_slote = $app_details[0]['time_slote'];
         $booking_date = $app_details[0]['booking_date'];

         $message = 'you are next patient.';
        if($device_type == 'android'){

          $this->androidPush($message, $device_token, 'Appointment status.');

        }else if($device_type == 'ios'){

          $this->sendIosPush($message, $device_token, 'Appointment status.');

        } 
    }  
  }
}

// private function androidPush($message,$token,$title){
//   $documentRoot = $this->config->item('file_path');  
//   require($documentRoot.'gcm/GCMPushMessage.php');

//   $apiKey = $this->config->item('android_push_key');

//   $devices = array();
//   $message = $message;

//   $devices[] =$token;
//   $an = new GCMPushMessage($apiKey);
//   $an->setDevices($devices);
//   $response = $an->send($message);
//         //return $response;
//   return true;
// }

private function androidPush($message,$token,$title){
 //$apiKey = $this->config->item('ios_push_key');
 $ch = curl_init("https://fcm.googleapis.com/fcm/send");
 $notification = array('title' =>$title , 'text' => $message);
 $arrayToSend = array('to' => $token, 'content_available' => true, 'notification' => $notification, 'data' => $notification,'priority'=>'high');
 $json = json_encode($arrayToSend);
 $headers = array();
 $headers[] = 'Content-Type: application/json';
   $headers[] = 'Authorization: key=AAAArvuRPWo:APA91bHvOREipl6uLHq32vx2cmM1KXOMaMIBDtMDGiseQ3YBxpb6f5-dVXluNh2k10X-x3P9SngGfXFgf-wbY7O_uaetOgqDAvRGX7J2bPKnujeh1UAENB_gw0qqvAgSyO96VhXNfW6L'; // 

   
   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                 
   curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
   curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);  
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);     
   $response = curl_exec($ch);
   curl_close($ch);
   return true;
}

/*private function sendIosPush($message,$token,$title){
 //$apiKey = $this->config->item('ios_push_key');
 $ch = curl_init("https://fcm.googleapis.com/fcm/send");
 $notification = array('title' =>$title , 'text' => $message);
 $arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');
 $json = json_encode($arrayToSend);
 $headers = array();
 $headers[] = 'Content-Type: application/json';
   $headers[] = 'Authorization: key=AAAArvuRPWo:APA91bHvOREipl6uLHq32vx2cmM1KXOMaMIBDtMDGiseQ3YBxpb6f5-dVXluNh2k10X-x3P9SngGfXFgf-wbY7O_uaetOgqDAvRGX7J2bPKnujeh1UAENB_gw0qqvAgSyO96VhXNfW6L'; // $apiKey key here
   //Setup curl, add headers and post parameters.
   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
   curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
   curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);  
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);     
   //Send the request
   $response = curl_exec($ch);
   //Close request
   curl_close($ch);
   //return $response;
   return true;
}*/
/*function test(){

  $token = "fw87X3Lcalk:APA91bFelAa2AdIoYA7qhLx8vjm5PfxV7cQJdXpRYyA3Yj_MhPMMISk5uZ3LFWmjJlhHAPf6mXjER5QvMnhzF2B6SKC19tUOz7BODcJSqDXjflTGHlrqtdc8a_2OZDfC7elU4NuUMLWk";
    $title = "Test Appointment notification";
    $body = "Bear island knows no king but the king in the north, whose name is stark.";

  $st = $this->sendIosPush($token, $title, $body);
  echo '<pre/>';
print_r($st);
}
*/
function sendIosPush($body, $token, $title ){
    $ch = curl_init("https://fcm.googleapis.com/fcm/send");
    $notification = array('title' =>$title , 'text' => $body);
    $arrayToSend = array('to' => $token, 'content_available' => true, 'notification' => $notification);
    $json = json_encode($arrayToSend);
    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key=AAAArvuRPWo:APA91bHvOREipl6uLHq32vx2cmM1KXOMaMIBDtMDGiseQ3YBxpb6f5-dVXluNh2k10X-x3P9SngGfXFgf-wbY7O_uaetOgqDAvRGX7J2bPKnujeh1UAENB_gw0qqvAgSyO96VhXNfW6L'; 
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                       
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);       
    $response = curl_exec($ch);
   // echo $response;die;
    curl_close($ch);
    return true;

}

   /** 
   * Appointment history
   */
   public function history(){
    $data['title'] = 'Appointment Records';  

    $data['s_date'] =  date('Y-m-d', strtotime("-7 days"));
    $data['e_date'] =  date('Y-m-d', strtotime("-1 days"));

    $s_date = $data['s_date'];
    $e_date = $data['e_date'];

    $post = $this->input->post();
    if($post && !empty($post))
    {
      $data['s_date'] = $post['s_date'];
      $data['e_date'] = $post['e_date'];

      $s_date = $data['s_date'];
      $e_date = $data['e_date'];
    }

    $data['result'] = $this->UserModel->appointments_history($s_date, $e_date);
//    dd($data['result']);
    $this->load->view('doctor/appointment/appointment_history_view',$data);

  }

   /**
   * Appointment status change
   */
   public function appointment_status_change(){
    $id = $this->input->post('id');
    $status = $this->input->post('status');
    $clinic_id = $this->input->post('clinic_id');

    $today = date('Y-m-d');

    $where = " where  id = '".$id."'";
    $result= $this->Common->select('appointments',$where);
    if(!empty($result)){

      $insertData = array('status' => $status);
      $where = array('id' => $id);
      $id = $this->Common->update('appointments', $insertData, $where);
      $this->send_status_notify($result[0]['patient_id'],$id);
      $message = array('status' => 1, 'message' => 'status change successfully');   

    }else{
      $message = array('status' => 0, 'error' => __LINE__, 'message' => 'Not found');
    }
    echo json_encode($message); die;  
  }


  public function fun_attended(){
   
   $id = $this->input->post('id');
   $today = date('Y-m-d');

   $where = " where  id = '".$id."'";
   $result= $this->Common->select('appointments',$where);
   if(!empty($result)){

      $insertData = array('status' => 'attended');
      $where = array('id' => $id);
      $id =  $this->Common->update('appointments', $insertData, $where);
      $message = array('status' => 1, 'message' => 'status change successfully');   

    }else{
      $message = array('status' => 0, 'error' => __LINE__, 'message' => 'Not found');
    }
    echo json_encode($message); die;    
  
  }


}