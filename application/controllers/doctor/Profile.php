<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	var $user_id = '';
	var $parent_id = '';

	public function __construct() {
		parent::__construct();
		check_user_login();
		$this->user_id = $this->session->userdata('user_id');
		$this->parent_id = $this->session->userdata('parent_id');

		$this->load->helper(array());
		$this->load->library(array('form_validation'));
		$this->load->model(array('UserModel'));
		//$this->output->enable_profiler(TRUE);	
	}

    /**
	* Doctor
	*/
	public function index(){    
    $data['title'] = 'Profile Management';  
	$data['result'] = $this->UserModel->getDoctor($this->user_id);
    //$data['result'] = $this->UserModel->getDoctor();
	$this->load->view('doctor/user/doctor_profile',$data);
    }

    /**
    * Notification setting
    */
    public function setting(){
        $id = $this->user_id;
        $data['id'] = $id;
        $data['title'] = 'Notification setting';  
        $data['message'] = '';
        $post = $this->input->post();
        if($post && !empty($post))
        {

          
            $blanckArray = array('notifications' => '',
                                 'clinical_specialist' => '',
                                 );
            $post = array_merge($blanckArray,$post);
            
            $notifications = 0;
            if($post['notifications'] == 'on'){
                $notifications = 1;
            }
           
            $data = array('notifications' => $notifications,
                                 'clinical_specialist' => $post['clinical_specialist'],
                                 );
            $where = array('id'=>$id);
            $this->Common->update('user', $data, $where);

        }
       $data['result'] = $this->UserModel->getDoctor($this->user_id);
       //dd($data['result']);
       $this->load->view('doctor/user/notification_setting',$data);
    }

    /**
	* Edit Doctor
	*/    
	public function edit(){
		$id = $this->user_id;
		$data['id'] = $id;
		$data['title'] = 'Update Profile';  
		$data['message'] = '';
		$post = $this->input->post();

		if($post && !empty($post))
		{
			$post = $this->input->post();
			$target_dir = $this->config->item('root_path');   
			$imgName = '';
			if($_FILES){
				if(!empty($_FILES["profile_image"]["name"])){

                    $where = " where id = '".$id."'"; 
                    $user_result = $this->Common->select('user',$where);
                    $user_profile_image = $user_result[0]['profile_image'];

					$target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
					$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
					$imgName = time().'.'.$imageFileType;
					$target_file = $target_dir . $imgName;
                    @unlink($target_dir.$user_profile_image);
					move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
				}
			}  
			
			$data = array('full_name' => $post['clinic_name'],
				'phone_number' => $post['phone_number'],
				'password' => $post['password'],
				'email_address' => $post['email_address'],
				'status' => $post['status'],
				'update_dt' => time()
			);

			if(!empty($imgName)){
				$array2 = array('picture_url' => $imgName);
				$data = array_merge($data, $array2);
			}
         
			$doctor_data = array('doctor_name' => $post['doctor_name'],
				'doctor_phone_no' => $post['doctor_phone_no'],
				'doctor_email_id' => $post['doctor_email_id'],
				'doctor_about_us' => $post['doctor_about_us'],
				'clinic_name' => $post['clinic_name'],
                'clinic_address' => $post['clinic_address'],
				'doctor_status' => $post['office_status'],
				'lat' => $post['latitude'],
				'long' => $post['longitude'],
			);

			
			$where = array('id'=>$id);
			$this->Common->update('user', $data, $where);


			$where = array('user_id'=>$id);
			$this->Common->update('clinic_doctor_management', $doctor_data, $where);
           
            /*foreach ($post['doctor_specialties'] as  $value)
            {
                $doctor_data = array('specialties_id'=>$value);
                $where       = array('user_id'=>$id,'id'=>$ds_id);
                $this->Common->update('doctor_specialties', $doctor_data, $where);
            }*/
			
			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('doctor/profile', 'refresh');
		}
		
        $data['result']                    = $this->UserModel->getDoctor($id);
        $data['doctor_specialties']        = $this->UserModel->getDoctor_specialties($id);
        $data['getDoctor_specialties_all'] = $this->UserModel->getDoctor_specialties_all();
		$this->load->view('doctor/user/profile_edit',$data);
	}


	/**
	* Staff management
	*/
	public function staff(){

		$data['title'] = 'Staff Management'; 

		if($this->parent_id == 0){
			$user_id = $this->user_id;
		}else{
			$user_id = $this->user_id;
		} 
		$data['result'] = $this->UserModel->getStaff();
		$this->load->view('doctor/user/staff_view',$data);
		
	}


    /*
    * Chat Settings
    */
    public function chat_settings(){
        $id = $this->user_id;
        $data['message'] = '';
        $data['title'] = 'Chat Setting';
        $post = $this->input->post();        
        $where = " where id = '".$id."'"; 
        $data['result'] = $this->Common->select('user',$where);
        if (!empty($post)) {
            $update_value = array('chat_setting' => $post['chat_setting'] );//echo $data;die;
            $where = array('id' => $id);
            $this->Common->update('user',$update_value, $where);
            $this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
            redirect('doctor/profile/chat_settings', 'refresh');
        }
        $this->load->view('doctor/user/chat_settings',$data);
    }

    /**
    * staff add
    */
    public function staffAdd(){
    	
    	$data['title'] = 'Add Staff Profile';
    	
    	$post = $this->input->post();
    	if($post && !empty($post)){

    		$where = " where email_address = '".$post['email_address']."'"; 
    		$result = $this->Common->select('user',$where);
    		if(empty($result)){	
    			
    			$target_dir = $this->config->item('root_path');   
    			$imgName = '';
    			if($_FILES){
    				if($_FILES["profile_image"]["name"] != ''){
    					$target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
    					$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    					$imgName = time().'.'.$imageFileType;
    					$target_file = $target_dir . $imgName;
    					move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
    				}
    			}  

    			$parent_id = $this->parent_id;
    			if($this->parent_id == 0){
    				$parent_id = $this->user_id;
    			}
    			$time = time();
    			$ins_data = array('full_name' => $post['full_name'],
    				'email_address' => $post['email_address'],
    				'designation' => $post['designation'],
    				'picture_url' => $imgName,
    				'password' => $post['password'],
    				'status' => $post['status'],
    				'create_dt' => $time,
    				'update_dt' => $time,
    				'parent_id' => $parent_id,
    				'user_role' => 4
    			);
    			
    			$this->Common->insert('user', $ins_data);
    			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
    			redirect('doctor/profile/staff');
    		}else{
    			$data['message'] = EMAIL_ERROR_MSG;	
    		}
    	}
    	
    	$this->load->view('doctor/user/staff_add',$data);	
    }




    /**
    * staff edit
    */
    public function staffEdit(){
    	$id = $this->uri->segment(4);
    	$data['id'] = $id;
    	$id = safe_b64decode($id);

    	$data['title'] = 'Edit Staff Profile';
    	$where = "where id = '".$id."'";
    	$data['result'] = $this->Common->select('user', $where);
    	
    	$post = $this->input->post();
    	if($post && !empty($post)){
    		$old_img = $data['result'][0]['picture_url'];
    		$target_dir = $this->config->item('root_path');   
    		$imgName = '';
    		if($_FILES){
    			if(!empty($_FILES["profile_image"]["name"])){
    				$target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
    				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    				$imgName = time().'.'.$imageFileType;
    				$target_file = $target_dir . $imgName;
    				@unlink($target_dir.$old_img);
    				move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
    			}
    		}  
    		
    		/* 'email_address' => $post['email_address'], */
    		$data = array('full_name' => $post['full_name'],
    			'designation' => $post['designation'],
    			'password' => $post['password'],
    			'status' => $post['status'],
    			'update_dt' => time()
    		);

    		if(!empty($imgName)){
    			$array2 = array('picture_url' => $imgName);
    			$data = array_merge($data, $array2);
    		}

    		
    		$where = array('id' => $id);
    		$this->Common->update('user', $data, $where);
    		$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
    		redirect('doctor/profile/staff');
    	}
    	
    	$this->load->view('doctor/user/staff_edit',$data);	
    }
    /*
    function name :- office_off_on
    Result :- 
        true false
    Required :-
        post data
    */
    public function office_off_on()
    {
        if ($this->input->is_ajax_request())
        {
            $user_id = $this->input->post('id');
            $status  = $this->input->post('status');
            if ($status=='1')
                $status = 'Available';
            else
                $status = 'Unavailable';

            $set     = array('user_id' => $user_id,'doctor_status'=>$status);
            $where   = array('user_id' => $user_id);
            $result  = $this->Common->update('clinic_doctor_management',$set, $where);
            $message = 'Change successfully.';
        }
        else
        {
            $message = 'Get some errors';
        }
        echo json_encode($message);
        exit;
    }
    /*
    function name :- doctor_specialties
    Result :- 
        true false
    */
    public function doctor_specialties()
    {
       // echo $this->session->userdata('clinic_id');exit;
        $id = $this->session->userdata('clinic_id');
        $data['title']       = 'Doctor specialties Management';
        $data['doctor_specialties'] = $this->UserModel->getDoctor_specialties($id);  
        $data['doctor_name'] = $this->UserModel->getDoctor();
        $data['result']      = $this->UserModel->getDoctor_specialties_all();
        $this->load->view('doctor/user/add_doctor_specialties',$data);
    }

    /*
    function name :- single_doctor_specialties
    Result :- 
        true false
    */
    public function single_doctor_specialties()
    {
        if ($this->input->is_ajax_request())
        {
            $user_id = $this->input->post('id');
            $message = $this->UserModel->getDoctor_specialties($user_id);
        }
        else
        {
            $message = 'Get some errors';
        }
        echo json_encode($message);
        exit;
    }
    /*
    function name:-
        edit_doctor_specialties
    result :-
        UPDATE value 
    required :-
        @id
    */
    public function edit_doctor_specialties()
    {
        $data['title']   = 'Update Doctor Specialties';  
        $data['message'] = '';
        $post            = $this->input->post();

        if($post && !empty($post))
        {
            $post = $this->input->post();
            if(!empty($post['doctor_specialties']))
            { 
                $id = $post['doctor_name'] ? $post['doctor_name'] : '';
                /*delete case */
                $this->Common->delete('doctor_specialties', $id, 'user_id');
                /*insert case */
                foreach ($post['doctor_specialties'] as  $value)
                {
                    $doctor_data = array('user_id'=>$id,'specialties_id'=>$value);
                    $this->Common->insert('doctor_specialties', $doctor_data);
                }
            }
            $this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
            //redirect('doctor/profile/', 'refresh');
            redirect('doctor/profile/doctor_specialties', 'refresh');
            
        }
    }
}


