<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	var $user_id = '';

	public function __construct() {
		parent::__construct();
		check_user_login();
		$this->user_id = $this->session->userdata('user_id');

		$this->load->helper(array());
		$this->load->library(array('form_validation'));
		$this->load->model(array('UserModel'));
		//$this->output->enable_profiler(TRUE);	
	}

    /**
	* Doctor
	*/
	public function index()
	{    $data['title'] = 'Profile Management';  
	$data['result'] = $this->UserModel->getDoctor($this->user_id);
	$this->load->view('doctor/user/doctor_profile',$data);
}



    /**
	* Add Patient
	*/    
	public function doctorAdd(){
		
		$data['title'] = 'Add Doctor Information';  
		$data['message'] = '';
		$post = $this->input->post();

		if($post && !empty($post))
		{
			$post = $this->input->post();
			$time = time();
			$target_dir = $this->config->item('root_path');   
			$imgName = '';
			if($_FILES){
				$target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				$imgName = time().'.'.$imageFileType;
				$target_file = $target_dir . $imgName;
				move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
			}  

			$data = array('full_name' => $post['clinic_name'],
				'phone_number' => $post['phone_number'],
				'password' => $post['password'],
				'email_address' => $post['email_address'],
				'status' => $post['status'],
				'picture_url' => $imgName,
				'create_dt' => $time,
				'update_dt' => $time,
				'user_role' => 2,
				
			);

			$id = $this->Common->insert('user', $data);

			$doctor_data = array('user_id' => $id,
				'doctor_name' => $post['doctor_name'],
				'doctor_phone_no' => $post['doctor_phone_no'],
				'doctor_email_id' => $post['doctor_email_id'],
				'doctor_about_us' => $post['doctor_about_us'],
				'clinic_name' => $post['clinic_name'],
				'clinic_address' => $post['clinic_address'],
				'lat' => $post['latitude'],
				'long' => $post['longitude'],
			);

			$this->Common->insert('clinic_doctor_management', $doctor_data);


			$this->session->set_flashdata('success', INSERT_SUCCESS_MSG);
			redirect('admin/user/doctor', 'refresh');
		}
		
		$data['result'] = $this->UserModel->getDoctor($id);
		$this->load->view('admin/user/doctor_add',$data);
	}


    /**
	* Edit Patient
	*/    
	public function doctorEdit(){
		$id = $this->user_id;
		$data['id'] = $id;
		$id = safe_b64decode($id);
		$data['title'] = 'Update Doctor Information';  
		$data['message'] = '';
		$post = $this->input->post();

		if($post && !empty($post))
		{
			$post = $this->input->post();
			$target_dir = $this->config->item('root_path');   
			$imgName = '';
			if($_FILES){
				if(!empty($_FILES["profile_image"]["name"])){
					$target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
					$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
					$imgName = time().'.'.$imageFileType;
					$target_file = $target_dir . $imgName;
					move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file);
				}
			}  

			$data = array('full_name' => $post['clinic_name'],
				'phone_number' => $post['phone_number'],
				'password' => $post['password'],
				'email_address' => $post['email_address'],
				'status' => $post['status'],
				'update_dt' => time()
			);

			if(!empty($imgName)){
				$array2 = array('picture_url' => $imgName);
				$data = array_merge($data, $array2);
			}

			$doctor_data = array('doctor_name' => $post['doctor_name'],
				'doctor_phone_no' => $post['doctor_phone_no'],
				'doctor_email_id' => $post['doctor_email_id'],
				'doctor_about_us' => $post['doctor_about_us'],
				'clinic_name' => $post['clinic_name'],
				'clinic_address' => $post['clinic_address'],
				'lat' => $post['latitude'],
				'long' => $post['longitude'],
			);


			$where = array('id'=>$id);
			$this->Common->update('user', $data, $where);


			$where = array('user_id'=>$id);
			$this->Common->update('clinic_doctor_management', $doctor_data, $where);


			$this->session->set_flashdata('success', UPDATE_SUCCESS_MSG);
			redirect('admin/user/doctor', 'refresh');
		}
		
		$data['result'] = $this->UserModel->getDoctor($id);
		$this->load->view('admin/user/doctor_edit',$data);
	}

}