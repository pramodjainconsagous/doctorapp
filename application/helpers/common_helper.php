<?php

function dd($post){
	echo "<pre>";
	print_r($post);
	die();
}

/**
*	Genrate rand number
**/	
function genrateRandNumber(){
	return  random_string('alnum',20);
}

/**
	* User profile image 
	**/
	function image_check($image,$url,$imgCategory = 'user'){
		$CI = & get_instance();

		if($imgCategory == 'user'){

			$check_url = DOCUMENT_ROOT.'media/user/';
			$returl_url = $CI->config->item('base_url').'media/user/';   

		}else if($imgCategory == 'ads'){

			$check_url = DOCUMENT_ROOT.'media/ads/';
			$returl_url = $CI->config->item('base_url').'media/ads/';    

		}else{

			$check_url = DOCUMENT_ROOT.'media/user/';
			$returl_url = $CI->config->item('base_url').'media/user/';   
		}

		$filename="$check_url$image"; 

		if(!empty($image)){
			if(@getimagesize($filename)){
				return $returl_url.$image ;
			}else{
				return $returl_url.'default.png';
			}
		}else{
			return $returl_url.'default.png';
		}
	}


	function pass_encode($str){
		return base64_encode($str);
	}

	function pass_decode($str){
		return base64_decode($str);
	}

	function safe_b64encode($string) {

		$data = base64_encode($string);
		$data = str_replace(array('+','/','='),array('-','_',''),$data);
		return $data;
	}

	function safe_b64decode($string) {
		$data = str_replace(array('-','_'),array('+','/'),$string);
		$mod4 = strlen($data) % 4;
		if ($mod4) {
			$data .= substr('====', $mod4);
		}
		return base64_decode($data);
	}

/**
* User login check
**/
function check_user_login(){
	$CI = & get_instance();
	$user_id = $CI->session->userdata('user_id');
	if($user_id ==''){
		$CI->session->set_flashdata('warning', 'Your session has expired. Please log in....');
		redirect(base_url('Signin'), 'refresh');
	}else{
		return $user_id;
	}
}


/**
* reverse geocoding
*/
function reverseGeocoding($address){
  //$address = urlencode($address);
	$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?latlng=".$address."&sensor=true";
	$xml = simplexml_load_file($request_url);
	$status = $xml->status;
	if ($status=="OK") {
		$address = $xml->result->formatted_address;

		return $address;
	}
	return true;
}

/** 
* Time stamp to m-d-y convert
*/

function convert_datetime($timestamp,$hms = NULL){
	if(empty($timestamp)){
		return false;
	}
	if(!empty($hms)){
		return date('d-M-Y',$timestamp); 
	}
	return date('d-M-Y H:i:s', $timestamp);  
}

/**
* date time ago 
*/
function ago($timestamp = ''){
	if(empty($timestamp)){
		return false;
	}
 // $difference = time() - strtotime($timestamp);
	$difference = time() - $timestamp;
	$periods = array('second', 'minute', 'hour', 'day', 'week', 'month', 'years', 'decade');
	$lengths = array('60', '60', '24', '7', '4.35', '12', '10');

	for($j = 0; $difference >= $lengths[$j]; $j++) $difference /= $lengths[$j];

		$difference = round($difference);
	if($difference != 1) $periods[$j] .= "s";

	return "$difference $periods[$j] ago";
}

/**
 * Function:	sendEmailCI
 * params:	
 * 				$to			can be string, array or comma saparated value, 
 * 				$from 		array('name'=>'', email=>''), 
 * 				$subject 	string, 
 * 				$body 		string, 
 * 				$attachment array
 */
function sendEmailCI($to, $from, $subject = '', $body = '', $attachments = array(), $filePath = ''){
	$CI = & get_instance();
	$config = array();
	$config['useragent']	= "CodeIgniter";
	$config['mailpath'] 	= "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
	$config['protocol'] 	= "smtp";
	$config['smtp_host']	= "localhost";
	$config['smtp_port']	= "25";
	$config['mailtype']		= 'html';
	$config['charset'] 		= 'utf-8';
	$config['newline']  	= "\r\n";
	$config['wordwrap'] 	= TRUE;

	$CI->load->library('email');
	$CI->email->initialize($config);
	
	$site_email = config_item('no_reply'); 
	$site_name = config_item('site_name'); 
	
	$CI->email->from($site_email, $site_name);
	$CI->email->to($to);
	$CI->email->subject($subject);
	$CI->email->message($body);	
	
	if(!empty($attachments))
	{
		foreach($attachments as $attachment){
			$file_path = $filePath ? $filePath : config_item('root_url');
			$CI->email->attach($file_path.$attachment);
		}
	}
	
	$result = $CI->email->send();
	if($result){
		return $result;
	}else{
	 return false; //echo $CI->email->print_debugger();
	}
}

/**
* status get
*/
function status($sel = NULL){
	$arr = array('Active','inActive');
	$option = '';
	foreach($arr as $val){
		$selVal = '';
		if($sel == $val){
			$selVal = 'selected';	
		}	
		$option .= '<option value="'.$val.'" '.$selVal.' >'.ucfirst($val).'</option>';
	}
	return $option;	
}

function specialties_status($sel = NULL){
	$arr = array('Active','Deactive');
	$option = '';
	foreach($arr as $val){
		$selVal = '';
		if($sel == $val){
			$selVal = 'selected';	
		}	
		$option .= '<option value="'.$val.'" '.$selVal.' >'.ucfirst($val).'</option>';
	}
	return $option;	
}
/*
	function name :- 
		office_status
	return :-

*/
 function office_status($sel = NULL)
 {
 	$arr = array('Unavailable','Available');
	$option = '';
	foreach($arr as $val){
		$selVal = '';
		if($sel == $val){
			$selVal = 'selected';	
		}	
		$option .= '<option value="'.$val.'" '.$selVal.' >'.ucfirst($val).'</option>';
	}
	return $option;	
 }


/**
* Booking Time slot
*/
function booking_time_slot($sel = NULL){
	$option = '';
	$st = array('AM','PM');
	foreach($st as $val){
		for($i=1; $i<13; $i++){
			$k = 1;
			for($j=0; $j<60; $j+=15){
				$t = $i.':'.$j.' '.$val;
				if($k == 1){
					$t = $i.':00 '. $val;;
				}
				$selVal = '';
				if($t == $sel){
					$selVal = 'selected';	
				}
				$option .= '<option value="'.$t.'" '.$selVal.' >'.ucfirst($t).'</option>';
				$k++;
			}
		}
	}
	return $option;	
}



/**
* Booking sattus
*/
function booking_status($sel = NULL){

	$arr = array('scheduled' => 'Scheduled', 'confirmed' => 'Confirmed', 'current' => 'current','next' => 'next', 'attended' => 'Attended', 'cancel' => 'Cancel');
	$arr = array('scheduled' => 'Scheduled','confirmed' => 'Confirmed', 'current' => 'current', 'attended' => 'Attended', 'cancel' => 'Cancel');
	$option = '';
	foreach($arr as $key => $val){
		$selVal = '';
		if($sel == $key){
			$selVal = 'selected';	
		}	
		$option .= '<option value="'.$key.'" '.$selVal.' >'.ucfirst($val).'</option>';
	}
	return $option;	
}



/**
* subscription get
*/
function subscription($sel = NULL){
	$arr = array('0' => 'Free', '10' => '10 Days', '30' => '1 Month', '90' => '3 Month', '180' => '6 Month', '360' => '1 Year');
	$option = '';
	foreach($arr as $key => $val){
		$selVal = '';
		if($sel == $key){
			$selVal = 'selected';	
		}	
		$option .= '<option value="'.$key.'" '.$selVal.' >'.ucfirst($val).'</option>';
	}
	return $option;	
}

/**
* login attempts insert
*/
function login_attempts($user_id){
	$CI = & get_instance();
	$data = array('user_id' => $user_id,
		'ip_address' => $_SERVER['REMOTE_ADDR'],
		'login_dt' => time(),
	);
	$CI->db->insert('user_login_attempts', $data); 
	return true;
}


/**
* numbsr formate 
*/
function numberFormat($str){
	return number_format($str);
}


function strUcwords($str){
	return ucwords($str);
}


function inarray($array, $str){
	if (in_array($str, $array)) {
		return true;
	}
	return false;
}


/**
* get manage_treatment_type
*/
function getTreatmentType($sel = NULL){
	$CI = & get_instance();

	$sql = "SELECT * FROM manage_treatment_type";
	$query = $CI->db->query($sql);
	if($query->num_rows()>0){
		$option = '';
		foreach($query->result_array() as $val){
			$selVal = '';
			if($sel == $val['id']){
				$selVal = 'selected';	
			}	
			$option .= '<option value="'.$val['id'].'" '.$selVal.' >'.ucfirst($val['name']).'</option>';
		}
		return $option;	
	}
	
	
}

function getChatFriend($user_id){
	$CI = & get_instance();

	$where = ' WHERE user_id = "'.$user_id.'" || friend_id = "'.$user_id.'"';
	$sql = "SELECT * FROM chat_user_friend";
	if(!empty($where)){
		$sql .= " $where";
	}
		//echo $sql; die;
	$query = $CI->db->query($sql);
	if($query->num_rows()>0){
		$data='';
		foreach($query->result_array() as $val){
			if($val['user_id'] != $user_id){
				$data .= $val['user_id'].',';
			}else if($val['friend_id'] != $user_id){
				$data .= "'".$val['friend_id']."',";
			}  
		} 
		$all_friend_id = rtrim($data,','); 
		$sql = "SELECT id,full_name,phone_number,email_address,picture_url FROM user where id IN ($all_friend_id)";
		$query = $CI->db->query($sql);
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}   
	}else{
		return false;
	}
}


function getCurrentPatient($clinic_id){
	if(empty($clinic_id)){
		return false;
	}
	$CI = & get_instance();
    
    $CI->db->select('u.id,u.picture_url, u.full_name, a.time_slote',false);
	$CI->db->from('appointments a');
	$CI->db->join('user u', 'u.id = a.patient_id', 'LEFT');
	$today = date('Y-m-d');
    $CI->db->where('a.booking_date', $today);
    $CI->db->where('a.clinic_id', $clinic_id);
    $CI->db->limit(1);

	$query = $CI->db->get();	
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
}


function getAttendedPatient($clinic_id){
	if(empty($clinic_id)){
		return false;
	}
	$CI = & get_instance();
    
    $CI->db->select('u.picture_url, u.full_name, a.time_slote',false);
	$CI->db->from('appointments a');
	$CI->db->join('user u', 'u.id = a.patient_id', 'LEFT');
	$today = date('Y-m-d');
    $CI->db->where('a.booking_date', $today);
    $CI->db->where('a.clinic_id', $clinic_id);
    $CI->db->where('a.status', 'attended');

	$query = $CI->db->get();	
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
}


/**
* Total clinic appointment
*/
function clinicTotalAppointment($clinic_id){
	if(empty($clinic_id)){
		return 0;
	}
	$CI = & get_instance();
    
    $CI->db->select('a.*',false);
	$CI->db->from('appointments a');
	$CI->db->where('a.clinic_id', $clinic_id);
	
	$query = $CI->db->get();	
		if($query->num_rows()>0){
			return count($query->result_array());
		}else{
			return 0;
		}
}

/**
* Today clinic appointment
*/
function clinicTodayAppointment($clinic_id){
	if(empty($clinic_id)){
		return 0;
	}
	$CI = & get_instance();
    
    $CI->db->select('a.*',false);
	$CI->db->from('appointments a');
	$CI->db->where('a.clinic_id', $clinic_id);
    $today = date('Y-m-d');
    $CI->db->where('a.booking_date', $today);
    
	$query = $CI->db->get();	
		if($query->num_rows()>0){
			return count($query->result_array());
		}else{
			return 0;
		}
}


/**
* Total patient appointment
*/
function patientTotalAppointment($patient_id){
	if(empty($patient_id)){
		return 0;
	}
	$CI = & get_instance();
    
    $CI->db->select('a.*',false);
	$CI->db->from('appointments a');
	$CI->db->where('a.patient_id', $patient_id);
	
	$query = $CI->db->get();	
		if($query->num_rows()>0){
			return count($query->result_array());
		}else{
			return 0;
		}
}


function total_specialties_count($where = ''){
	$CI = & get_instance();

	$sql = "SELECT count('id') as count FROM doctor_specialties";
	if(!empty($where)){
	   $sql .= " $where";
	}
	//echo $sql; die;
	$query = $CI->db->query($sql);
    if($query->num_rows()>0){
        return $query->result_array()[0]['count'];
    }else{
        return false;
    }
}
function total_visiting_count($where = ''){
	$CI = & get_instance();

	$sql = "SELECT count('visiting_status') as count FROM appointments";
	if(!empty($where)){
	   $sql .= " $where";
	}
	//echo $sql; die;
	$query = $CI->db->query($sql);
    if($query->num_rows()>0){
        return $query->result_array()[0]['count'];
    }else{
        return false;
    }
}

function select($table,$where = ''){
	$CI = & get_instance();

	$sql = "SELECT count('id') as count FROM appointments";
	if(!empty($where)){
	   $sql .= " $where";
	}
	//echo $sql; die;
	$query = $CI->db->query($sql);
    if($query->num_rows()>0){
        return $query->result_array()[0]['count'];
    }else{
        return false;
    }
}


function getAppointmentNumber($user_id,$clinic_id){
	$CI = & get_instance();
    
    $CI->db->select('a.*',false);
	$CI->db->from('appointments a');
	$CI->db->where('a.clinic_id', $clinic_id);
    $today = date('Y-m-d');
    $CI->db->where('a.booking_date', $today);
    $CI->db->order_by("a.time_slote", "asc");
    $query = $CI->db->get();	
	
    $current_number = 1;
    $my_number = 1;
      $i = 0;
	    if($query->num_rows()>0){
			foreach($query->result_array() as $val){
				if( $i == 0){
				$i = 1;	
				}
				
                   if($val['status'] == 'current'){
                   	$current_number = $i;
                   }
                   if($val['patient_id'] == $user_id){
                   	$my_number = $i;
                   }
                   $i++;
			}

		}
		$message = array('current_number' => $current_number,'my_number' => $my_number);
		return $message;
}


/**
   * subscriber active user
   */
    function getSubscriberActiveUser(){
    $CI = & get_instance();

	$CI->db->select('us.*,u.full_name as clinic_name, u.phone_number as clinic_phone_number,u.status as user_ac_status, m.plan_name, m.amount',false);
	$CI->db->from('user_subscription_plan us');
	$CI->db->join('user u', 'u.id = us.user_id', 'LEFT');
	$CI->db->join('manage_subscription_plan m', 'm.id = us.subscription_id', 'LEFT');
	$today = time();
    $CI->db->where('us.subscription_end_dt >', $today);
  

	$query = $CI->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }


 /**
   * get group id
   */
    function getChatGroupId($receiver_id, $sender_id){
    $CI = & get_instance();
   
    $where = ' WHERE (user_id = "'.$receiver_id.'" || friend_id = "'.$receiver_id.'") && (user_id = "'.$sender_id.'" || friend_id = "'.$sender_id.'")'; 
        $sql = "SELECT * FROM chat_user_friend";
		if(!empty($where)){
		   $sql .= " $where";
		}

    $query = $CI->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return false;
        }
		
    }



?>