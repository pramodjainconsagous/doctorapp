<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/* Message */
$lang['warning'] = 'Warning! ';
$lang['please_fill_vaild_details'] = 'Please fill vaild details.';

/* login */
$lang['administrator_panel'] 			=	'Administrator panel';
$lang['email_id'] 						=	'Email id';
$lang['password'] 						=	'Password';
$lang['remember_me'] 					=	'Remember me';
$lang['sign_in'] 						=	'Sign In';
$lang['copyright'] 						=	'Copyright';
$lang['site_name'] 						=	'DoctorApp';
$lang['all_right_reserved'] 			=	'All right reserved.';

/* Sidebar */
$lang['home']					=	'Home';
$lang['sign_out']				=	'Sign Out';

// for doctor
$lang['dashboard'] 					=	'Dashboard';
$lang['setting'] 					=	'Setting';
	$lang['profile'] 				=	'Profile';
	$lang['notifications'] 			=	'Notifications';
	$lang['staff'] 					=	'Staff';
	$lang['chat_settings'] 			=	'Chat Settings';
$lang['appointment'] 				=	'Appointment';
	$lang['today_appointment'] 		=	'Today Appointment';
	$lang['appointment_records'] 	=	'Appointment Records';
$lang['chat_friends'] 				=	'CHAT YOU FRIENDS';

// for admin
$lang['manage_users']					= 'Manage Users';
	$lang['doctor_management']			= 'Doctor Management';
	$lang['patient_management']			= 'Patient Management';
	$lang['add_subscription']			= 'Add Subscription';
	$lang['subscriber_user']			= 'Subscriber User';
$lang['manage_setting']					= 'Manage Settings';
	$lang['manage_appointment_status']	= 'Manage Appointment Status';
	$lang['manage_type_of_visit']		= 'Manage Type of Visit';
	$lang['manage_subscription']		= 'Manage Subscription';
	$lang['doctor_specialties']			= 'Doctor Specialties';	
$lang['manage_ads']						= 'Manage Ads';	
$lang['add_doctor_specialties']			= 'Add Doctor Specialties';
	

/* Dashboard */
$lang['subscribe_clinic']		=	'Subscribe Clinic';
$lang['visiting_status']		=	'Visiting Status';

/* Placeholder */
$lang['enter_email_&_name'] = 'Enter Email and Name';

/* Content */
$lang['welcome_to_doctorApp']	= 'Welcome to DoctorApp';
$lang['clinic_turn_on_off']		= 'Clinic Turn on / off';
$lang['doctor_about_us']		= 'Doctor About Us';
$lang['patient']				= 'Patient';
$lang['doctor'] 				= 'Doctor';

$lang['on_appointment_day_only']	= 'On Appointment day Only';
$lang['open_for_any_time'] 			= 'Open For Any Time';
$lang['turn_off'] 					= 'Turn Off';

$lang['attended_patient']	= 'Attended Patient';
$lang['current_patient']	= 'Current Patient';
$lang['total_patient'] 		= 'Total Patient';
$lang['next_patient'] 		= 'Next Patient';

$lang['start_date'] 		= 'Start Date';
$lang['end_date'] 			= 'End Date';
$lang['total_appointment'] 	= 'Total Appointment';
$lang['today_appointment'] 	= 'Today Appointment';
$lang['subscription_name'] 	= 'Subscription Name';
$lang['doctor_name'] 		= 'Doctor Name';
$lang[''] 					= '';

/* Table header name */
$lang['id'] 			= 'ID';	
$lang['name'] 			= 'Name';
$lang['status'] 		= 'Status';
$lang['plan_amount']	= 'Plan Amount';
$lang['due_date'] 		= 'Due Date';
$lang['count'] 			= 'Count';
$lang['create_date']	= 'Create Date';

$lang['doctor_image'] 		= 'Doctor Image';
$lang['office_in_out']		=	'Office in/out';
$lang['doctor_name']		=	'Doctor Name';
$lang['doctor_address']		=	'Doctor Address';
$lang['doctor_number']		=	'Doctor Number';
$lang['clinic_number']		=	'Clinic Number';
$lang['clinic_name']		=	'Clinic Name';
$lang['doctor_phone_no']	=	'Doctor Phone No';
$lang['doctor_email']		=	'Doctor Email';
$lang['register_date']		=	'Register Date';
$lang['status']				=	'Status';
$lang['action']				=	'Action';
$lang['plan_name']			=	'Plan Name';
$lang['plan_description']	=	'Plan Description';
$lang['amount']				=	'Amount';
$lang['plan_duration']		=	'Plan Duration';

$lang['specialties_name']	=	'Specialties Name';
$lang['specialties_status']	=	'Specialties Status';
$lang['specialties_counts']	=	'Specialties counts';

$lang['ads_title']			=	'Ads Title';
$lang['ads_description']	=	'Ads Description';
$lang['ads_image']			=	'Ads Image';
$lang['ads_link']			=	'Ads Link';
$lang['ads_status']			=	'Ads Status';
$lang['specialties']		=	'Specialties';

$lang['patient_mobile_no_search ']	=	'Patient Mobile No Search ';
$lang['appointment_time_slot']		=	'Appointment Time Slot';
$lang['doctor_visit_status']		=	'Doctor Visit Status';

$lang['date']			=	'Date';
$lang['avtar']			=	'Avtar';
$lang['staff_name']		=	'Staff Name';
$lang['email']			=	'Email';
$lang['designation']	=	'Designation';
$lang['status']			=	'Status';

$lang['patient_avtar']		=	'Patient Avtar';
$lang['patient_name']		=	'Patient Name';
$lang['patient_phone_no']	=	'Patient Phone No';
$lang['time_slote']			=	'Time Slote';
$lang['observaciones']		=	'Observaciones';
$lang['appointment_date']	=	'Appointment Date';
$lang['appointment_status']	=	'Appointment Status';
$lang['visting_status']		=	'Visting Status';
$lang['activation_code']	=	'Activation Code';
$lang['acc_status']			=	'A/c Status';

$lang['subscription_start_date']	=	'Subscription Start Date';
$lang['subscription_due_date']	=	'Subscription Due Date';

$lang['visiting_name']	=	'Visiting Name';
$lang['appointment_name']	=	'Appointment Name';

/* Title */
$lang['profile_management']	=	'Profile Management';
$lang['notification_setting']	=	'Notification Setting';
$lang['staff_management']	=	'Staff Management';
$lang['chat_setting']	=	'Chat Setting';
$lang['today_appointment']	=	'Today Appointment';

$lang['doctor_management']	=	'Doctor Management';
$lang['clinic_phone_no']	=	'Clinic Phone No';
$lang['patient_management']	=	'Patient Management';
$lang['add_patient']	=	'Add Patient';
$lang['add_clinic_information']	=	'Add Clinic Information';
$lang['type_of_visit_status']	=	'Type Of Visit Status';
$lang['add_visit_status']	=	'Add Visit Status';
$lang['add_appointment_status']	=	'Add Appointment Status';
$lang['add_specialties']	=	'Add Specialties';
$lang['doctor_specialties_management']	=	'Doctor Specialties Management';

/* Button */
$lang['search_patient']		=	'Search Patient';
$lang['edit']				=	'Edit';
$lang['update']				=	'Update';
$lang['delete']				=	'Delete';
$lang['search']				=	'Search';
$lang['save']				=	'Save';
$lang['back']				=	'Back';

/* Note */
$lang['turnoff']			=	'Patient will not be able to send any chat message to this doctor on any day but will be able to see chat history if any.';
$lang['onlyappointment']	=	'Where a patient will be able to see chat history if any, but will be able to send new chat message only if its the appointment day.';
$lang['anytime']			=	'Open For Any Time';

$lang['for_windows']		=	'For windows: Hold down the control (ctrl) button to select multiple options ';
$lang['for_mac']			=	'For Mac: Hold down the command button to select multiple options';

/* Form field name */
$lang['full_name']					=	'Full Name';
$lang['phone_number']				=	'Phone Number';
$lang['doctor_email_id']			=	'Doctor Email Id';
$lang['clinic_address']				=	'Clinic Address';
$lang['clinic_image']				=	'Clinic Image';
$lang['clinic_phone_number']		=	'Clinic Phone Number';
$lang['login_email']				=	'Login Email';
$lang['login_password']				=	'Login Password';
$lang['visit_status_name']			=	'Visit Status Name';
$lang['appointment_status_name']	=	'Appointment Status Name';
$lang['subscription_description']	=	'Subscription Description';
$lang['subscription_status']		=	'Subscription Status';
$lang['subscription_amount']		=	'Subscription Amount';
$lang['subscription_duration']		=	'Subscription Duration';
$lang['']	=	'';
$lang['']	=	'';
$lang['']	=	'';