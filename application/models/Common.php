<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends CI_Model {


  	/* updating data in datebase */
  	public function update($table,$data,$where)
	{
		if(is_array($where)){
			foreach ($where as $key => $value){
			  $this->db->where($key, $value);
			}
		} 
		$this->db->update($table, $this->db->escape_str($data));
		return true;
	}

	/* insert data in datebase */
	public function insert($table,$data){
		$query = $this->db->insert($table, $data); 
		$id = $this->db->insert_id();
		return $id;
	}

	/* delete data in datebase */
	public function delete($table,$id,$col = 'id'){
		$this->db->where($col, $id);
		$query = $this->db->delete($table); 
		return $query;
	}


	/* select query */
    function select($table, $where ='',$coloumn = '*'){
        $sql = "SELECT $coloumn FROM $table";
		if(!empty($where)){
		   $sql .= " $where";
		}
		//echo $sql; die;
		$query = $this->db->query($sql);
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return false;
        }
    }
    

   /* user infomation and  user contact infomation get */
    function userContactInfomation($userId){

	$this->db->select('*',false);
	$this->db->from('user u');
	$this->db->where('u.status', 'Active');
	$this->db->where('u.id', $userId); 
	$query = $this->db->get();	
	    if($query->num_rows()>0){
	        return $query->result_array();
	    }else{
	        return false;
	    }
    }


   /**
	* image upload
	*/
	
	/*
	
    [img] => Array
        (
            [name] => rescue.png
            [type] => image/png
            [tmp_name] => E:\xampp\tmp\phpDD06.tmp
            [error] => 0
            [size] => 219177
        )

	*/
	public function imageUpload($files, $target_dir){
		$target_file = $target_dir . basename($files["name"]);
		$uploadOk = 1;
		$imageFileType = trim(strtolower(pathinfo($target_file,PATHINFO_EXTENSION)));
		
		
		// Allow certain file formats
		   //echo $imageFileType; die;
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
			   $msg = array('success' => 0, 'message' => 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.', 'name' => '');
				$uploadOk = 0;
			}
		$image_name = time().mt_rand().'.'.$imageFileType;
		if (move_uploaded_file($files["tmp_name"], $target_dir.$image_name)) {
			$msg = array('success' => 1, 'message' => 'The file has been uploaded.', 'name' => $image_name);
		} else {
			$msg = array('success' => 0, 'message' => 'Sorry, there was an error uploading your file.', 'name' => '');
		}
		return $msg;
	} 


/**
* product sell
*/

	public function product_sell($user_id = NULL){
	
	$this->db->select('p.id, p.quantity, p.create_dt, p.user_id, a.price, a.gst, a.article_title, a.article_content, a.article_img, c.name, c.mobile, c.address, ac.title as company_name, ab.title as brand_name',false);
	$this->db->from('product_sell p');
	$this->db->join('article a', 'a.id = p.article_id', 'LEFT');
	$this->db->join('customer c', 'c.id = p.user_id ', 'LEFT');
	$this->db->join('article_company ac', 'ac.id = a.company_id', 'LEFT');
	$this->db->join('article_brand ab', 'ab.id = a.brand_id', 'LEFT');
	
    
	
	if(!empty($user_id)){
		$this->db->where('p.user_id', $user_id); 
	}else{
	   $this->db->group_by('p.user_id'); 
	}
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
	}
	
	
/**
* invoice
*/
public function product_invoice($create_dt = NULL){
	
	$this->db->select('p.company_info,p.id,p.sell_price as price, p.quantity, p.gst, p.create_dt, p.user_id, a.article_title, a.article_content, a.article_img, u.user_name as name, u.mobile, u.address, ac.title as company_name, ab.title as brand_name',false);
	$this->db->from('product_sell p');
	$this->db->join('article a', 'a.id = p.article_id', 'LEFT');
	$this->db->join('user u', 'u.id = p.user_id ', 'LEFT');
	$this->db->join('article_company ac', 'ac.id = p.company_id', 'LEFT');
	$this->db->join('article_brand ab', 'ab.id = p.brand_id', 'LEFT');
	
    
	
	if(!empty($create_dt)){
		$this->db->where('p.create_dt', $create_dt); 
	}else{
	   $this->db->group_by('p.create_dt'); 
	}
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
	}	


/**
*
*/
public function getProductStock($company_id,$brand_id){
	
	$this->db->select('a.*,p.avaliable_quantity',false);
	$this->db->from('article a');
	$this->db->join('product_stock p', 'p.company_id = a.company_id', 'LEFT');
	$this->db->where('a.company_id', $company_id); 
	$this->db->where('a.brand_id', $brand_id); 
	
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
	}	
}



