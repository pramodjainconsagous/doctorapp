<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SettingModel extends CI_Model {


   /**
   * country infomation get 
   */
    function country($country_id = NULL){

	$this->db->select('c.*',false);
	$this->db->from('country c');
	if(!empty($country_id)){
		$this->db->where('c.countryid', $country_id); 
	}
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }
	
	
   /**
   * country state infomation get 
   */
    function state($country_id = NULL, $state_id = NULL){

	$this->db->select('s.*,c.country ',false);
	$this->db->from('country_regions s');
	$this->db->join('country c', 'c.countryid = s.countryid', 'LEFT');
	    
	if(!empty($country_id)){
		$this->db->where('s.countryid', $country_id); 
	}
	if(!empty($state_id)){
		$this->db->where('s.regionid', $state_id); 
	}
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }
	
	
	 /**
   * country state city infomation get 
   */
    function city($state_id = NULL,$city_id = NULL){

	$this->db->select('c.*,s.region,ct.country',false);
	$this->db->from('country_region_cities c');
	$this->db->join('country_regions s', 's.regionid = c.regionid', 'LEFT');
	$this->db->join('country ct', 'ct.countryid = c.countryid', 'LEFT');
	    
	if(!empty($state_id)){
		$this->db->where('s.regionid', $state_id); 
	}
	if(!empty($city_id)){
		$this->db->where('c.cityId', $city_id); 
	}
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }
	
	
	

}