<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserModel extends CI_Model {




   /**
   * user infomation get 
   */
    function userInfomation($user_id = NULL){

	$this->db->select('u.*',false);
	$this->db->from('user u');
	if(!empty($user_id)){
		$this->db->where('u.id', $user_id); 
	}
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }

   

    /**
   * Doctor Staff infomation get 
   */
    function getAppointments($appointments_id = NULL){

	$this->db->select('a.*,vs.id as visit_id,vs.name as visit_name , vs.status as visit_status ,u.id as patient_id, u.picture_url, u.full_name as patient_name, u.phone_number as patient_phone_number, u.picture_url as patient_picture_url, m.name as treatment_name',false);
	$this->db->from('appointments a');

	$this->db->join('manage_treatment_type m', 'm.id = a.description', 'LEFT');

	$this->db->join('user u', 'u.id = a.patient_id', 'LEFT');
	$this->db->join('manage_visit_status vs', 'vs.id = a.visiting_status', 'LEFT');

    $user_role = $this->session->userdata('user_role');
    $ses_user_id = $this->session->userdata('user_id');
    $parent_id = $this->session->userdata('parent_id');
    

    if(!empty($appointments_id)){
		$this->db->where('a.id', $appointments_id); 
	}else{

       if($user_role == 2){
    	$this->db->where('a.clinic_id', $ses_user_id);
       }else if($user_role == 4){
    	 $this->db->where('a.clinic_id', $parent_id);
       } 
	}

    $today = date('Y-m-d');
    $this->db->where('a.booking_date', $today);

	$query = $this->db->get();	
//	echo  $this->db->last_query();die;
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
		
    }

	/*
	* get code
	*/
	public function get_code($user_id,$friend_id){
		$this->db->select('*');
		$this->db->from('chat_user_friend');
		$this->db->where('user_id',$user_id);
		$this->db->where('friend_id',$friend_id);

		$query = $this->db->get();

		if($query->result()){
			return $query->result();
		}
		return false;
	}


    /**
    * Appointments history
    */
    public function appointments_history($s_date = NULL, $e_date = NULL){
        
        $this->db->select('a.*,u.id as patient_id, u.full_name as patient_name,vs.id as visit_id,vs.name as visit_name , vs.status as visit_status , u.phone_number as patient_phone_number, u.picture_url as patient_picture_url,m.name as treatment_name',false);
		$this->db->from('appointments a');
		$this->db->join('manage_treatment_type m', 'm.id = a.description', 'LEFT');
		$this->db->join('manage_visit_status vs', 'vs.id = a.visiting_status', 'LEFT');

		$this->db->join('user u', 'u.id = a.patient_id', 'LEFT');

    	$user_role = $this->session->userdata('user_role');
    	$ses_user_id = $this->session->userdata('user_id');
    	$parent_id = $this->session->userdata('parent_id');
    

	    if(!empty($appointments_id)){
			$this->db->where('a.id', $appointments_id); 
		}else{

	       if($user_role == 2){
	    	$this->db->where('a.clinic_id', $ses_user_id);
	       }else if($user_role == 4){
	    	 $this->db->where('a.clinic_id', $parent_id);
	       } 
		}

	    /*$today = date('Y-m-d');
	    $this->db->where('a.booking_date', $today);*/


	    $this->db->where('a.booking_date >=', $s_date);
		$this->db->where('a.booking_date <=', $e_date);

		$query = $this->db->get();	
			if($query->num_rows()>0){
				return $query->result_array();
			}else{
				return false;
			}
    
    }

    /**
   * Doctor Staff infomation get 
   */
    function getStaff($user_id = NULL){

	$this->db->select('u.*,ur.role as role_name',false);
	$this->db->from('user u');
	$this->db->join('user_role ur', 'ur.id = u.user_role', 'LEFT');
    $this->db->where('u.user_role', 4);
    
    $user_role = $this->session->userdata('user_role');
    $ses_user_id = $this->session->userdata('user_id');
    
    if($user_role == 2){
    	$this->db->where('u.parent_id', $ses_user_id);
    }

    if($user_role == 4){
    	 $this->db->where('u.id', $ses_user_id);
    }

	
	if(!empty($user_id)){
		$this->db->where('u.id', $user_id); 
	}
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }


   /**
   * Doctor infomation get 
   */
    function getDoctor($user_id = NULL){

	$this->db->select('u.*,ur.role as role_name, d.id as doctor_id, d.doctor_name, d.doctor_phone_no, d.doctor_email_id, d.clinic_name, d.clinic_address, d.lat,d.long,d.doctor_about_us,d.doctor_status',false);
	$this->db->from('user u');
	$this->db->join('user_role ur', 'ur.id = u.user_role', 'LEFT');

	$this->db->join('clinic_doctor_management d', 'd.user_id = u.id', 'LEFT');
	

    $this->db->where('u.user_role', 2);
	
	if(!empty($user_id)){
		$this->db->where('u.id', $user_id); 
	}
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }
   
/**
   * Appointment infomation get 
   */
    function getAppointment($clinic_id = NULL){

	$this->db->select('a.*,u.*',false);
	$this->db->from('appointments a');
	$this->db->join('user u', 'a.patient_id = u.id', 'LEFT');	

    $this->db->where('a.clinic_id',$clinic_id );
	
	// if(!empty($user_id)){
	// 	$this->db->where('', $user_id); 
	// }
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }

    /**
   * Patient infomation get 
   */
    function getPatient($user_id = NULL){

	$this->db->select('u.*,ur.role as role_name',false);
	$this->db->from('user u');
	$this->db->join('user_role ur', 'ur.id = u.user_role', 'LEFT');

	$this->db->where('u.user_role', 3);
	
	if(!empty($user_id)){
		$this->db->where('u.id', $user_id); 
	}
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }

   /**
   * user Buyer infomation get 
   */
    function getBuyerInfomation($user_id = NULL){

	$this->db->select('u.*,ur.role_name ',false);
	$this->db->from('user u');
	$this->db->join('user_role ur', 'ur.id = u.role_id', 'LEFT');
    $this->db->where('u.role_id', 3);
	
	if(!empty($user_id)){
		$this->db->where('u.id', $user_id); 
	}
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }


   /**
   * subscriber user
   */
    function getSubscriberUser(){

	$this->db->select('us.*,u.full_name as clinic_name, u.phone_number as clinic_phone_number,u.status as user_ac_status, m.plan_name, m.amount',false);
	$this->db->from('user_subscription_plan us');
	$this->db->join('user u', 'u.id = us.user_id', 'LEFT');
	$this->db->join('manage_subscription_plan m', 'm.id = us.subscription_id', 'LEFT');
  

	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }

    function getSubscriberUser_doctor(){

	$this->db->select('us.*,u.full_name as clinic_name, u.phone_number as clinic_phone_number,u.status as user_ac_status, m.plan_name, m.amount,cdm.doctor_name,cdm.doctor_phone_no',false);
	$this->db->from('user_subscription_plan us');
	$this->db->join('user u', 'u.id = us.user_id', 'LEFT');
	$this->db->join('manage_subscription_plan m', 'm.id = us.subscription_id', 'LEFT');
	$this->db->join('clinic_doctor_management cdm', 'cdm.user_id = u.id', 'LEFT');
  
	
	$query = $this->db->get();	
		if($query->num_rows()>0){
				return $query->result_array();
		}else{
				return false;
		}
		
    }
/*
	Function Name:-
		getDoctor_specialties
	Return : - 
		arrray	
	Required :-
		@id
*/
public function getDoctor_specialties($id)
{
    $this->db->select('ds.*');
    $this->db->from('doctor_specialties as ds');
    $this->db->where('ds.user_id',$id);
    $result = $this->db->get()->result_array();
    foreach ($result as $value)
    {
        $data[] = $value['specialties_id'];
    }
    //echo"<pre>";print_r($data);
    return $data;
}
/*public function getDoctor_specialties($id)
{
    $this->db->select('ds.*');
    $this->db->from('doctor_specialties as ds');
    $this->db->where('ds.user_id',$id);
    $result = $this->db->get()->result_array();
    return $result;
}*/
public function getDoctor_specialties_all()
{
    $this->db->select('mds.id,mds.name');
    $this->db->from('manage_doctor_specialties as mds');
    $result = $this->db->get()->result_array();
    return $result;
}


/*
	Function Name:-
		getvisiting_status
	Return : - 
		arrray	
	Required :-
		@id
*/
public function getvisiting_status($id)
{
   	$this->db->select('appointments.*,mvs.name,mvs.status as mvs_status',false);
	$this->db->from('appointments');
	$this->db->join('manage_visit_status mvs', 'appointments.visiting_status = mvs.id', 'LEFT');
	$this->db->where('appointments.clinic_id',$id);
	$this->db->where('appointments.booking_date',date('Y-m-d'));
	$this->db->group_by('appointments.visiting_status');
  	$result = $this->db->get()->result_array();
	$result = array_filter($result, function($var)
    {
        return !is_null($var['name']);
    });
    return $result;
}
}