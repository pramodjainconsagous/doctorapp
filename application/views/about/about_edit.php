<?php $this->load->view('common/header');?>
<?php $this->load->view('common/admin_header'); ?>
<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->

<div id="main" class="main">
  <div class="row"> 
    <!-- breadcrumb section -->
    <div class="ribbon">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
      </ul>
    </div>

    <div id="content">
      <div id="sortable-panel" class="">
        <div id="titr-content" class="col-md-12">
          <h2><?php echo ucwords($title);?></h2>
        </div>

      <!-- Admin over view .col-md-12 col-md-offset-3-->
        <div class="col-md-12">
          <div  class="panel panel-default">


          <div class="panel-heading">
          <div class="panel-title"> <i class="fa fa-edit"></i> <?php echo ucwords($title);?>
            <div class="bars pull-right"> <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a> <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a> </div>
          </div>
        </div>
            <div class="panel-body"> 

<!-- middel content section --> 
             <div class="row"> 
				<div class="panel-body">
				
				<form action="<?php echo base_url('user/editUser/'.$id);  ?>" role="form" id="form1" novalidate method="post" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
                                <?php if(!empty($message)){ ?>
									<div class="alert alert-danger"> <?php echo $message;  ?></div>
									
									<?php } ?>
                                </div>
                               </div>
							
                            
                            <div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										First Name <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="First name" class="form-control" id="first_name" name="first_name" value="<?php echo $result[0]['first_name']; ?>">
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										Last Name <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Last name" class="form-control" id="last_name" name="last_name" value="<?php echo $result[0]['last_name']; ?>">
								</div>
							</div>
							
							
                            
							
							
							
							
                            
                            <div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										Email <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Email" class="form-control" id="email_address" name="email_address" value="<?php echo $result[0]['email_address']; ?>">
								</div>
							</div>
                            
                            <div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										Password <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="password" placeholder="Password" class="form-control" id="password" name="password" value="<?php echo $result[0]['password']; ?>">
								</div>
							</div>
                            
                             <div class="col-md-2">
								<div class="form-group">
									<label class="control-label">
										Phone Number <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Phone number" class="form-control" id="phone_number" name="phone_number" value="<?php echo $result[0]['phone_number']; ?>">
								</div>
							</div>
							
							
							 <div class="col-md-5">
								<div class="form-group">
									<label class="control-label">
										Mailing address1 <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Mailing address1" class="form-control" id="mailing_address1" name="mailing_address1" value="<?php echo $result[0]['mailing_address1']; ?>">
								</div>
							</div>
							
							
							 <div class="col-md-5">
								<div class="form-group">
									<label class="control-label">
										Mailing address2 <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Mailing address2" class="form-control" id="mailing_address2" name="mailing_address2" value="<?php echo $result[0]['mailing_address2']; ?>">
								</div>
							</div>
							
					
					
					 <div class="col-md-3">
								<div class="form-group">
									<label class="control-label">
										Country <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Country" class="form-control" id="country" name="country" value="<?php echo $result[0]['country']; ?>">
								</div>
							</div>
							
							 <div class="col-md-3">
								<div class="form-group">
									<label class="control-label">
										State <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="State" class="form-control" id="state" name="state" value="<?php echo $result[0]['state']; ?>">
								</div>
							</div>
							
							 <div class="col-md-3">
								<div class="form-group">
									<label class="control-label">
										City <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="City" class="form-control" id="city" name="city" value="<?php echo $result[0]['city']; ?>">
								</div>
							</div>
							
							 <div class="col-md-3">
								<div class="form-group">
									<label class="control-label">
										Zipcode <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Zipcode" class="form-control" id="zipcode" name="zipcode" value="<?php echo $result[0]['zipcode']; ?>">
								</div>
							</div>		
							
							
							
							 <div class="col-md-2">
								<div class="form-group">
									<label class="control-label">
										School state <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="School state" class="form-control" id="school_state" name="school_state" value="<?php echo $result[0]['school_state']; ?>">
								</div>
							</div>		
							 <div class="col-md-4">
								<div class="form-group">
									<label class="control-label">
										Name of school <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Name of school" class="form-control" id="name_of_school" name="name_of_school" value="<?php echo $result[0]['name_of_school']; ?>">
								</div>
							</div>		
							 <div class="col-md-2">
								<div class="form-group">
									<label class="control-label">
										Graduation month <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Graduation month" class="form-control" id="graduation_month" name="graduation_month" value="<?php echo $result[0]['graduation_month']; ?>">
								</div>
							</div>		
							 <div class="col-md-2">
								<div class="form-group">
									<label class="control-label">
										Graduation year <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Graduation year" class="form-control" id="graduation_year" name="graduation_year" value="<?php echo $result[0]['graduation_year']; ?>">
								</div>
							</div>		
							 <div class="col-md-2">
								<div class="form-group">
									<label class="control-label">
										Board month <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Board month" class="form-control" id="board_month" name="board_month" value="<?php echo $result[0]['password']; ?>">
								</div>
							</div>		
							
							
							
						<div class="col-md-4">
                        <div class="form-group">
                          <label class="control-label"> Product Image<span class="symbol required" aria-required="true"></span> </label>
                          <input type="file" class="form-control" id="profile_image" name="profile_image" style="height: 100%;" accept="image/*">
                        </div>
                      </div>
					  
					  
					  <div class="col-md-4">
                        <div class="form-group">
                          <?php $img = image_check($result[0]['picture_url'],USER_IMAGE);?>
                          <img src="<?php echo $img; ?>" alt="<?php echo $result[0]['picture_url']; ?>" style="height: 60px; width: 60px;" > </div>
                      </div>
					  
							
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">
										Status <span class="symbol required" aria-required="true"></span>
									</label>
									<select class="form-control" name="status" id="status" >
                                    <?php echo status(); ?>
                                    </select>
								</div>
							</div>
                            
                            
						</div>
						<div class="row">
						<div class="col-md-7">
						<p></p>
						</div>
							<div class="col-md-2">
							  <a href="<?php echo base_url('user');?>" class="btn btn-light-grey btn-block">								
									<i class="fa fa-arrow-circle-left"></i> <?php echo BACK; ?>  							
							  </a>
							</div>
							<div class="col-md-3">
							  
								<button class="btn btn-success btn-block" type="submit">
									<?php echo UPDATE; ?>  <i class="fa fa-arrow-circle-right"></i>
								</button>
							
							</div>
						</div>
					</form>
					
				</div>
		    </div>
		<!-- end #content --> 
		</div>
	</div>
</div>
<!-- end of admin over view -->
		 </div>
	</div>
  </div>
  <!-- end .row --> 
</div>
<!-- ./end #main  -->


<?php $this->load->view('common/footer_content');?>
<script type="text/javascript">
	/* $('#example1').dataTable({
		    	responsive: true
		    });*/
	$(document).ready(function() {
	
		        var form = $('#form1');
		        var errorHandler1 = $('.errorHandler', form);
		        var successHandler1 = $('.successHandler', form);
		        form.validate({
		            errorElement: "span", // contain the error msg in a span tag
		            errorClass: 'help-block',
		            errorPlacement: function (error, element) { // render error placement for each input type
		                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
		                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
		                } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
		                    error.insertAfter($(element).closest('.form-group').children('div'));
		                } else {
		                    error.insertAfter(element);
		                    // for other inputs, just perform default behavior
		                }
		            },
				  ignore: "",
		            rules: {
		                first_name: {
		                    required: true
		                },
						 last_name: {
		                    required: true
		                },
						 email_address: {
		                    required: true,
							email: true,
		                },
						 password: {
		                    required: true,
							 minlength: 5,
		                },
						 status: {
		                    required: true
		                }
						
		              },
		            invalidHandler: function (event, validator) { //display error alert on form submit
		                successHandler1.hide();
		                errorHandler1.show();
		            },
		            highlight: function (element) {
		                $(element).closest('.help-block').removeClass('valid');
		                // display OK icon
		                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
		                // add the Bootstrap error class to the control group
		            },
		            unhighlight: function (element) { // revert the change done by hightlight
		                $(element).closest('.form-group').removeClass('has-error');
		                // set error class to the control group
		            },
		            success: function (label, element) {
		                label.addClass('help-block valid');
		                // mark the current input as valid and display OK icon
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
		            },
		            submitHandler: function (form) {
		                successHandler1.show();
		                errorHandler1.hide();
		                // submit form
		                 form.submit();
		            }
		        });
	});
</script>
<?php $this->load->view('common/footer');?>
