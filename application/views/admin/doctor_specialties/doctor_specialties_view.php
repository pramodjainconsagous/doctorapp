 <?php $this->load->view('common/header');?>
 <?php $this->load->view('common/admin_header'); ?>
 <!-- sidebar menu -->
 <?php $this->load->view('common/sidebar'); ?>
 <!-- /end #sidebar -->
 <!-- main content  -->
 
 <div id="main" class="main">
  <div class="row">
    <!-- breadcrumb section -->
    <div class="ribbon">
      <ul class="breadcrumb">
        <li>
          <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a>
        </li>
      </ul>
    </div>
    <?php $this->load->view('common/message'); ?>
 <!-- main content -->
 <div id="content">
   <div id="sortable-panel" class="">
      <div id="titr-content" class="col-md-12">
        <h2><?php echo ucwords($title); ?></h2>
        <h5>&nbsp;</h5>
        <div class="actions">
          <a href="<?php echo base_url('admin/doctor_specialties/doctor_specialties_add');?>" class="btn btn-success  has-ripple"> <?php echo ADD_NEW; ?> </a>
        </div>
      </div>
     <!-- Admin over view .col-md-12 -->
     <div class="col-md-12 ">
      <div  class="panel panel-default">
        <div class="panel-body"> <i class="glyphicon glyphicon-stats"></i> <b><?php echo ucwords($title); ?>
          <hr>
          <div class="row">
            <!-- progress section -->
            <div class="panel-body">
              <table id="example1" class="table table-striped table-bordered width-100 cellspace-0" >
                <thead>
                  <tr>
                    <th>Specialties Name</th>
                    <th>Specialties Status</th>
                    <th>Specialties counts</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php if($result)
                { 
                  foreach($result as $val)
                  {
                    $ids       = $val['id'];
                    $where     = "where specialties_id=$ids";
                    $get_count = total_specialties_count($where);
                    $id        = safe_b64encode($val["id"]); 
                  ?>
                    <tr id="ads_<?php echo $id; ?>">
                     <td><?php echo strUcwords($val['name']); ?></td>
                     <td><?php echo $val['status']; ?></td>
                     <td><?php echo $get_count; ?></td>
                      <td>
                        <div class  =" action-buttons"> 
                          <a title        ="Edit" class="btn btn-info" href="<?php echo base_url('admin/doctor_specialties/doctor_specialties_edit/'.$id) ?>"> 
                           Edit <i class   ="fa fa-pencil" aria-hidden="true"></i>
                          </a>  
                        </div>
                      </td>
                    </tr>
                  <?php
                  }
                }?>
                </tbody>
              </table>
            </div>
            <!-- ./preogress section -->
          </div>
        </div>
      </div>
       <!-- end panel -->
     </div>
     <!-- /end Admin over view .col-md-12 -->
   </div>
   <!-- end col-md-12 -->
 </div>
 <!-- end #content -->
 </div>
 <!-- end .row -->
 </div>
 <!-- ./end #main  -->
 <?php $this->load->view('common/footer_content');?>
 <script type="text/javascript">
 $('#example1').dataTable({
 responsive: true
 });
 
 
 var del_ads = function(id) {
 var result = confirm("Want to delete?");
 if (result) {
 //Logic to delete the item
 var url = base_url+'admin/subscription/subscription_delete';  
 $.ajax({
 type: "POST",
 url: url,
 data: {'id': id},
 })
 .done(function(result){
 
 var result = JSON.parse(result);
 if( parseInt(result.status) == 1 ) {
 $('#ads_'+id).hide();
 }
 var message = result.message;
 alert(message);
 }); 
 }
 };  
 
 </script>
 <?php $this->load->view('common/footer');?>
