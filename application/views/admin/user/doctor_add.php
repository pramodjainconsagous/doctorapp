<?php $this->load->view('common/header');?>
<?php $this->load->view('common/admin_header'); ?>
<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->

<div id="main" class="main">
	<div class="row"> 
		<!-- breadcrumb section -->
		<div class="ribbon">
			<ul class="breadcrumb">
				<li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
			</ul>
		</div>

		<div id="content">
			<div id="sortable-panel" class="">
				<div id="titr-content" class="col-md-12">
					<h2><?php echo ucwords($title);?></h2>
				</div>

				<!-- Admin over view .col-md-12 col-md-offset-3-->
				<div class="col-md-12">
					<div  class="panel panel-default">


						<div class="panel-heading">
							<div class="panel-title"> <i class="fa fa-edit"></i> <?php echo ucwords($title);?>
								<div class="bars pull-right"> <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a> <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a> </div>
							</div>
						</div>
						<div class="panel-body"> 

							<!-- middel content section --> 
							<div class="row"> 
								<div class="panel-body">

									<form action="<?php echo base_url('admin/user/doctorAdd');  ?>" role="form" id="form1" novalidate method="post" enctype="multipart/form-data">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<?php if(!empty($message)){ ?>
													<div class="alert alert-danger"> <?php echo $message;  ?></div>

													<?php } ?>
												</div>
											</div>

											<div class="col-md-6 col-sm-4 col-xs-12">
												<div class="form-group">
													<label class="control-label">
														Doctor Name <span class="symbol required" aria-required="true"></span>
													</label>
													<input type="text" placeholder="Doctor Name" class="form-control" id="doctor_name" name="doctor_name" value="">
												</div>
											</div>

											<div class="col-md-3 col-sm-4 col-xs-6 full-xs">
												<div class="form-group">
													<label class="control-label">
														Doctor Phone No <span class="symbol required" aria-required="true"></span>
													</label>
													<input type="text" placeholder="Doctor phone no" class="form-control" id="doctor_phone_no" name="doctor_phone_no" value="">
												</div>
											</div>


											<div class="col-md-3 col-sm-4 col-xs-4 full-xs">
												<div class="form-group">
													<label class="control-label">
														Doctor Email Id <span class="symbol required" aria-required="true"></span>
													</label>
													<input type="text" placeholder="Doctor email id" class="form-control" id="doctor_email_id" name="doctor_email_id" value="">
												</div>
											</div>



											<!-- left side -->
											<div class="col-md-6 col-sm-6 col-xs-6 full-xs">
												<div class="form-group">
													<label class="control-label">
														Doctor About Us <span class="symbol required" aria-required="true"></span>
													</label>
													<textarea placeholder="Doctor about us"  class="form-control" id="doctor_about_us" name="doctor_about_us"></textarea>

												</div>

												<div class="form-group row">
													<div class="col-md-9">
														<label class="control-label"> 
															Clinic Image<span class="symbol required" aria-required="true"></span> </label>
															<input type="file" class="form-control" id="profile_image" name="profile_image" style="height: 100%;" accept="image/*">
														</div>
														<div class="col-md-3">
															<?php $img = image_check($result[0]['picture_url'],USER_IMAGE);?>
															<img src="<?php echo $img; ?>" alt="" style="height: 60px; width: 60px;" > 
														</div>
													</div>

													<div class="form-group">
														<label class="control-label">
															Clinic Name <span class="symbol required" aria-required="true"></span>
														</label>
														<input type="text" placeholder="Clinic name" class="form-control" id="clinic_name" name="clinic_name" value="">
													</div>

													<div class="form-group">
														<label class="control-label">
															Clinic Phone Number <span class="symbol required" aria-required="true"></span>
														</label>
														<input type="text" placeholder="Phone number" class="form-control" id="phone_number" name="phone_number" value="">
													</div>
												</div>

												<!-- left side end -->

												<!-- right side -->
												<div class="col-md-6 col-sm-6 col-xs-6 full-xs">

													<div class="form-group">
														<label class="control-label">
															Clinic Address <span class="symbol required" aria-required="true"></span>
														</label>
														<input type="text" placeholder="Clinic address" class="form-control" id="address" name="clinic_address" value="">


														<input type="hidden" class="form-control" id="latitude" name="latitude" value="">
														<input type="hidden" class="form-control" id="longitude" name="longitude" value="">

													</div>
													<div class="form-group" id="map" style="width: 100%; height: 250px;"></div>

												</div>
												<!-- right side end -->






												<div class="col-md-4 col-sm-4 col-xs-12">
													<div class="form-group">
														<label class="control-label">
															Login Email <span class="symbol required" aria-required="true"></span>
														</label>
														<input autocomplete="off" type="text" placeholder="Email" class="form-control" id="email_address" name="email_address" value="">
													</div>
												</div>

												<div class="col-md-4 col-sm-4 col-xs-6 full-xs">
													<div class="form-group">
														<label class="control-label">
															Login Password <span class="symbol required" aria-required="true"></span>
														</label>
														<input autocomplete="off" type="password" placeholder="Password" class="form-control" id="password" name="password" value="">
													</div>
												</div>

												<div class="col-md-4 col-sm-4 col-xs-6 full-xs">
													<div class="form-group">
														<label class="control-label">
															A/c Status <span class="symbol required" aria-required="true"></span>
														</label>
														<select class="form-control" name="status" id="status" >
															<?php echo status(); ?>
														</select>
													</div>
												</div>

											</div>
											<div class="row">
												<div class="col-md-2 col-sm-4 col-xs-6 full-xs bottom-margin-xs">
													<a href="<?php echo base_url('admin/user/doctor');?>" class="btn btn-light-grey btn-block">					
														<i class="fa fa-arrow-circle-left"></i> <?php echo BACK; ?>  							
													</a>
												</div>
												<div class="col-md-3 col-sm-6 col-xs-6 full-xs">
													<button class="btn btn-success btn-block" type="submit">
														<?php echo SAVE; ?>  <i class="fa fa-arrow-circle-right"></i>
													</button>
												</div>
											</div>
										</form>

									</div>
								</div>
								<!-- end #content --> 
							</div>
						</div>
					</div>
					<!-- end of admin over view -->
				</div>
			</div>
		</div>
		<!-- end .row --> 
	</div>
	<!-- ./end #main  -->


	<?php $this->load->view('common/footer_content');?>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA02ImK47DaSgcOQjNMyPnKQ9hDZgTT-x4&libraries=places&callback=initialize"
	async defer></script>





	<script type="text/javascript" src="<?php echo base_url('assets/js/custom/address_change_map.js'); ?>"></script>


	<script type="text/javascript">



		function initialize() {
			var latlng = new google.maps.LatLng(51.5073509,-0.12775829999998223);
			var map = new google.maps.Map(document.getElementById('map'), {
				center: latlng,
				zoom: 13
			});
			var marker = new google.maps.Marker({
				map: map,
				position: latlng,
				draggable: true,
				anchorPoint: new google.maps.Point(0, -29)
			});
			var input = document.getElementById('address');
//map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
var geocoder = new google.maps.Geocoder();
var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
var infowindow = new google.maps.InfoWindow();   
autocomplete.addListener('place_changed', function() {
	infowindow.close();
	marker.setVisible(false);
	var place = autocomplete.getPlace();
	if (!place.geometry) {
		window.alert("Autocomplete's returned place contains no geometry");
		return;
	}

// If the place has a geometry, then present it on a map.
if (place.geometry.viewport) {
	map.fitBounds(place.geometry.viewport);
} else {
	map.setCenter(place.geometry.location);
	map.setZoom(17);
}

marker.setPosition(place.geometry.location);
marker.setVisible(true);          

bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
infowindow.setContent(place.formatted_address);
infowindow.open(map, marker);

});
// this function will work on marker move event into map 
google.maps.event.addListener(marker, 'dragend', function() {
	geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			if (results[0]) {        
				bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
				infowindow.setContent(results[0].formatted_address);
				infowindow.open(map, marker);
			}
		}
	});
});
}
function bindDataToForm(address,lat,lng){
	document.getElementById('address').value = address;
	document.getElementById('latitude').value = lat;
	document.getElementById('longitude').value = lng;
}
google.maps.event.addDomListener(window, 'load', initialize);





$(document).ready(function() {

	var form = $('#form1');
	var errorHandler1 = $('.errorHandler', form);
	var successHandler1 = $('.successHandler', form);
	form.validate({
errorElement: "span", // contain the error msg in a span tag
errorClass: 'help-block',
errorPlacement: function (error, element) { // render error placement for each input type
if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
	error.insertAfter($(element).closest('.form-group').children('div').children().last());
} else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
	error.insertAfter($(element).closest('.form-group').children('div'));
} else {
	error.insertAfter(element);
// for other inputs, just perform default behavior
}
},
ignore: "",
rules: {
	doctor_name: {
		required: true,
		minlength: 3,
	},
	doctor_phone_no: {
		required: true,
		minlength: 6,
	},
	doctor_email_id: {
		required: true,
		email:true
	},
	clinic_name: {
		required: true,
		minlength: 4,
	},
	clinic_address: {
		required: true,
		minlength: 5,
	},
	phone_number: {
		required: true,
		minlength: 6,
		maxlength:12
	},
	email_address: {
		required: true,
		email:true
	},
	password: {
		required: true,
		minlength: 5,
	},
	status: {
		required: true
	}

},
invalidHandler: function (event, validator) { //display error alert on form submit
	successHandler1.hide();
	errorHandler1.show();
},
highlight: function (element) {
	$(element).closest('.help-block').removeClass('valid');
// display OK icon
$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
// add the Bootstrap error class to the control group
},
unhighlight: function (element) { // revert the change done by hightlight
	$(element).closest('.form-group').removeClass('has-error');
// set error class to the control group
},
success: function (label, element) {
	label.addClass('help-block valid');
// mark the current input as valid and display OK icon
$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
},
submitHandler: function (form) {
	successHandler1.show();
	errorHandler1.hide();
// submit form
form.submit();
}
});
});
</script>
<?php $this->load->view('common/footer');?>
