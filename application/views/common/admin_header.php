
<header id="header">
  <nav class="navbar navbar-default nopadding" > 
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
      <button type="button" id="menu-open" class="navbar-toggle menu-toggler pull-left"> <span class="sr-only">Toggle sidebar</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="#" id="logo-panel"> 
        <img src="<?php echo $this->config->item('site_url') ?>assets/img/logo.png" title="<?php echo $this->config->item('site_name').' '.$this->config->item('site_full_name'); ?>" alt="<?php echo $this->config->item('site_name').' '.$this->config->item('site_full_name'); ?>" style="height:35px;"> 
        <?php //echo $this->config->item('site_name'); ?>
      </a> </div>
    <form action="#" class="form-search-mobile pull-right">
      <input id="search-fld" class="search-mobile" type="text" name="param" placeholder="Search ...">
      <button id="submit-search-mobile" type="submit"> <i class="fa fa-search"></i> </button>
      <a href="#" id="cancel-search-mobile" title="Cancel Search"><i class="fa fa-times"></i></a>
    </form>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li id="search-show-li" class="dropdown"> <a href="#" id="search-mobile-show" class="dropdown-toggle" > <i class="fa fa-search"></i> </a> </li>
        
        <!-- notification -->
        
        <!-- end notification -->
        
        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img alt="Golabi Avatar Admin" src="<?php echo $this->config->item('site_url') ?>assets/img/avatars/avatar.png" height="50" width="50" class="img-circle" /> <?php echo ucwords($this->session->userdata('name')); ?> <strong class="caret"></strong> </a>
          <ul class="dropdown-menu">
            <!-- <li> <a href="<?php echo base_url('admin/user/patientEdit/'.safe_b64encode($this->session->userdata('user_id'))); ?>">Profile<span class="fa fa-user pull-right"></span></a> </li> 
            <li class="divider"> </li>-->
            <li> <a href="<?php echo base_url('signin/logout'); ?>"> <span class="fa fa-power-off"></span> Sign out</a> </li>
          </ul>
        </li>
        
        
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li id="fullscreen-li"> <a href="#" id="fullscreen" class="dropdown-toggle" > <i class="fa fa-arrows-alt"></i> </a> </li>
        <li id="side-hide-li" class="dropdown"> <a href="#" id="side-hide" class="dropdown-toggle" > <i class="fa fa-reorder"></i> </a> </li>
      </ul>
    </div>
  </nav>
</header>
