<?php if($_SESSION['user_role'] == 2 || $_SESSION['user_role'] == 4){ ?>
<div class="col-xs-12 col-sm-12 pull-right" style="float: right !important;position: fixed;">
	<?php include(DOCUMENT_ROOT.'chatapp/index.html'); ?>
</div>
<?php } ?>

<!-- footer -->
<div class="page-footer">				
	<div class="text-center">
		
		<p>Copyright <?php echo date('Y').' '. $this->config->item('site_name') ?>. All right reserved.</p>
		
		
	</div>			
</div>
<!-- /footer -->
</div>


<!-- General JS script library-->
<!-- <script type="text/javascript" src="<?php echo base_url('assets/vendors/jquery/jquery.min.js'); ?>"></script> -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.2/socket.io.js"></script>



<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-ui/js/jquery-ui.min.js"></script>	
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/bootstrap/js/bootstrap.min.js"></script>						
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-searchable/js/jquery.searchable.min.js"></script>									
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js"></script>																		

<!-- Yeptemplate JS Script --><!-- Please use *.min.js in production -->
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/js/yep-script.js"></script>	


<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-datatables/js/jquery.dataTables.min.js"></script>	
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-datatables/js/dataTables.bootstrap.min.js"></script>	
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-datatables/js/dataTables.responsive.min.js"></script>	
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-datatables/js/dataTables.tableTools.min.js"></script>	

<!-- jquery validation js script file -->
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-validation/js/jquery.validate.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=o071gjf9aj18fw3ui42pi1ftkcd0pr16z8xudrgimdfpwoij"></script>

<link rel="stylesheet" href="<?php echo $this->config->item('site_url') ?>assets/js/thickbox/lightbox.css">
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/js/thickbox/lightbox.js"></script>	


<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/js/jquery.lazy.js"></script>	


<!-- Related JavaScript Library to This Pagee -->
<?php 
if(isset($javascript) && !empty($javascript)){
	$i=0;
	foreach($javascript as $value){$i++;
		$tab = ($i!=1)?"\t\t ":"";
		echo $tab.'<script src="'.$this->config->item('site_url').$value.'" type="text/javascript"></script>'."\n";
	}
}
if(isset($script) && !empty($script)){
	foreach($script as $value){
		echo $value;
	}
}
?>	

<!-- End Related JavaScript Library to This Pagee -->
<script type="text/javascript">
	$(function() {
		$('.lazy').lazy();
	});
	jQuery(document).ready(function() {
		
	})
	
	function getBrandInfo(id,sel_id){
		jQuery('#brand_image').empty().prepend('Please wait....<img id="Please wait...." src="'+base_url+'/assets/images/loading-small.gif"   />');  
		var url = base_url+'ajax/getBrandImage';  
		$.ajax({
			type: "POST",
			url: url,
			data: {'id': id}
		})
		.done(function(msg) {
			if(typeof(sel_id) != "undefined" && sel_id != ''){
				jQuery('#brand_image_'+sel_id).empty().prepend('<a href="'+msg+'" title="" data-lightbox="roadtrip"><img class="lazy" id="theImg" src="'+msg+'" height="58" width="80"  /></a>')
			}else{
				jQuery('#brand_image').empty().prepend('<a href="'+msg+'" title="" data-lightbox="roadtrip"><img class="lazy" id="theImg" src="'+msg+'" height="80" width="80"  /></a>')
			}
		});
	}	
	
	
	
	
	function notifiction_read(id,url){
		var jqxhr =
		$.ajax({
			url: base_url+"ajax/notification_unread",
			type : "POST",
			data: {
				id : id,
				url : url
			}
		})
		.done (function(data) {
			$(location).attr('href', url);
		})
		.fail (function()  { 
			alert("Error "); 
		});	
	}
</script>


<script>
	var app = angular.module('myApp', []);
	$('.chat_main_div').hide();
	app.controller('myCtrl', function($scope, $http) {
		$scope.message = '';
		$scope.senderName = '';
		$scope.senderId = '';
		$scope.receiverId ='';
		$scope.groupId = '';
		$scope.messages = [];
		$scope.receiver_name = '';
		
		var socket = io.connect('https://doctorappchatdemo.herokuapp.com/');
		socket.on('connect', function() {
        //var groupId = "5";
        //socket.emit('addUser', groupId);
    });

		socket.on('updatechat', function(history) {
			$scope.messagess = history;
			console.log($scope.messagess);
			$scope.$apply();
			$('.usermessage-box').animate({
                    scrollTop: $('.usermessage-box')[0].scrollHeight}, 2000);
            

		});

		$scope.residentchat = {};
		$scope.send = function() {
			$scope.message = $scope.residentchat.message;
			if($scope.message){
			socket.emit('sendchat', $scope.message, $scope.senderName, $scope.senderId,  $scope.receiverId,$scope.groupId);
			$scope.residentchat.message = '';
		    }
		}
		$scope.getChat = function(receiver_id, sender_id, group_id, senderName, receiver_name) {
			$('.chat_main_div').show();
        	 // $('.glyphicon-comment').html(senderName);

        	 socket.emit('addUser', group_id);
        	 $scope.message = $scope.residentchat.message;
        	 $scope.senderName = senderName;
        	 $scope.senderId = sender_id;
        	 $scope.receiverId = receiver_id;
        	 $scope.groupId = group_id;
        	 $scope.receiver_name = receiver_name;
        	};
        	
        });

	$(".icon_close").click(function(){
		$('.chat_main_div').hide();
	});

</script>



