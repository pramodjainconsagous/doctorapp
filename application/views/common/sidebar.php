<style type="text/css">
#contact-list-search::placeholder{
  color : #000;
}

/*@-webkit-keyframes blinker {
  from {opacity: 1;}
  to {opacity: 0.0;}
}*/

span.blink {
  background-color: #fff;
  padding: 3px;
  border-radius: 50px;
  font-size: 10px;
/*  -webkit-animation-name: blinker;
  -webkit-animation-duration: 0.9s;
  -webkit-animation-iteration-count:infinite;
  -webkit-animation-timing-function:ease-in-out;
  -webkit-animation-direction: alternate;*/
}
</style>
<?php

?>
<div id="sidebar" class="sidebar" >
  <div class="tabbable-panel">
    <div class="tabbable-line">
      <!-- <ul class="nav nav-tabs nav-justified">
       
        <li id="tab_menu_a" class="active pull-left"> 
          <a href="#tab_menu_1" data-toggle="tab"> <i class="fa fa-reorder"></i> </a> 
        </li>

        <li id="contact-tab" class="">
          <a href="" data-target="#tab_contact_2" data-toggle="tab" aria-expanded="false">
            <i class="fa fa-user">gg</i>
          </a>
        </li>
      </ul> -->


      <ul class="nav nav-tabs nav-justified">
        <li id="tab_menu_a" class="active">
          <a href="" data-target="#tab_menu_1" data-toggle="tab" aria-expanded="true">
            <i class="fa fa-reorder"></i>
          </a>
        </li>
        <li id="contact-tab" class="">
          <a href="" data-target="#tab_contact_2" data-toggle="tab" aria-expanded="false">
            <i class="fa fa-user"></i>
          </a>
        </li>

      </ul>


      <?php $class = strtolower($this->router->fetch_class());
      $method = strtolower($this->router->fetch_method());
      ?>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_menu_1">
          <!-- sidebar Menu -->
          <div id="MainMenu" class="">
            <ul id="menu-list" class="nav nav-list">
              <li class="separate-menu"><span>Main Menu</span></li>
              <li class="<?php if($class == 'dashboard'){ echo 'active open'; } ?>"> <a href="<?php echo base_url('Dashboard'); ?>">
               <i class="menu-icon fa fa-tachometer"></i> <span class="menu-text"> Dashboard </span> </a> <b class="arrow"></b> </li>




               <?php 
               if($_SESSION['user_role'] == 1){
                 /* Admin menu role  1 */
                 ?>

                 <li class="<?php if($class == 'user'){ echo 'active open'; } ?>"> <a href="#" class="dropdown-toggle"> <i class="menu-icon fa fa-user"></i> <span class="menu-text"> Manage Users </span> <b class="arrow fa fa-angle-down"></b> </a> <b class="arrow"></b>
                  <ul class="submenu nav-show"  >

                    <li class=""> <a href="<?php echo base_url('admin/user/doctor'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Doctor Manegment</span> </a> <b class="arrow"></b> </li>

                    <li class=""> <a href="<?php echo base_url('admin/user/patient'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Patient Manegment</span> </a> <b class="arrow"></b> </li>

                    <li class=""> <a href="<?php echo base_url('admin/user/addSubscription'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Add Subscription</span> </a> <b class="arrow"></b> </li>

                    <li class=""> <a href="<?php echo base_url('admin/user/subscriber'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text"></span> Subscriber user</a> <b class="arrow"></b> </li>


                  </ul>
                </li>
                
              <?php
                $mclass = array("manage_status", "subscription","doctor_specialties");
               $rdata = inarray($mclass, $class);
              ?>
              <li class="<?php if($rdata){ echo 'open active'; }?>">
                 <a href="#" class="dropdown-toggle"> <i class="menu-icon fa fa-cog"></i> <span class="menu-text"> Manage Settings </span> <b class="arrow fa fa-angle-down"></b> </a> <b class="arrow"></b>
                  <ul class="submenu nav-show"  >

                    <li class=""> <a href="<?php echo base_url('admin/Manage_status/visit_status'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Manage Type of Visit</span> </a> <b class="arrow"></b> </li>

                    <li class=""> <a href="<?php echo base_url('admin/Manage_status/appointment_status'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Manage Appointment Status</span> </a> <b class="arrow"></b> </li>

                    <li class=""> <a href="<?php echo base_url('admin/subscription'); ?>" ><i class="menu-icon fa fa-money" aria-hidden="true"></i><span class="menu-text"> Manage Subscription</span> </a> <b class="arrow"></b> </li>

                    <li class=""> <a href="<?php echo base_url('admin/doctor_specialties'); ?>" ><i class="menu-icon fa fa-money" aria-hidden="true"></i><span class="menu-text"> Doctor specialties</span> </a> <b class="arrow"></b> </li>

                  </ul>
                </li>

                <li class="<?php if($class == 'manage'){ echo 'active open'; } ?>"> <a href="<?php echo base_url('admin/manage/ads'); ?>" > <i class="menu-icon fa fa-eye"></i> <span class="menu-text"> Manage Ads</span> </a> <b class="arrow"></b> </li>

<!-- 
                <li class="<?php if($class == 'subscription'){ echo 'active open'; } ?>"> <a href="<?php echo base_url('admin/subscription'); ?>" ><i class="menu-icon fa fa-money" aria-hidden="true"></i><span class="menu-text"> Manage Subscription</span> </a> <b class="arrow"></b> </li>

                <li class="<?php if($class == 'doctor_specialties'){ echo 'active open'; } ?>"> <a href="<?php echo base_url('admin/doctor_specialties'); ?>" ><i class="menu-icon fa fa-money" aria-hidden="true"></i><span class="menu-text"> Doctor specialties</span> </a> <b class="arrow"></b> </li> -->

                <li class="<?php if($class == 'profile'){ echo 'active open'; } ?>"> <a href="<?php echo base_url('doctor/profile/doctor_specialties'); ?>" > <i class="menu-icon fa fa-plus"></i> <span class="menu-text">Add Doctor Specialties</span> </a> <b class="arrow"></b> </li>


                <?php }

                else if($_SESSION['user_role'] == 2 || $_SESSION['user_role'] == 4){  
                 /* Doctor menu role  2 */
                 ?>



                 <li class="<?php if($class == 'profile'){ echo 'active open'; } ?>"> <a href="#" class="dropdown-toggle"> <i class="menu-icon fa fa-user"></i> <span class="menu-text"> Setting </span> <b class="arrow fa fa-angle-down"></b> </a> <b class="arrow"></b>
                  <ul class="submenu nav-show"  >

                   <!-- <li class=""> <a href="<?php echo base_url('doctor/profile/doctor_specialties'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Add Doctor Specialties</span> </a> <b class="arrow"></b> </li> -->

                   <li class=""> <a href="<?php echo base_url('doctor/profile'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Profile</span> </a> <b class="arrow"></b> </li>

                   <li class=""> <a href="<?php echo base_url('doctor/profile/setting'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Notifications</span> </a> <b class="arrow"></b> </li>

                   <li class=""> <a href="<?php echo base_url('doctor/profile/staff'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Staff</span> </a> <b class="arrow"></b> </li>

                   <li class=""> <a href="<?php echo base_url('doctor/profile/chat_settings'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Chat Settings</span> </a> <b class="arrow"></b> </li>

                 </ul>
               </li>


               <li class="<?php if($class == 'appointment'){ echo 'active open'; } ?>"> <a href="#" class="dropdown-toggle"> <i class="menu-icon fa fa-medkit"></i> <span class="menu-text"> Appointment </span> <b class="arrow fa fa-angle-down"></b> </a> <b class="arrow"></b>
                <ul class="submenu nav-show"  >

                  <li class=""> <a href="<?php echo base_url('doctor/appointment/appointment_list'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Today Appointment</span> </a> <b class="arrow"></b> </li>

                  <li class=""> <a href="<?php echo base_url('doctor/appointment/history'); ?>" > <i class="menu-icon fa fa-caret-right"></i> <span class="menu-text">Appointment Records</span> </a> <b class="arrow"></b> </li>

                </ul>
              </li>



              <?php } ?>





              <!-- 			  <li class="<?php if($class == 'product'){ echo 'active open'; } ?>"> <a href="<?php echo base_url('product'); ?>" class=""> <i class="menu-icon fa fa-file-text"></i> <span class="menu-text"> Product sell </span> </a> </li> -->
            </ul>
            <a class="sidebar-collapse" id="sidebar-collapse" data-toggle="collapse" data-target="#test"> <i id="icon-sw-s-b" class="fa fa-angle-double-left"></i> </a> </div>
          </div>
        </div>
        <!-- end tab-content-->
      </div>




      <!-- end tabbable-line -->
    </div>


    <?php if($_SESSION['user_role'] == 2 || $_SESSION['user_role'] == 4){  
     /* Doctor menu role  2 */
     ?>
     <div class="tab-pane main_chat" id="tab_contact_2">

      <div class="search-menu-form" role="search">
        <div class="">
          <input id="contact-list-search" placeholder="Enter Name and Email." class="form-control search-menu" type="text" style="max-width: 200px;color:#000;background-color: #fff;margin: 10px;" placeholder="Search user">
          <a href="#modal-add-contact" data-toggle="modal" class="btn-modal btn-link" title="Add Contact">
            <i class="fa fa-plus"></i>
          </a>
        </div>
      </div>


      <ul class="list-group side_bar_chat" id="contact-list">

        <li class="separate-menu"><span>Chat you friends</span> <span class="chat_setting" ><a href="<?php echo base_url('doctor/profile/chat_settings'); ?>"><i class="fa fa-cog" aria-hidden="true" id="chat_setting" style="float: right; color: black;"></i></a></span></li>

        <?php
        $online_friend = getChatFriend($this->session->userdata('user_id'));
        if(!empty($online_friend)){
          foreach($online_friend as $val){

            $picture_url = image_check($val['picture_url'],USER_IMAGE, 'user');

                                 // $full_name = (substr(ucwords($val['full_name'])),0,10);
            $iparr = explode(" ", $val['full_name']);
            $sender_name = ucwords($iparr[0]);

                                  //$full_name = substr($iparr[0], 0,10);
                                  //$full_name = ucwords($iparr[0]);

            $full_name = $this->session->userdata('name'); 
            $fullName = ucwords($full_name);
            $full_name = ucwords(substr($full_name,0,1)); 

            $mt = getChatGroupId($val['id'],$this->session->userdata('user_id'));
            $s_id = $this->session->userdata('user_id');
            ?>

            <li class="list-group-item">
              <div class="col-xs-12 col-sm-3 avatar-contact">
                <img src="<?php echo $picture_url; ?>" alt="Scott Stevens" class="img-responsive img-flat">
              </div>
             <!-- {{<?php echo $mt[0]['id']; ?>}}--{{chat_groupId}} --> 
             <?php 
               // echo "groupid_".$mt[0]['id']."_".$val['id']; 

             ?>
           <!--  <p id="groupid_<?php echo $mt[0]['id']; ?>_<?php echo $val['id']; ?>"></p>
 -->
             
              <div class="col-xs-12 col-sm-9 ">
                <p ng-click="getChat('<?php echo $val['id']; ?>', '<?php echo $this->session->userdata('user_id'); ?>', '<?php echo $mt[0]['id']; ?>', '<?php echo $full_name; ?>', '<?php echo $sender_name; ?>','<?php echo $fullName; ?>' );">

                 <!--  <span style="display: none;" id="groupid_<?php echo $mt[0]['id']; ?>_<?php echo $val['id']; ?>">
  <img src="<?php echo base_url('assets/images/pulse.gif'); ?>" style="position: initial;">
</span> -->
                  <span class="name"><?php echo ucwords($val['full_name']); ?></span>
                </p>
                

                <span class="glyphicon glyphicon-earphone text-muted c-info" data-toggle="tooltip" title="" data-original-title="<?php echo $val['phone_number']; ?>"></span>
                <span class="visible-xs"> <span class="text-muted"><?php echo $val['phone_number']; ?></span><br></span>
                <span class="fa fa-comments text-muted c-info" data-toggle="tooltip" title="" data-original-title="<?php echo $val['email_address']; ?>"></span>
                <span class="visible-xs"> <span class="text-muted"><?php echo $val['email_address']; ?></span>
                <br></span>

<!-- chat count show -->

                <span data-ng-init="get_chat_count(<?php echo $mt[0]['id'].','.$val['id'].','.$s_id;?>)" >
                <span data-ng-init="get_read_status(<?php echo $mt[0]['id'].','.$val['id'].','.$s_id;?>)">
                    <span class="blink" id="groupid_<?php echo $val['id']; ?>_<?php echo $mt[0]['id']; ?>"></span>
                </span>
              </span>

<!-- chat count show -->



                 <span style="display: none;" id="groupid_<?php echo $mt[0]['id']; ?>_<?php echo $val['id']; ?>" class="groupid_<?php echo $mt[0]['id']; ?>_<?php echo $val['id']; ?>">
  <img src="<?php echo base_url('assets/images/pulse.gif'); ?>" style="position: initial; width:37px;float: right;margin-top: -11px;">
</span>


              </div>
              <div class="clearfix"></div>
            </li>
            <?php  }    
          }else{

                                //echo 'not found';
          }

          ?>





        </ul>

      </div>

      <?php } ?>


      <!-- end tabbable-panel -->
    </div>

