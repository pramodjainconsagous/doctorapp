<?php $this->load->view('common/header');?>
<?php $this->load->view('common/admin_header'); ?>
<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->
<!-- main content  -->

<div id="main" class="main">
  <div class="row"> 
    <!-- breadcrumb section -->
    <div class="ribbon">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
      </ul>
    </div>
    
    <!-- main content -->
    <div id="content">
      <div id="sortable-panel" class="">
        <div id="titr-content" class="col-md-12">
          <h2>&nbsp;</h2>
        </div>
        
        <!-- Admin over view .col-md-12 -->
        <div class="col-md-12 ">
          <div  class="panel panel-default">
            <div class="panel-body"> 
              <div class="row"> 
                <!-- progress section -->
                <div class="panel-body">
                  <h1 class="darkblue">Welcome to <?php echo strUcwords($this->config->item('site_name').' '.$this->config->item('site_full_name')); ?></h1>



                  <div class="col-md-3 no-padd-left-right"></div>


                  <div class="col-md-3 no-padd-left-right black-text">
                    <div class="well well-sm text-center">
                      <span class="h1 shadow-block no-margin darkblue"> <?php echo count($appointment_result); ?></span>
                      <span class="text-center">
                        <i class="menu-icon fa fa-medkit"></i>
                      </span>
                      <span class="black">Appointment</span>
                    </div>
                  </div>

                  <div class="col-md-3 no-padd-left-right black-text">
                    <div class="well well-sm text-center">
                      <span class="h1 shadow-block no-margin purple"><?php echo count($patient_result); ?></span>
                      <span class="text-center">
                        <i class="fa fa-user"></i>
                      </span>
                      <span class="black">Patient</span>
                    </div>
                  </div>
<!-- 
              <div class="col-md-3 no-padd-left-right">
                                <div class="well well-sm text-center">
                                    <span class="h1 shadow-block no-margin orange"><?php echo count($patient_result); ?></span>
                                        <span class="text-center">
                                            <i class="fa fa-user text-muted"></i>
                                        </span>
                                    <span class="text-muted">Patient</span>
                                </div>
              </div>

              <div class="col-md-3 no-padd-left-right">
                                <div class="well well-sm text-center">
                                    <span class="h1 shadow-block no-margin orange"><?php echo count($patient_result); ?></span>
                                        <span class="text-center">
                                            <i class="fa fa-user text-muted"></i>
                                        </span>
                                    <span class="text-muted">Patient</span>
                                </div>
                              </div> -->





                           <div class="col-md-12 layout no-padding">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-body no-padding">
                        <div class="tabbable tabs-above ">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="" data-target="#one" data-toggle="tab">
                                        <i class="fa fa-users success"></i>
                                       Subscribe Clinic
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content ">
                                <div class="tab-pane active" id="one">
                                    <div class="table-responsive">
                                      <?php 
                                        $st = getSubscriberActiveUser();
                                        if(!empty($st)){ ?>
                                        <table class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Plan Amount</th>
                                                <th>Due Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                              <?php foreach($st as $val){ 
                                                   //dd($val);
                                                ?>
                                            <tr>
                                                <td>#12</td>
                                                 <td><?php echo strUcwords($val['clinic_name']); ?></td>
                                                <td><span class="label label-table label-success">Active</span></td>
                                                <td><?php echo $val['amount']; ?></td>
                                                <td><?php echo convert_datetime($val['subscription_end_dt']); ?></td>
                                            </tr>
                                            <?php } ?>
                                          
                                            </tbody>
                                        </table>
                                        <?php } ?>
                                        <hr>
                                       

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end .col-md-12 -->

           
            <!-- end .col-md-12 -->

        </div>   
        <?php if($_SESSION['user_role'] == 2 || $_SESSION['user_role'] == 4){?>
        <!-- visting section -->
        <div class="col-md-12 layout no-padding">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-body no-padding">
                        <div class="tabbable tabs-above ">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="" data-target="#one" data-toggle="tab">
                                        <i class="fa fa-users success"></i>
                                       Visiting  Status 
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content ">
                                <div class="tab-pane active" id="one">
                                    <div class="table-responsive">
                                      <?php 
                                      //dd($visiting_result);
                                        if(!empty($visiting_result)){ 
                                          function is_not_null($visiting_result)
                                          { 
                                            return !is_null($visiting_result);
                                          }
                                          $visiting_result = array_filter($visiting_result, 'is_not_null');
                                          ?>
                                        <table class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Count</th>
                                                <th>Create Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                              <?php foreach($visiting_result as $val){ 
                                                $visiting_status = $val['visiting_status'];
                                                $clinic_id = $val['clinic_id'];
                                                $where     = "where visiting_status=$visiting_status AND clinic_id=$clinic_id";
                                                $get_count = total_visiting_count($where);
                                                ?>
                                            <tr>
                                                 <td><?php echo strUcwords($val['name']); ?></td>
                                                <td><span class="label label-table label-success"><?php echo $val['mvs_status'];?></span></td>

                                                <td><?php echo $get_count;?></td>
                                                <td><?php echo $val['booking_date']; ?></td>
                                            </tr>
                                            <?php } ?>
                                          
                                            </tbody>
                                        </table>
                                        <?php } ?>
                                        <hr>
                                       

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end .col-md-12 -->

           
            <!-- end .col-md-12 -->

        </div>   
        <?}?>


                            </div>
                            <!-- ./preogress section --> 

                          </div>
                        </div>
                      </div>
                      <!-- end panel --> 
                    </div>
                    <!-- /end Admin over view .col-md-12 --> 

                  </div>
                  <!-- end col-md-12 --> 
                </div>
                <!-- end #content --> 
              </div>
              <!-- end .row --> 
            </div>
            <!-- ./end #main  -->

            <?php $this->load->view('common/footer_content');?>
            <script type="text/javascript">
              $('#example1').dataTable({
               responsive: true
             });
           </script>
           <?php $this->load->view('common/footer');?>
