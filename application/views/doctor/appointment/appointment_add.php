<?php $this->load->view('common/header');?>
<?php $this->load->view('common/admin_header'); ?>
<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->

<div id="main" class="main">
	<div class="row"> 
		<!-- breadcrumb section -->
		<div class="ribbon">
			<ul class="breadcrumb">
				<li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
			</ul>
		</div>

		<div id="content">
			<div id="sortable-panel" class="">
				<div id="titr-content" class="col-md-12">
					<h2><?php echo ucwords($title);?></h2>
				</div>

				<!-- Admin over view .col-md-12 col-md-offset-3-->
				<div class="col-md-12">
					<div  class="panel panel-default">
 		<?php $this->load->view('common/message'); ?>

						<div class="panel-heading">
							<div class="panel-title"> <i class="fa fa-edit"></i> <?php echo ucwords($title);?>
								<div class="bars pull-right"> <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a> <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a> </div>
							</div>
						</div>
						<div class="panel-body"> 

							<!-- middel content section --> 
							<div class="row"> 
								<div class="panel-body">

									<form action="<?php echo base_url('doctor/appointment/Add');  ?>" role="form" id="form1" novalidate method="post" enctype="multipart/form-data">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<?php if(!empty($message)){ ?>
													<div class="alert alert-danger"> <?php echo $message;  ?></div>

													<?php } ?>

													<?php if(!empty( validation_errors())){ ?>
													<div class="alert alert-danger">
														<?php echo validation_errors(); ?>
													</div>
													<?php } ?>
												</div>
											</div>


											<div class="col-md-4 col-sm-4 col-xs-6 full-xs">
												<div class="form-group">
													<label class="control-label">
														Patient Mobile No Search <span class="symbol required" aria-required="true"></span>
													</label>
													<br/>
													<label>
														<input class="form-control" minlength="10" maxlength="12"  placeholder="Mobile number" type="text" name="mobile_no" id="mobile_no">
													</label>

													<label> <a href="javascript:void(0);" onclick="search_patient();" class="btn btn-info btn-block"> 
														<i class="fa fa-search"></i>Search Patient </a></label>
													</div>
												</div>







												<div class="col-md-4 col-sm-4 col-xs-6 full-xs" id="error_msg"></div>


												<div class="col-md-4 col-sm-4 col-xs-6 full-xs" id="patient_info" style="display: none;">


													<div class="col-md-12" style="background-color: #efefef;padding: 20px;">
														<div class="form-group">
															<label id="patient_name"></label>
															<label id="patient_img">

															</label>
														</div>
													</div>

													<div class="col-md-6 col-xs-6 full-xs">
														<div class="form-group"></div>
													</div>


													<div class="col-md-6 col-xs-6 full-xs" style="display: none;">
														<div class="form-group">
															<label class="control-label">
																Patient Phone Number <span class="symbol required" aria-required="true"></span>
															</label><br/>
															<label id="patient_mobile_number"></label>
														</div>
													</div>



												</div>


												<input type="hidden" id="patient_id" name="patient_id" value="" >

												<div class="col-md-12">
													<div class="row">

														<div class="col-md-4 col-sm-4 col-xs-6 full-xs">
															<div class="form-group">
																<label class="control-label">
																	Appointment Date <span class="symbol required" aria-required="true"></span>
																</label>


																<input type="date" placeholder="yyyy-mm-dd" class="form-control datepicker" id="appointment_date" name="appointment_date" value="" >

															</div>
														</div>

														<div class="col-md-4 col-sm-4 col-xs-6 full-xs">
															<div class="form-group">
																<label class="control-label">
																	Appointment Time Slot <span class="symbol required" aria-required="true"></span>
																</label>


																<select class="form-control" name="time_slote" id="time_slote" >
																	<?php echo booking_time_slot(); ?>
																</select>


															</div>
														</div>


														<div class="col-md-4 col-sm-4 col-xs-12">
															<div class="form-group">
																<label class="control-label">
																	Appointment Status <span class="symbol required" aria-required="true"></span>
																</label>
																<select class="form-control" name="booking_status" id="booking_status" >
																	<option value="scheduled">Scheduled</option>
																	<?php //echo booking_status(); ?>
																</select>
															</div>
														</div>

													</div>

												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">
															Observaciones <span class="symbol required" aria-required="true"></span>
														</label>
														<textarea name="description" id="description" rows="4" cols="30" class="form-control"></textarea>
													</div>
												</div>
												<!-- <div class="col-md-6">
													<div class="form-group">
														<label class="control-label">
															Type of treatment <span class="symbol required" aria-required="true"></span>
														</label>
														<select class="form-control" name="description" id="description">

															<?php echo getTreatmentType(); ?>
														</select>
													</div>
												</div> -->
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">
															Doctor Visit Status <span class="symbol required" aria-required="true"></span>
														</label>
														<select class="form-control" name="visiting_status" id="visiting_status" >
															<?php if($visit_status){ foreach ($visit_status as $value) {?>
																<option value="<?php echo $value['id'];?>">
																	<?php echo $value['name'];?></option>
															<?php }} ?>
														</select>
													</div>
												</div>



											</div>
											<div class="row">
												<div class="col-md-2 col-sm-3 col-xs-6 full-xs bottom-margin-xs">
													<a href="<?php echo base_url('doctor/appointment/appointment_list');?>" class="btn btn-light-grey btn-block">								
														<i class="fa fa-arrow-circle-left"></i> <?php echo BACK; ?>  							
													</a>
												</div>
												<div class="col-md-3 col-sm-4 col-xs-6 full-xs">

													<button class="btn btn-success btn-block" type="submit">
														<?php echo SAVE; ?>  <i class="fa fa-arrow-circle-right"></i>
													</button>

												</div>
											</div>
										</form>

									</div>
								</div>
								<!-- end #content --> 
							</div>
						</div>
					</div>
					<!-- end of admin over view -->
				</div>
			</div>
		</div>
		<!-- end .row --> 
	</div>
	<!-- ./end #main  -->



	<!-- patient add model-->
	<div id="myModal" class="modal fade addPatientClass" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add new patient</h4>
				</div>
				<form action="<?php echo base_url('admin/user/patientAdd');  ?>" role="form" id="form1" novalidate method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div id="ptient_msg"></div>
								</div>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-6 full-xs">
								<div class="form-group">
									<label class="control-label">
										Patient Name <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Patient Name" class="form-control" id="newPatientFullName" name="newPatientFullName" value="" minlength="2" maxlength="40">
									<span id="newPatientFullName-error" class="error">This field is required.</span>
								</div>
							</div>


							<div class="col-md-6 col-sm-6 col-xs-6 full-xs">
								<div class="form-group">
									<label class="control-label">
										Patient Mobile No <span class="symbol required" aria-required="true"></span>
									</label>
									<input type="text" placeholder="Patient Mobile No" class="form-control" id="newPatientMobileNo" name="newPatientMobileNo" value="" minlength="10" maxlength="12">
									<span id="newPatientMobileNo-error" class="error">This field is required.</span>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

						<a class="btn btn-success" href="javascript:void(0);" onclick="addNewPtient();"><?php echo SAVE; ?>  <i class="fa fa-arrow-circle-right"></i></a>



					</button>

				</div>
			</form>
		</div>
	</div>
</div>
<!-- end model-->

<?php $this->load->view('common/footer_content');?>


<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<style type="text/css">
.addPatientClass .error { display: none;

}
.addPatientClass .error {
	margin-top: 5px;
	margin-bottom: 10px;
	color: #8B0000;
}
</style>
<script type="text/javascript">

	function addNewPtient(){
		$('#ptient_msg').html('');
		var newPatientFullName = $('#newPatientFullName').val();
		var newPatientMobileNo = $('#newPatientMobileNo').val();

		$('#newPatientFullName-error').hide();
		$('#newPatientMobileNo-error').hide();

		if(newPatientFullName == ''){
			$('#newPatientFullName-error').show();
			return false;
		}
		if(newPatientMobileNo == ''){
			$('#newPatientMobileNo-error').show();
			return false;
		}



		var url = base_url+'ajax/addNewPatient';  
		$.ajax({
			type: "POST",
			url: url,
			data: {'name': newPatientFullName, 'mobile_no': newPatientMobileNo},
		})
		.done(function(result) {
			var result_data = JSON.parse(result);

			if(parseInt(result_data.status) == 1 ) {
				$('#mobile_no').val(newPatientMobileNo);
				$('#ptient_msg').html('<div class="alert alert-success">Patient create successfully.</div>');
				$('#error_msg').html('');
				setTimeout("model_close();", 3000);  


				setTimeout("search_trigger();", 3000);

			}else{
				$('#ptient_msg').html('<div class="alert alert-danger">'+result_data.message+'</div>');
			} 
		});	
	}

	function model_close(){
		$('#myModal').modal('hide');
	}

	function model_open(){
		$('#newPatientFullName').val('');
		$('#newPatientMobileNo').val('');
		$('#ptient_msg').html('');

		$('#myModal').modal('show');
	}

	function search_trigger(){
		search_patient();
	}

	function search_patient(){
		$('#patient_info').hide();
		$('#error_msg').show();

		$('#error_msg').html('<div class="alert alert-info">Please wait.....<div>');
		var mobile_no = $('#mobile_no').val();
		var url = base_url+'ajax/search_patient';  
		$.ajax({
			type: "POST",
			url: url,
			data: {'mobile_no': mobile_no},
		})
		.done(function(result) {
			var result_data = JSON.parse(result);



			if(parseInt(result_data.status) == 1 ) {
				$('#error_msg').hide();
				$('#patient_info').show();

				var patient_name = result_data.result['patient_name'];
				var patient_phone_number = result_data.result['patient_phone_number'];
				var patient_id = result_data.result['patient_id'];
				var patient_picture_url = result_data.result['patient_picture_url'];

				$('#patient_id').val(patient_id);
				$('#patient_name').html(patient_name);
				$('#patient_mobile_number').html(patient_phone_number);
				$('#patient_img').html('<img src="'+patient_picture_url+'" alt="'+patient_name+'" style="height: 60px; width: 60px;" >');

			}else{
				$('#patient_info').hide();
				$('#error_msg').html('<div class="alert alert-danger">Not found patient <a  class="btn btn-success" onclick="model_open();">Add new patient</a></div>');
			} 
		});	

	}

	$(document).ready(function() {
		$("#appointment_date").datepicker({
			defaultDate: "+1w",
			dateFormat: "yy-mm-dd",
			minDate: 0
		});
	});



	$(document).ready(function() {

		var form = $('#form1');
		var errorHandler1 = $('.errorHandler', form);
		var successHandler1 = $('.successHandler', form);
		form.validate({
errorElement: "span", // contain the error msg in a span tag
errorClass: 'help-block',
errorPlacement: function (error, element) { // render error placement for each input type
if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
	error.insertAfter($(element).closest('.form-group').children('div').children().last());
} else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
	error.insertAfter($(element).closest('.form-group').children('div'));
} else {
	error.insertAfter(element);
// for other inputs, just perform default behavior
}
},
ignore: "",  
rules: {
	mobile_no: {
		required: true,
	},
	patient_id: {
		required: true,
	},
	appointment_date: {
		required: true,
	},
	time_slote: {
		required: true
	},
	booking_status: {
		required: true
	},
	visiting_status:{
		required: true
	}// ,
	// description:{
	// 	required: true
	// }

},
invalidHandler: function (event, validator) { //display error alert on form submit
	successHandler1.hide();
	errorHandler1.show();
},
highlight: function (element) {
	$(element).closest('.help-block').removeClass('valid');
// display OK icon
$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
// add the Bootstrap error class to the control group
},
unhighlight: function (element) { // revert the change done by hightlight
	$(element).closest('.form-group').removeClass('has-error');
// set error class to the control group
},
success: function (label, element) {
	label.addClass('help-block valid');
// mark the current input as valid and display OK icon
$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
},
submitHandler: function (form) {
	successHandler1.show();
	errorHandler1.hide();
// submit form
form.submit();
}
});
	});
</script>
<?php $this->load->view('common/footer');?>
