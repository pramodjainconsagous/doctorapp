<?php $this->load->view('common/header');?>
<?php $this->load->view('common/admin_header'); ?>
<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->

<div id="main" class="main">
  <div class="row"> 
    <!-- breadcrumb section -->
    <div class="ribbon">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
      </ul>
    </div>

    <div id="content">
      <div id="sortable-panel" class="">
        <div id="titr-content" class="col-md-12">
          <h2><?php echo ucwords($title);?></h2>
        </div>

      <!-- Admin over view .col-md-12 col-md-offset-3-->
        <div class="col-md-12">
          <div  class="panel panel-default">


          <div class="panel-heading">
          <div class="panel-title"> <i class="fa fa-edit"></i> <?php echo ucwords($title);?>
            <div class="bars pull-right"> <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a> <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a> </div>
          </div>
        </div>
            <div class="panel-body"> 

<!-- middel content section --> 
             <div class="row"> 
				<div class="panel-body">
				
				<form action="<?php echo base_url('doctor/appointment/edit/'.$id);  ?>" role="form" id="form1" novalidate method="post" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
                                <?php if(!empty($message)){ ?>
									<div class="alert alert-danger"> <?php echo $message;  ?></div>
									
									<?php } ?>
                                </div>
                               </div>
							
                            
                            <div class="col-md-4 col-sm-4 col-xs-6 full-xs">
								<div class="form-group">
									<label class="control-label">
										Patient Name <span class="symbol required" aria-required="true"></span>
									</label>
									<br/>
									<label><?php echo ucwords($result[0]['patient_name']); ?></label>
								</div>
							</div>
							

							  <div class="col-md-4 col-sm-4 col-xs-6 full-xs">
								<div class="form-group">
									<label class="control-label">
										Patient Phone Number <span class="symbol required" aria-required="true"></span>
									</label><br/>
									<label><?php echo $result[0]['patient_phone_number']; ?></label>
								</div>
							</div>


							  <div class="col-md-4 col-sm-4 col-xs-6 full-xs">
								<div class="form-group">
									<label class="control-label">
										Patient Avatea <span class="symbol required" aria-required="true"></span>
									</label><br/>
									<label>

                                           <?php $img = image_check($result[0]['patient_picture_url'],USER_IMAGE);?>
                          <img src="<?php echo $img; ?>" alt="<?php echo $result[0]['patient_picture_url']; ?>" style="height: 60px; width: 60px;" > 

										</label></div>
								</div>

							 


						    
                            <div class="col-md-4 col-sm-4 col-xs-6 full-xs">
								<div class="form-group">
									<label class="control-label">
										Appointment Date <span class="symbol required" aria-required="true"></span>
									</label>
									

									<input type="date" placeholder="yyyy-mm-dd" class="form-control datepicker" id="appointment_date" name="appointment_date" value="<?php echo $result[0]['booking_date']; ?>" >

								</div>
							</div>
                            
                            <div class="col-md-4 col-sm-4 col-xs-6 full-xs">
								<div class="form-group">
									<label class="control-label">
										Appointment Time Slot <span class="symbol required" aria-required="true"></span>
									</label>

									
									<select class="form-control" name="time_slote" id="time_slote" >
                                    <?php echo booking_time_slot($result[0]['time_slote']); ?>
                                    </select>

									
								</div>
							</div>
                            
                          
							<div class="col-md-4 col-sm-4 col-xs-6 full-xs">
								<div class="form-group">
									<label class="control-label">
										Appointment Status <span class="symbol required" aria-required="true"></span>
									</label>
									<select class="form-control" name="booking_status" id="booking_status" >
                                    <?php echo booking_status($result[0]['status']); ?>
                                    </select>
								</div>
							</div>
                            

                            <div class="col-md-6">
								<div class="form-group">

									<label class="control-label">
										Observaciones <span class="symbol required" aria-required="true"></span>
									</label>
	                              
	                                <!-- <select class="form-control" name="description" id="description"> -->					<textarea name="description" id="description" rows="4"  class="form-control">
											<?php echo $result[0]['description']; ?>
	                                	</textarea>							
									<!-- </select> -->

								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">
										Doctor Visit Status <span class="symbol required" aria-required="true"></span>
									</label>
									<select class="form-control" name="visiting_status" id="visiting_status" >
                                    	<?php if(!empty($visit_status)){foreach($visit_status as $value){?>
                                    	<option value="<?php echo $value['id'];?>" <?php if($result[0]['visit_id'] == $value['id']){?> selected <?php }?> >
                                    		<?php echo $value['name']; ?></option>
                                    	
                                    	<?php }} ?>
                                    </select>
								</div>
							</div>



                            
						</div>


						<div class="row">
							<div class="col-md-2 col-sm-3 col-xs-6 full-xs bottom-margin-xs">
							  <a href="<?php echo base_url('doctor/appointment/appointment_list');?>" class="btn btn-light-grey btn-block">								
									<i class="fa fa-arrow-circle-left"></i> <?php echo BACK; ?>  							
							  </a>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-6 full-xs">
							  
								<button class="btn btn-success btn-block" type="submit">
									<?php echo UPDATE; ?>  <i class="fa fa-arrow-circle-right"></i>
								</button>
							
							</div>
						</div>
					</form>
					
				</div>
		    </div>
		<!-- end #content --> 
		</div>
	</div>
</div>
<!-- end of admin over view -->
		 </div>
	</div>
  </div>
  <!-- end .row --> 
</div>
<!-- ./end #main  -->


<?php $this->load->view('common/footer_content');?>


<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

<script type="text/javascript">
	$(document).ready(function() {
	    $("#appointment_date").datepicker({
	    defaultDate: "+1w",
	    dateFormat: "yy-mm-dd",
	    });
    });

	$(document).ready(function() {
	
		        var form = $('#form1');
		        var errorHandler1 = $('.errorHandler', form);
		        var successHandler1 = $('.successHandler', form);
		        form.validate({
		            errorElement: "span", // contain the error msg in a span tag
		            errorClass: 'help-block',
		            errorPlacement: function (error, element) { // render error placement for each input type
		                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
		                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
		                } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
		                    error.insertAfter($(element).closest('.form-group').children('div'));
		                } else {
		                    error.insertAfter(element);
		                    // for other inputs, just perform default behavior
		                }
		            },
				  ignore: "",
		            rules: {
		                appointment_date: {
		                    required: true,
		                },
						 time_slote: {
		                    required: true
		                },
		                booking_status: {
		                    required: true
		             	}//,
		             //    description:{
		            	// 	required: true
		            	// }
						
		              },
		            invalidHandler: function (event, validator) { //display error alert on form submit
		                successHandler1.hide();
		                errorHandler1.show();
		            },
		            highlight: function (element) {
		                $(element).closest('.help-block').removeClass('valid');
		                // display OK icon
		                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
		                // add the Bootstrap error class to the control group
		            },
		            unhighlight: function (element) { // revert the change done by hightlight
		                $(element).closest('.form-group').removeClass('has-error');
		                // set error class to the control group
		            },
		            success: function (label, element) {
		                label.addClass('help-block valid');
		                // mark the current input as valid and display OK icon
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
		            },
		            submitHandler: function (form) {
		                successHandler1.show();
		                errorHandler1.hide();
		                // submit form
		                 form.submit();
		            }
		        });
	});
</script>
<?php $this->load->view('common/footer');?>
