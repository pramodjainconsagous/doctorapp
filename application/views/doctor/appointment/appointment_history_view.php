<?php $this->load->view('common/header');?>
<?php $this->load->view('common/admin_header'); ?>
<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->
<!-- main content  -->

<div id="main" class="main">
  <div class="row">
    <!-- breadcrumb section -->
    <div class="ribbon">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
      </ul>
    </div>
    <?php $this->load->view('common/message'); ?>
    <!-- main content -->
    <div id="content">
      <div id="sortable-panel" class="">
        <div id="titr-content" class="col-md-12">
          <h2><?php echo ucwords($title); ?></h2>
          <h5>&nbsp;</h5>
          <div class="actions">   </div>
        </div>
        <!-- Admin over view .col-md-12 -->
        <div class="col-md-12 ">
          <div  class="panel panel-default">
            <div class="panel-body"> <i class="glyphicon glyphicon-stats"></i> <b><?php echo ucwords($title); ?>



  <div class="col-md-12 ">

                <form action="<?php echo base_url('doctor/appointment/history'); ?>" method="post">
                

              <div class="col-md-4 ">
                <div class="form-group">
                  <label class="control-label">
                    Start Date
                  </label>
                  <input type="date" placeholder="yyyy-mm-dd" class="form-control datepicker" id="s_date" name="s_date" value="<?php echo $s_date; ?>"  >
                </div>
              </div>



               <div class="col-md-4 ">
                <div class="form-group">
                  <label class="control-label">
                    End Date
                  </label>
                <input type="date" placeholder="yyyy-mm-dd" class="form-control datepicker" id="e_date" name="e_date" value="<?php echo $e_date; ?>"  >
                </div>
              </div>



               <div class="col-md-4 ">
                <div class="form-group">
                  <label class="control-label"> &nbsp;
                  </label> <br/>
                  <button style="width: 100px; float: left;" class="btn btn-success btn-block" type="submit">
                           Search <i class="fa fa-search"></i>
                </button>
                </div>
              </div>

              </form>
            </div>


              <hr>
              <div class="row">
                <!-- progress section -->
                <div class="panel-body">
                  <table id="example1" class="table table-striped table-bordered width-100 cellspace-0" >
                    <thead>


                      <tr>
                         <th>Patient Avtar</th>
                        <th>Patient Name</th>
						            <th>Patient Phone No</th>
                        <th>Time Slote</th>
                         <th>Observaciones<!-- Description --> </th>

						            <th>Appointment Date</th>
                        <th>Visting Status</th>
                        <th>Appointment Status</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
						if($result){ 




						   foreach($result as $val){
							$id = safe_b64encode($val["id"]); 
               $img = image_check($val['patient_picture_url'],USER_IMAGE);
					 ?>
                      <tr>
                           <td> <img src="<?php echo $img; ?>" alt="<?php echo $result[0]['patient_name']; ?>" style="height: 60px; width: 60px;" > </td>
                        <td><?php echo strUcwords($val['patient_name']); ?></td>
                        <td><?php echo $val['patient_phone_number']; ?></td>
                        <td><?php echo $val['time_slote']; ?></td>

                         <td><?php echo strUcwords($val['description']); ?></td>
                         
                        <td><?php echo $val['booking_date']; ?></td>
                        <td><?php echo strUcwords($val['visit_name']); ?></td>
                        <td><?php echo strUcwords($val['status']); ?></td>
                        
                      </tr>
                      <?php } 
										       } ?>
                    </tbody>
                  </table>
                </div>
                <!-- ./preogress section -->
              </div>
            </div>
          </div>
          <!-- end panel -->
        </div>
        <!-- /end Admin over view .col-md-12 -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end #content -->
  </div>
  <!-- end .row -->
</div>
<!-- ./end #main  -->
<?php $this->load->view('common/footer_content');?>
<script type="text/javascript">
	 $('#example1').dataTable({
		    	responsive: true
		    });
</script>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

<script type="text/javascript">

  $(document).ready(function() {
      $("#appointment_date").datepicker({
      defaultDate: "+1w",
      dateFormat: "yy-mm-dd",
      });
    });
  $(document).ready(function() {
  
     /* $("#s_date").datepicker({
        defaultDate: "+1w",
        dateFormat: "yy-mm-dd",
      });

      $("#e_date").datepicker({
        defaultDate: "+1w",
        dateFormat: "yy-mm-dd",
      });*/


    $("#s_date").datepicker({
        numberOfMonths: 1,
        dateFormat: "yy-mm-dd",
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#e_date").datepicker("option", "minDate", dt);
        }
    });
    $("#e_date").datepicker({
        numberOfMonths: 1,
        dateFormat: "yy-mm-dd",
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#s_date").datepicker("option", "maxDate", dt);
        }
    });


  

  });
</script>

<?php $this->load->view('common/footer');?>
