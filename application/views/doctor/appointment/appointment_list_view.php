<?php $this->load->view('common/header');?>
<?php $this->load->view('common/admin_header'); ?>
<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->
<!-- main content  -->

<div id="main" class="main">
  <div class="row">
    <!-- breadcrumb section -->
    <div class="ribbon">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
      </ul>
    </div>
    <?php $this->load->view('common/message'); ?>
    <!-- main content -->
    <div id="content">
      <div id="sortable-panel" class="">
        <div id="titr-content" class="col-md-12">
          <h2><?php echo ucwords($title); ?></h2>
          <h5>&nbsp;</h5>
          <div class="actions"> <a href="<?php echo base_url('doctor/appointment/Add');?>" class="btn btn-success  has-ripple"> <?php echo ADD_NEW; ?> Appointment</a>  </div>
        </div>
        <!-- Admin over view .col-md-12 -->
        <div class="col-md-12 ">
          <div  class="panel panel-default">
            <div class="panel-body"> <i class="glyphicon glyphicon-stats"></i> <b><?php echo ucwords($title); ?>
              <hr>

              <div class="row">
               <div class="col-md-12" >
                 <div class="col-md-2 ">
                  Total Patient <span class="badge badge-success" style="padding: 10px;" >
                    <?php if(!empty($result)){ echo count($result);}else{ echo '0';} ?></span>
                  </div>
                  

                  <div class="col-md-2 ">
                    Attended Patient <span class="badge badge-primary" style="padding: 10px;" >
                    1</span>
                  </div>


                    <!--  <div class="col-md-2 ">
                      Current Patient <span class="badge badge-info" style="padding: 10px;" >
                        <?php $st = getCurrentPatient($clinic_id); 
                              if(!empty($st)){
                                echo strUcwords($st[0]['full_name'].' '.$st[0]['time_slote']);
                              }

                        ?></span>
                     </div>   
                   -->
                   <div class="col-md-3 ">
                    Current Patient
                    <select class="form-control" name="booking_status" id="booking_status" onchange="current_patient(this.value,'current','<?php echo $clinic_id; ?>');" >
                      <option value="">-----select current patient -----</option>
                      <?php  
                      
                      $st = getCurrentPatient($clinic_id); 
                      $current_patient_id = '';
                      if(!empty($st)){
                        $current_patient_id = $st[0]['id'];
                      }

                      if($result){ 
                        foreach($result as $val){

                          if((strtolower($val["status"]) != 'attended') && (strtolower($val["status"]) != 'cancel') && (strtolower($val["status"]) != 'next')){
                         $sel = '';
                         if($current_patient_id == $val['id']){
                          $sel = 'selected';
                        }
                        
                        ?>
                        <option <?php echo $sel; ?> value="<?php echo $val["id"]; ?>">
                          <?php echo strUcwords($val["patient_name"].' - '.$val["time_slote"].' - '.$val["status"]); ?></option> 
                          <?php }
                        }
                      }
                        ?>   

                      </select>
                    </div>


                    <div class="col-md-3 ">
                      Next Patient
                      <select class="form-control" name="booking_status" id="booking_status" onchange="current_patient(this.value,'next','<?php echo $clinic_id; ?>');" >
                        <option value="">-----select next patient-----</option>
                        <?php  if($result){ 
                          foreach($result as $val){ 
                           
                          if((strtolower($val["status"]) != 'attended') && (strtolower($val["status"]) != 'cancel') && (strtolower($val["status"]) != 'current')){

                          ?>
                          <option value="<?php echo $val["id"]; ?>">
                            <?php echo strUcwords($val["patient_name"].' - '.$val["time_slote"].' - '.$val["status"]); ?></option> 
                            <?php }
                          }
                        }
                          ?>   

                        </select>
                      </div>
                      
                    </div>                  
                  </div>
                  <br/><br/>
                  
                  <div class="row">
                   <div class="col-md-12" id="messageShow" >
                   </div></div>
                   <div class="row">
                    <!-- progress section -->
                    <div class="panel-body">
                      <table id="example1" class="table table-striped table-bordered width-100 cellspace-0" >
                        <thead>
                          <tr>
                           <th>Patient Avtar</th>
                           <th>Patient Name</th>
                           <th>Patient Phone No</th>
                           <th>Time Slote</th>

                           <th>Observaciones<!-- Description --> </th>

                           <th>Appointment Date</th>
                           <th>Appointment Status</th>
                           <th>Visting Status</th>
                           <th>Activation Code</th>
                           <th>Action</th>
                         </tr>
                       </thead>
                       <tbody>
                        <?php 
                        if($result){ 
                         foreach($result as $val){
                           $id = safe_b64encode($val["id"]); 
                           $img = image_check($val['patient_picture_url'],USER_IMAGE,'user');
                             $class = ''; 
                           if(strtolower($val['status']) == 'attended'){
                            $class = 'badge badge-success'; 
                           }
                           $re = $this->UserModel->get_code($val['clinic_id'], $val['patient_id']);
                           ?>
                           <tr>
                             <td> <img src="<?php echo $img; ?>" alt="<?php echo $result[0]['patient_name']; ?>" style="height: 60px; width: 60px;" > </td>
                             <td><?php echo strUcwords($val['patient_name']); ?></td>
                             <td><?php echo $val['patient_phone_number']; ?></td>
                             <td><?php echo $val['time_slote']; ?></td>

                             <td><?php echo strUcwords($val['description']); ?></td>
                             
                             <td><?php echo $val['booking_date']; ?></td>
                             <td>
                             <span class="<?php echo $class; ?>">
                                     <?php echo strUcwords($val['status']); ?>
                             </span>
                            
                            <?php  if(strtolower($val['status']) == 'current'){ ?>
                             <a id="atten_<?php echo $val["id"]; ?>" class="btn btn-info pull-right" href="javascript:void(0);" onclick="fun_attended('<?php echo $val["id"]; ?>');">Make Attended</a>
                             <?php } ?>
                              </td>
                              <td>
                              <?php echo $val['visit_name'];?></td>
                              <td><?php echo $re[0]->friend_code;?></td>
                             <td><div class="action-buttons"> 

                              <a title="Edit"  class="btn btn-info" href="<?php echo base_url('doctor/appointment/edit/' .$id) ?>"> 
                               Edit <i class="fa fa-pencil" aria-hidden="true"></i> 
                             </a> |
                             <a href="javascript:void(0);" class="btn btn-danger" onclick="return isconfirm('<?php echo base_url('doctor/appointment/appointment_delete/' .$id) ?>');">Delete <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                           </div></td>
                           </tr>
                           <?php } 
                         } ?>
                       </tbody>
                     </table>
                   </div>
                   <!-- ./preogress section -->
                 </div>
               </div>
             </div>
             <!-- end panel -->
           </div>
           <!-- /end Admin over view .col-md-12 -->
         </div>
         <!-- end col-md-12 -->
       </div>
       <!-- end #content -->
     </div>
     <!-- end .row -->
   </div>
   <!-- ./end #main  -->
   <?php $this->load->view('common/footer_content');?>
   <script type="text/javascript">
function isconfirm(url_val){
   if(confirm('Are you sure ?') == false)
   {
       return false;
   }
   else
   {
       location.href=url_val;
   }
}
    
    $('#example1').dataTable({
     responsive: true
   });
    
    var fun_attended = function(id){
      $('#messageShow').hide();
              if(id == ''){
                return false;
              }
      var url = base_url+'doctor/appointment/fun_attended';  
      $.ajax({
        type: "POST",
        url: url,
        data: {'id': id},
      })
       .done(function(result){
        var result = JSON.parse(result);
        var message = result.message;
        
        $('#messageShow').show();

        $('#messageShow').html('<p class="alert alert-success">'+message+'</p>'); 
             $('#atten_'+id).hide();
      //  setTimeout(function(){ location.reload(); }, 3000);

      }); 

    }

    var current_patient = function(id,status,clinic_id){
      
      if(id == ''){
        return false;
      }
      $('#messageShow').hide();
      
      var result = confirm("Do you want to change status");
      if (result) {
       var url = base_url+'doctor/appointment/appointment_status_change';  
       $.ajax({
        type: "POST",
        url: url,
        data: {'id': id,'status': status,'clinic_id': clinic_id},
      })
       .done(function(result){
        var result = JSON.parse(result);
        var message = result.message;
        
        $('#messageShow').show();

        $('#messageShow').html('<p class="alert alert-success">'+message+'</p>'); 

        setTimeout(function(){ location.reload(); }, 3000);

      }); 
     }
   }
 </script>
 <?php $this->load->view('common/footer');?>
