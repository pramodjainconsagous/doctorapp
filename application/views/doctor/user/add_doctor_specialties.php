<?php //echo"<pre>"; print_r($doctor_specialties);exit;?>
<?php $this->load->view('common/header');?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
<?php $this->load->view('common/admin_header'); ?>

<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->

<div id="main" class="main">
	<div class="row"> 
		<!-- breadcrumb section -->
		<div class="ribbon">
			<ul class="breadcrumb">
				<li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
			</ul>
		</div>
		<?php $this->load->view('common/message'); ?>
		<div id="content">
			<div id="sortable-panel" class="">
				<div id="titr-content" class="col-md-12">
					<h2><?php echo ucwords($title);?></h2>
				</div>

				<!-- Admin over view .col-md-12 col-md-offset-3-->
				<div class="col-md-12">
					<div  class="panel panel-default">
						
						<div class="panel-heading">
							<div class="panel-title"> <i class="fa fa-edit"></i> <?php echo ucwords($title);?>
								<div class="bars pull-right"> <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a> <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a> </div>
							</div>
						</div>
						<div class="panel-body"> 

							<!-- middel content section --> 
							<div class="row"> 
								<div class="panel-body">

									<form action="<?php echo base_url('doctor/profile/edit_doctor_specialties/');  ?>" role="form" id="doctor_specialties_form" novalidate method="post" enctype="multipart/form-data">
										<!-- <div class="row"> -->
											<div class="col-md-12">
												<div class="form-group">
													<?php if(!empty($message)){ ?>
													<div class="alert alert-danger"> <?php echo $message;  ?></div>

													<?php } ?>
												</div>
											</div>
											 <div class="form-group">
												<label class="control-label">
													Doctor Name <span class="symbol required" aria-required="true"></span>
												</label>
												<select class="form-control" name="doctor_name" id="doctor_name" >
													<?php foreach($doctor_name as $key => $value){ ?>

													<option value="<?php echo $value['id'];?>" ><?php echo $value['doctor_name'];?></option>

													<?php  } ?>
												</select>

											</div> 
											<div class="form-group">
												<label class="control-label">
													Doctor specialties <span class="symbol required" aria-required="true"></span>
												</label>



												<select class="form-control" name="doctor_specialties[]" id="doctor_specialties" multiple="multiple">
													<?php foreach($result as $key => $value){ ?>
													<option value="<?php echo $value['id'];?>"   <?php if(in_array($value['id'], $doctor_specialties)){echo "selected";} ?> ><?php echo $value["name"];?></option>
													<?php  } ?>
												</select>
												<!-- </div> -->
												<!-- left side end -->
											</div>
											<div class="alert alert-warning"> <span class="label label-warning">NOTE!</span> <span> For windows: Hold down the control (ctrl) button to select multiple options </span> 
													<br>
													<span class="label label-warning">NOTE!</span> <span> For Mac: Hold down the command button to select multiple options </span>
												</div>
											<div class="row">
												<!-- <div class="col-md-2 col-sm-4 col-xs-6 full-xs bottom-margin-xs">
													<a href="<?php #echo base_url('doctor/profile');?>" class="btn btn-light-grey btn-block">								
														<i class="fa fa-arrow-circle-left"></i> <?php #echo BACK; ?>  							
													</a>
												</div> -->
												<div class="col-md-3 col-sm-6 col-xs-6 full-xs">

													<button class="btn btn-success btn-block" type="submit">
														<?php echo UPDATE; ?>  <i class="fa fa-arrow-circle-right"></i>
													</button>

												</div>
											</div>
										</form>

									</div>
								</div>
								<!-- end #content --> 
							</div>
						</div>
					</div>
					<!-- end of admin over view -->
				</div>
			</div>
		</div>
		<!-- end .row --> 
	</div>
	<!-- ./end #main  -->


	<?php $this->load->view('common/footer_content');?>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA02ImK47DaSgcOQjNMyPnKQ9hDZgTT-x4&libraries=places&callback=initialize"
	async defer></script>
	<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
	<!-- write jquery for ajax set doctor name  -->

	<script type="text/javascript">
			$(document).ready(function() {
				//var doctor_name = $('#doctor_name').val();
				$("#doctor_name").select2();
			});
	</script>
	<script>

		$('#doctor_name').on('change',function()
		{   
			var cu_this = $('#doctor_specialties');
			id = $(this).find(':selected').val();
			$.ajax(
			{
				method: "POST",
				url: "<?php echo base_url();?>"+'doctor/profile/single_doctor_specialties',
				data: {"id":id},
				success : function(result)
				{
					set = cu_this.find('option');
					for (var i = 0; i<set.length;i++)
					{
						$(set[i]).removeAttr('selected');
						results = JSON.parse(result);
						get = $(set[i]).attr('value');
						if(jQuery.inArray(get, results) !== -1)
						{
							$(set[i]).attr('selected','selected');
						}
					}
				}
			});
		});
		$('#doctor_name').on('click',function()
		{   
			var cu_this = $('#doctor_specialties');
			id = $(this).find(':selected').val();
			$.ajax(
			{
				method: "POST",
				url: "<?php echo base_url();?>"+'doctor/profile/single_doctor_specialties',
				data: {"id":id},
				success : function(result)
				{
					set = cu_this.find('option');
					for (var i = 0; i<set.length;i++)
					{
						$(set[i]).removeAttr('selected');
						results = JSON.parse(result);
						get = $(set[i]).attr('value');
						if(jQuery.inArray(get, results) !== -1)
						{
							$(set[i]).attr('selected','selected');
						}
					}
				}
			});
		});
//Function for validate add/edit  form
$(document).ready(function()
{
	$("#doctor_specialties_form").validate({
		//ignore: [],  
		rules: {
			doctor_name: {
				required : true
			},
			"doctor_specialties[]": {
				required : true
			}
		},
		messages: {
			doctor_name: {
				required : "Please selecte doctor name."
			},
			"doctor_specialties[]": {
				required : "Please selecte doctor specialties.",
			}
		},		
		ignore: [],
		focusInvalid: false,
		submitHandler: function(form)
		{
			return true;
		}
	});
});


</script>

<script type="text/javascript" src="<?php echo base_url('assets/js/custom/address_change_map.js'); ?>"></script>
<?php $this->load->view('common/footer');?>
