<?php $this->load->view('common/header');?>
<?php $this->load->view('common/admin_header'); ?>
<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->
<!-- main content  -->

<div id="main" class="main">
  <div class="row">
    <!-- breadcrumb section -->
    <div class="ribbon">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
      </ul>
    </div>
    <?php $this->load->view('common/message'); ?>
    <!-- main content -->
    <div id="content">
      <div id="sortable-panel" class="">
        <div id="titr-content" class="col-md-12">
          <h2><?php echo ucwords($title); ?></h2>
          <h5>&nbsp;</h5>
         <!--  <div class="actions"> <a href="<?php echo base_url('doctor/profile/staffAdd');?>" class="btn btn-success  has-ripple"> <?php echo ADD_NEW; ?> Staff </a>  </div> -->
        </div>
        <!-- Admin over view .col-md-12 -->
        <div class="col-md-12 ">
          <div  class="panel panel-default">
            <div class="panel-body"> <i class="glyphicon glyphicon-stats"></i> <b><?php echo ucwords($title); ?>
              <hr>
              <div class="row">
                <!-- progress section -->
                <div class="panel-body">
                  <form action="<?php echo base_url('doctor/profile/chat_settings/');  ?>" role="form" id="doctor_specialties_form" novalidate method="post" enctype="multipart/form-data">
                    <!-- <div class="row"> -->
                      <div class="col-md-12">
                        <div class="form-group">
                          <?php if(!empty($message)){ ?>
                          <div class="alert alert-danger"> <?php echo $message;  ?></div>

                          <?php } ?>
                        </div>
                      </div> 
                      <div class="form-group">
                        
                      
                    <div class="col-md-12">
                      <div class="form-group">
                      <div class="col-md-12">
                       <label class="control-label"><input type="radio" name="chat_setting" value="0"
                        <?php if($result[0]['chat_setting'] == '0'){?> checked <?php }?>  > Turn Off</label>
                     </div>

                      <div class="col-md-12">
                      <span class="label label-warning">NOTE!</span> <span> Patient will not be able to send any chat message to this doctor on any day but will be able to see chat history if any. </span> 
                    </div>
                    </div>
                  </div>
                    <div class="col-md-12">
                          <div class="form-group">
                          </div>
                        </div>
                      
                       <div class="col-md-12">
                        <div class="form-group">
                         <div class="col-md-12">
                        <label class="control-label"><input type="radio" name="chat_setting" value="1" <?php if($result[0]['chat_setting'] == '1'){?> checked <?php }?> > On Appointment day Only</label>
                      </div>

                       <div class="col-md-12">
                         <span class="label label-warning ">NOTE!</span> <span> Where a patient will be able to see chat history if any, but will be able to send new chat message only if its the appointment day. </span> 
                       </div>
                       </div>
                    </div>


                         
                         <div class="col-md-12">
                          <div class="form-group">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                           <div class="col-md-12">
                        <label class="control-label"><input type="radio" name="chat_setting" value="2" <?php if($result[0]['chat_setting'] == '2'){?> checked <?php }?> > Open For Any Time</label>
                      </div>

                       <div class="col-md-12">
                   
                        <span class="label label-warning">NOTE!</span> <span> As it currently </span> 
                      </div>
                      </div>

                      <div class="col-md-12">
                          <div class="form-group">
                          </div>
                        </div>

                    </div>
                      </div>
                      <div class="row">
                        <!-- <div class="col-md-2 col-sm-4 col-xs-6 full-xs bottom-margin-xs">
                          <a href="<?php #echo base_url('doctor/profile');?>" class="btn btn-light-grey btn-block">               
                            <i class="fa fa-arrow-circle-left"></i> <?php #echo BACK; ?>                
                          </a>
                        </div> -->
                        <div class="col-md-3 col-sm-6 col-xs-6 full-xs">

                          <button class="btn btn-success btn-block" type="submit">
                            <?php echo UPDATE; ?>  <i class="fa fa-arrow-circle-right"></i>
                          </button>

                        </div>
                      </div>
                    </form>
                </div>
                <!-- ./preogress section -->
              </div>
            </div>
          </div>
          <!-- end panel -->
        </div>
        <!-- /end Admin over view .col-md-12 -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end #content -->
  </div>
  <!-- end .row -->
</div>
<!-- ./end #main  -->
<?php $this->load->view('common/footer_content');?>

<?php $this->load->view('common/footer');?>
