<?php $this->load->view('common/header');?>
<?php $this->load->view('common/admin_header'); ?>
<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->
<!-- main content  -->

<div id="main" class="main">
  <div class="row">
    <!-- breadcrumb section -->
    <div class="ribbon">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
      </ul>
    </div>
    <?php $this->load->view('common/message'); ?>
    <!-- main content -->
    <div id="content">
      <div id="sortable-panel" class="">
        <div id="titr-content" class="col-md-12">
          <h2><?php echo ucwords($title); ?></h2>
          <h5>&nbsp;</h5>
          <div class="actions">  </div>
        </div>
        <!-- Admin over view .col-md-12 -->
        <div class="col-md-12 ">
          <div  class="panel panel-default">
            <div class="panel-body"> <i class="glyphicon glyphicon-stats"></i> <b><?php echo ucwords($title); ?>
              <hr>
              <div class="row">
                <!-- progress section -->
                <div class="panel-body">
                  <table id="example1" class="table table-striped table-bordered width-100 cellspace-0" >
                    <thead>
                      <tr>
                        <th>Doctor Image</th>
                        <th>Office in/out</th>
                        <th>Doctor Name</th>
                        <th>Doctor Address</th>
                        <th>Clinic Number</th>
                        
                        <th>Clinic Name</th>
                        <th>Doctor Phone No</th>
                        <th>Doctor Email</th>
                        
                        <th>Register Date</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
						if($result){ 

						   foreach($result as $val){
                /*echo"<pre>";
                print_r($result);*/
							$id = safe_b64encode($val["id"]); 
              $img = image_check($val['picture_url'],USER_IMAGE);
					 ?>
                      <tr>
                         <td> <img src="<?php echo $img; ?>" alt="<?php echo $result[0]['ads_img']; ?>" style="height: 60px; width: 60px;" > </td>
                        <td>
                        <?php echo $val['doctor_status']; 
                        if($val['doctor_status']=='Unavailable')
                        {?>
                        <button type="button" class="btn btn-primary" onclick='off_on("<?php echo $val["id"];?>","1")'>office in</button>
                        <?php }
                        else
                        {?>
                          <button type="button" class="btn btn-danger" onclick='off_on("<?php echo $val["id"];?>","0")'>office out</button>
                       <?php }?>
                        </td>
                         <td><?php echo $val['doctor_name']; ?></td>
                        <td><?php echo $val['clinic_address']; ?></td>
                         <td><?php echo $val['phone_number']; ?></td>
                        <td><?php echo $val['clinic_name']; ?></td>
                        <td><?php echo $val['doctor_phone_no']; ?></td>
                        <td><?php echo $val['doctor_email_id']; ?></td>
                       


                         <td><?php echo convert_datetime($val['create_dt']); ?></td>

                        <td><?php echo $val['status']; ?></td>
                        <td><div class="action-buttons"> 

                          <a title="Edit"  class="btn btn-info" href="<?php echo base_url('doctor/profile/edit/'.$id) ?>"> 
                           Edit <i class="fa fa-pencil" aria-hidden="true"></i> 
                         </a> </div></td>
                      </tr>
                      <?php } 
										       } ?>
                    </tbody>
                  </table>
                </div>
                <!-- ./preogress section -->
              </div>
            </div>
          </div>
          <!-- end panel -->
        </div>
        <!-- /end Admin over view .col-md-12 -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end #content -->
  </div>
  <!-- end .row -->
</div>
<!-- ./end #main  -->
<?php $this->load->view('common/footer_content');?>
<script type="text/javascript">
	 $('#example1').dataTable({
		    	responsive: true
		    });

   function off_on(id,status)
   {
    $.ajax({
      method: "POST",
      url: "<?php echo base_url();?>"+'doctor/profile/office_off_on',
      data: {"id":id,"status":status},
      success : function(result)
      {
        location.reload();
      }
    });
   }
</script>
<?php $this->load->view('common/footer');?>
