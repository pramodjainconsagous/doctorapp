<?php $this->load->view('mail_template/mail_header'); ?>
<h3>
	Hi <?=ucwords($full_name);?></h3>
<p>
	Thank you for registering with <a href="<?php echo base_url(); ?>"><?php echo config_item('site_name'); ?></a></p>
 
<p>
	<a href="<?=base_url('login?verifier='.$activation_code); ?>"><button class="red_button" type="button">Email verification</button></a></p>

<p>
	Copy paste this url</p>

<p>
	<?=base_url('login?verifier='.$activation_code); ?></p>
<p>
	Can&#39;t see the button? Use this link: <a href="<?=base_url('login?verifier='.$activation_code)?>">Click here</a></p>
	
	<br/>
	<h3>Thanks</h3>
	<p><?php echo config_item('site_name'); ?></p>
<?php $this->load->view('mail_template/mail_footer'); ?>