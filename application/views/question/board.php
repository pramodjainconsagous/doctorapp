<?php $this->load->view('common/header');?>
<?php $this->load->view('common/admin_header'); ?>
<!-- sidebar menu -->
<?php $this->load->view('common/sidebar'); ?>
<!-- /end #sidebar -->
<!-- main content  -->

<div id="main" class="main">
  <div class="row">
    <!-- breadcrumb section -->
    <div class="ribbon">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="<?php echo base_url('Dashboard'); ?>">Home</a> </li>
      </ul>
    </div>
    <?php $this->load->view('common/message'); ?>
    <!-- main content -->
    <div id="content">
      <div id="sortable-panel" class="">
        <div id="titr-content" class="col-md-12">
          <h2><?php echo ucwords($title); ?></h2>
          <h5>&nbsp;</h5>
          <div class="actions"> <a href="<?php echo base_url('setting/addCountry');?>" class="btn btn-success  has-ripple"> <?php echo ADD_NEW; ?> </a> </div>
        </div>
        <!-- Admin over view .col-md-12 -->
        <div class="col-md-12 ">
          <div  class="panel panel-default">
            <div class="panel-body"> <i class="glyphicon glyphicon-stats"></i> <b><?php echo ucwords($title); ?>
              <hr>
              <div class="row">
                <!-- progress section -->
                <div class="panel-body">
                  <table id="example1" class="table table-striped table-bordered width-100 cellspace-0 collaptable" >
                    <thead>
                      <tr>
                       <th>Name</th>
                        <th>Title</th>
                          <th>Is Parent</th>
                            <th>Status</th>
                       
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
						if($result){ 
              $i = 1;
						   foreach($result as $val){
							$id = safe_b64encode($val["id"]); 
					 ?>
                      <tr data-id="<?php echo $i; ?>" data-parent="">
                        <td><?php echo $val['app_name']; ?></td>
                        <td><?php echo $val['title']; ?></td>
                         <td><?php if($val['parent_id'] == 0){ echo 'Parent'; }else{ echo 'Children'; } ?></td>
                       
                        <td><?php echo $val['status']; ?></td>
                        <td><div class=" action-buttons"> 
						
						
						<a title="State view" href="<?php echo base_url('Question/board_children/' .$id) ?>" class="btn btn-xs btn-success tooltips" data-placement="top" data-original-title="View"> <i class="fa fa-eye"></i></a> - 
						
						 <a title="Edit country" href="<?php echo base_url('setting/editCountry/' .$id) ?>" class="btn btn-xs btn-primary tooltips" data-placement="top" data-original-title="Edit"> <i class="fa fa-pencil"></i></a> 
						 
						</div></td>
                      </tr>
                         


                       <?php $i++;
                       } 
										       } ?>
                    </tbody>
                  </table>
                </div>
                <!-- ./preogress section -->
              </div>
            </div>
          </div>
          <!-- end panel -->
        </div>
        <!-- /end Admin over view .col-md-12 -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end #content -->
  </div>
  <!-- end .row -->
</div>
<!-- ./end #main  -->
<?php $this->load->view('common/footer_content');?>
<script type="text/javascript">
	 $('#example1').dataTable({
		    	responsive: true
		    });

   $('.collaptable').aCollapTable({ 

// the table is collapased at start
startCollapsed: true,

// the plus/minus button will be added like a column
addColumn: true, 

// The expand button ("plus" +)
plusButton: '<span class="i">+</span>', 

// The collapse button ("minus" -)
minusButton: '<span class="i">-</span>' 
  
});

</script>





<?php $this->load->view('common/footer');?>
