<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
<title>Login Panel</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo $this->config->item('site_url') ?>assets/vendors/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $this->config->item('site_url') ?>assets/vendors/font-awesome/css/font-awesome.min.css">
<!-- <link rel="stylesheet" href="../assets/css/yep-rtl.css"> -->

<!-- Related css to this page -->
<link rel="stylesheet" href="<?php echo $this->config->item('site_url') ?>assets/vendors/animate/css/animate.min.css">

<!-- Yeptemplate css --><!-- Please use *.min.css in production -->
<link rel="stylesheet" href="<?php echo $this->config->item('site_url') ?>assets/css/yep-style.css">
<link rel="stylesheet" href="<?php echo $this->config->item('site_url') ?>assets/css/yep-vendors.css">

<!-- favicon -->
<link rel="shortcut icon" href="<?php echo $this->config->item('site_url') ?>assets/img/favicon/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo $this->config->item('site_url') ?>assets/img/favicon/favicon.ico" type="image/x-icon">
</head>

<!-- You can use .rtl CSS in #login-page -->
<body id="mainbody" class="login-page" >
<canvas id="spiders" class="hidden-xs" ></canvas>
<div class="">
  <div style="margin: 5% auto; position: relative; width: 80%;">
    <div id="sign-form" class="panel panel-default" style="height:300px;">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <div class="text-center">
              <h2><?php echo $this->config->item('site_name') ?></h2>
			  <div class="<?php echo $class; ?>" ><?php echo $msg; ?></div>
            </div>
            
            </div>
        </div>
      </div>
    </div>
    
   
  </div>
</div>

<!-- General JS script library--> 
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-ui/js/jquery-ui.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-searchable/js/jquery.searchable.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js"></script> 

<!-- Yeptemplate JS Script --><!-- Please use *.min.js in production --> 
<script type="text/javascript" src="<?php echo $this->config->item('site_url') ?>assets/js/yep-script.js"></script> 

<!-- Related JavaScript Library to This Pagee --> 

<!-- Plugins Script --> 

</body>
</html>