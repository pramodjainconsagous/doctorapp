var map;
var autocomplete = {};
var autocomplete;
var markers = [];
var marker = '';
var markericon = base_url+'assets/images/map-pointer.png';
var init_lat = 22.7196;
var init_lang = 75.85773;

var geocoder = new google.maps.Geocoder();
 

 function initialize() {
  //alert(init_lat+'--'+init_lang+'---'+markericon);
    var latlng = new google.maps.LatLng(parseFloat(init_lat), parseFloat(init_lang)); 
    var mapOptions = {
      zoom: 8,
      center: latlng,
	  icon: markericon
    }
	 map = new google.maps.Map(document.getElementById('map'), mapOptions);


    marker = new google.maps.Marker({
          position: map.getCenter(),
          icon: markericon,
          map: map,
          title: 'drag address',
          draggable: true
        });

/* document.getElementById('address').addEventListener('click', function() {
          codeAddress(geocoder, map);
        });
*/
	 initializeAutocomplete();
   codeAddress();
  }
  
 function initializeAutocomplete() {

  autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('address')),
            {types: ['geocode']});


  /*autocomplete = new google.maps.places.Autocomplete(
    document.getElementById('address'),
    {
      types: ['geocode']
    }
  );*/

   autocomplete.addListener('place_changed', fillInAddress);
   
 /* google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });*/
}


function fillInAddress() {
  var place = autocomplete.getPlace();
  var latitude = place.geometry.location.lat();
  var longitude = place.geometry.location.lng();

  if(latitude != '' && longitude != ''){
    init_lat = latitude;
    init_lang = longitude;

  }
  $('#latitude').val(latitude);
  $('#longitude').val(longitude);
  var formatted_address = place.formatted_address;
  codeAddress();
}

$(function(){
    $('#address').focusout(function(){
     initialize();
		var address = document.getElementById("address").value;
		codeAddress();
  });
});
  

/*  function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }*/


function codeAddress() {

    var address = document.getElementById('address').value;
    if(address == '') {
        alert("Address can not be empty!");
        return;
    }
		
    geocoder.geocode( { 'address': address}, function(results, status) {
      
    
      if (status == google.maps.GeocoderStatus.OK) 
      {
		  clearMarkers();
          var lat = results[0].geometry.location.lat();
          var lng = results[0].geometry.location.lng();
          
        
          $("#latitude").val(results[0].geometry.location.lat());
          $("#longitude").val(results[0].geometry.location.lng());
          map.setCenter(results[0].geometry.location);
          marker = new google.maps.Marker({
          map: map, 
          position:results[0].geometry.location,
          icon: markericon,
		      title: 'drag address',
          draggable: true

        });
		markers.push(marker);
        google.maps.event.addListener(marker, 'dragend', function() {
        geocodePosition(marker.getPosition());
        });
     
      } 
    });
}
  
function geocodePosition(pos) {
    geocoder.geocode({
      latLng: pos
    }, function(responses) {
      if (responses && responses.length > 0) {
      
      document.getElementById('address').value=responses[0].formatted_address;
      } else {
        alert('Cannot determine address at this location.');
      }
    });
}
  
function clearMarkers() {
  if (markers) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
  }
  markers = new Array();
}

google.maps.event.addDomListener(window,'load', initialize);