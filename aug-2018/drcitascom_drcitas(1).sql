-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 30, 2018 at 05:02 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drcitascom_drcitas`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `click_name` varchar(250) NOT NULL,
  `about_us` text NOT NULL,
  `picture_url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `name`, `click_name`, `about_us`, `picture_url`) VALUES
(1, 'dr abc1', 'dental clinic2', 'loren lipson3', '');

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(11) NOT NULL,
  `ads_title` varchar(250) NOT NULL,
  `ads_description` text NOT NULL,
  `ads_img` varchar(100) NOT NULL,
  `ads_link` text NOT NULL,
  `ads_type` varchar(50) NOT NULL,
  `status` enum('Active','inActive') NOT NULL,
  `create_dt` int(11) NOT NULL,
  `update_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ads_associates_doctor`
--

CREATE TABLE `ads_associates_doctor` (
  `id` int(11) NOT NULL,
  `ads_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ads_associates_doctor`
--

INSERT INTO `ads_associates_doctor` (`id`, `ads_id`, `doctor_id`) VALUES
(69, 19, 235),
(70, 18, 60),
(75, 20, 241),
(84, 27, 57),
(87, 16, 57),
(88, 17, 57),
(89, 28, 57),
(92, 31, 57),
(93, 32, 57),
(94, 29, 57),
(95, 30, 57),
(96, 21, 60);

-- --------------------------------------------------------

--
-- Table structure for table `ads_associates_specialties`
--

CREATE TABLE `ads_associates_specialties` (
  `id` int(11) NOT NULL,
  `ads_id` int(11) NOT NULL,
  `specialties_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ads_associates_specialties`
--

INSERT INTO `ads_associates_specialties` (`id`, `ads_id`, `specialties_id`) VALUES
(39, 14, 2),
(71, 19, 3),
(72, 18, 3),
(82, 20, 2),
(83, 20, 4),
(87, 23, 4),
(90, 26, 1),
(120, 21, 1),
(121, 21, 2),
(122, 21, 3),
(123, 21, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ads_type`
--

CREATE TABLE `ads_type` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ads_id` int(11) NOT NULL,
  `date` varchar(20) NOT NULL,
  `ads_type` varchar(20) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ads_type`
--

INSERT INTO `ads_type` (`id`, `user_id`, `ads_id`, `date`, `ads_type`, `status`) VALUES
(281, 57, 27, '2018-08-13', 'doctor', '0'),
(282, 57, 16, '2018-08-13', 'doctor', '0'),
(283, 57, 17, '2018-08-13', 'doctor', '0'),
(284, 57, 28, '2018-08-13', 'doctor', '0'),
(285, 57, 29, '2018-08-13', 'doctor', '0'),
(286, 57, 30, '2018-08-13', 'doctor', '0'),
(287, 57, 31, '2018-08-13', 'doctor', '0'),
(288, 57, 32, '2018-08-13', 'doctor', '0'),
(289, 57, 14, '2018-08-13', 'speciality', '0'),
(290, 57, 20, '2018-08-13', 'speciality', '0'),
(291, 57, 21, '2018-08-13', 'speciality', '0'),
(312, 60, 18, '2018-08-28', 'doctor', '0'),
(313, 60, 20, '2018-08-28', 'speciality', '0'),
(314, 60, 23, '2018-08-28', 'speciality', '0'),
(315, 60, 21, '2018-08-28', 'speciality', '0');

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` int(11) NOT NULL,
  `clinic_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `token_no` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `time_slote` varchar(200) NOT NULL,
  `time_id` int(11) NOT NULL,
  `status` enum('scheduled','confirmed','current','next','attended','cancel') NOT NULL,
  `visiting_status` int(11) NOT NULL,
  `booking_date` varchar(20) NOT NULL,
  `create_dt` int(11) NOT NULL,
  `update_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `clinic_id`, `patient_id`, `token_no`, `description`, `time_slote`, `time_id`, `status`, `visiting_status`, `booking_date`, `create_dt`, `update_dt`) VALUES
(12, 60, 246, 0, '', '', 0, 'scheduled', 1, '2018-03-08', 1520502540, 0),
(19, 60, 237, 33, 'tesrt', '9:00 AM', 2, 'scheduled', 1, '2018-03-23', 1520577495, 0),
(37, 60, 246, 73, '', '7:00 PM', 0, 'scheduled', 1, '2018-03-09', 1520600967, 0),
(38, 60, 237, 74, '', '7:15 PM', 0, 'scheduled', 1, '2018-03-09', 1520600989, 0),
(39, 60, 67, 75, '', '7:30 PM', 0, 'scheduled', 1, '2018-03-09', 1520601010, 0),
(40, 60, 258, 76, '', '7:45 PM', 0, 'scheduled', 1, '2018-03-09', 1520601025, 0),
(41, 60, 189, 77, '', '8:00 PM', 0, 'scheduled', 1, '2018-03-09', 1520601050, 0),
(43, 60, 246, 51, '', '1:30 PM', 51, 'attended', 1, '2018-03-10', 1520665513, 0),
(44, 60, 237, 56, '', '2:45 PM', 94, 'scheduled', 1, '2018-03-10', 1520665531, 0),
(45, 60, 67, 54, '', '2:15 PM', 93, 'scheduled', 1, '2018-03-10', 1520665621, 0),
(46, 60, 189, 52, '', '1:45 PM', 52, 'current', 1, '2018-03-10', 1520665638, 0),
(47, 60, 230, 33, '', '9:00 AM', 33, 'scheduled', 4, '2018-03-10', 1520685025, 0),
(58, 60, 67, 84, '', '9:45 PM', 7, 'scheduled', 1, '2018-03-12', 1520853672, 0),
(61, 60, 189, 90, '', '11:15 PM', 12, 'scheduled', 1, '2018-03-12', 1520853720, 0),
(62, 60, 256, 79, '', '8:30 PM', 6, 'scheduled', 1, '2018-03-12', 1520853751, 0),
(63, 60, 257, 74, '', '7:15 PM', 5, 'scheduled', 1, '2018-03-12', 1520853780, 0),
(64, 60, 224, 28, '', '7:45 AM', 8, 'scheduled', 1, '2018-03-12', 1520854468, 0),
(65, 60, 230, 29, '', '8:00 AM', 9, 'scheduled', 4, '2018-03-12', 1520854510, 0),
(66, 60, 291, 40, 'test', '10:45 AM', 10, 'scheduled', 1, '2018-03-12', 1520855461, 0),
(67, 60, 261, 42, '', '11:15 AM', 11, 'scheduled', 1, '2018-03-12', 1520855631, 0),
(68, 60, 264, 87, '', '10:30 PM', 12, 'scheduled', 1, '2018-03-12', 1520855886, 0),
(70, 60, 246, 77, '', '8:00 PM', 12, 'scheduled', 1, '2018-03-12', 1520860363, 0),
(71, 60, 237, 83, 'test', '9:30 PM', 12, 'scheduled', 1, '2018-03-12', 1520861504, 0),
(72, 60, 258, 75, '', '7:30 PM', 2, 'scheduled', 1, '2018-03-12', 1520861644, 0),
(73, 60, 230, 29, '', '8:00 AM', 1, 'scheduled', 1, '2018-03-13', 1520940909, 0),
(74, 60, 224, 28, '', '7:45 AM', 3, 'scheduled', 2, '2018-03-13', 1520940952, 0),
(76, 60, 237, 54, '', '2:15 PM', 5, 'scheduled', 1, '2018-03-14', 1520947505, 0),
(77, 60, 237, 1, '', '1:00 AM', 1, 'attended', 1, '2018-03-15', 1520947695, 1521099605),
(78, 60, 72, 87, 'testing', '10:30 PM', 2, 'scheduled', 1, '2018-03-13', 1520948542, 0),
(79, 60, 246, 50, '', '1:15 PM', 2, 'attended', 1, '2018-03-14', 1521004063, 0),
(80, 60, 67, 51, '', '1:30 PM', 3, 'current', 1, '2018-03-14', 1521004108, 0),
(81, 60, 258, 52, '', '1:45 PM', 6, 'scheduled', 1, '2018-03-14', 1521004134, 0),
(82, 60, 189, 53, '', '2:00 PM', 3, 'scheduled', 1, '2018-03-14', 1521004159, 0),
(83, 60, 86, 1, '', '1:00 AM', 3, 'current', 1, '2018-03-14', 1521004176, 0),
(84, 60, 224, 33, '', '9:00 AM', 7, 'scheduled', 1, '2018-03-14', 1521031852, 0),
(85, 60, 247, 68, '', '5:45 PM', 8, 'scheduled', 1, '2018-03-14', 1521031936, 0),
(86, 60, 230, 59, '', '3:30 PM', 9, 'scheduled', 1, '2018-03-14', 1521031953, 0),
(87, 60, 256, 72, '', '6:45 PM', 10, 'scheduled', 1, '2018-03-14', 1521031973, 0),
(88, 60, 246, 41, '', '11:00 AM', 6, 'scheduled', 3, '2018-03-15', 1521090630, 1521117769),
(89, 60, 67, 33, '', '9:00 AM', 3, 'scheduled', 4, '2018-03-15', 1521090780, 1521119389),
(90, 60, 258, 55, '', '2:30 PM', 5, 'scheduled', 1, '2018-03-15', 1521090794, 0),
(91, 60, 86, 42, '', '11:15 AM', 2, 'current', 1, '2018-03-15', 1521095354, 0),
(92, 60, 230, 44, '', '11:45 AM', 3, 'scheduled', 2, '2018-03-15', 1521117680, 0),
(93, 60, 256, 34, '', '9:15 AM', 7, 'scheduled', 2, '2018-03-15', 1521119144, 0),
(94, 60, 257, 35, '', '9:30 AM', 8, 'scheduled', 1, '2018-03-15', 1521119173, 0),
(95, 60, 247, 36, '', '9:45 AM', 9, 'scheduled', 1, '2018-03-15', 1521119229, 0),
(96, 60, 224, 93, '', '12:00 PM', 10, 'scheduled', 1, '2018-03-15', 1521119313, 0),
(97, 60, 72, 53, 'testing', '2:00 PM', 1, 'scheduled', 1, '2018-03-16', 1521176608, 0),
(98, 60, 224, 65, 'testing', '5:00 PM', 2, 'scheduled', 1, '2018-03-16', 1521177258, 0),
(99, 60, 230, 69, 'testing', '6:00 PM', 3, 'attended', 1, '2018-03-16', 1521177289, 0),
(100, 60, 246, 67, '', '5:30 PM', 4, 'current', 1, '2018-03-16', 1521186836, 0),
(106, 60, 246, 29, '', '8:00 AM', 1, 'scheduled', 1, '2018-03-30', 1521196778, 0),
(107, 60, 246, 23, '', '6:30 AM', 1, 'scheduled', 1, '2018-03-28', 1521204247, 0),
(110, 60, 237, 78, '', '8:15 PM', 5, 'scheduled', 1, '2018-03-19', 1521208461, 1521208495),
(111, 60, 237, 88, '', '10:45 PM', 5, 'scheduled', 1, '2018-03-01', 1521208671, 1521208682),
(114, 60, 237, 8, '', '2:45 AM', 1, 'scheduled', 1, '2018-03-24', 1521210653, 0),
(115, 60, 237, 44, '', '11:45 AM', 1, 'scheduled', 1, '2018-03-25', 1521211100, 0),
(116, 60, 237, 34, '', '9:15 AM', 1, 'scheduled', 1, '2018-03-27', 1521211527, 0),
(117, 60, 237, 37, '', '10:00 AM', 2, 'scheduled', 1, '2018-03-28', 1521212136, 0),
(118, 60, 237, 15, '', '4:30 AM', 2, 'scheduled', 1, '2018-03-30', 1521212221, 0),
(119, 60, 237, 44, '', '11:45 AM', 1, 'scheduled', 1, '2018-03-31', 1521262996, 0),
(120, 60, 237, 55, '', '2:30 PM', 1, 'scheduled', 1, '2018-04-01', 1521263269, 0),
(121, 60, 237, 13, '', '4:00 AM', 1, 'scheduled', 1, '2018-04-02', 1521263516, 0),
(122, 60, 237, 49, '', '1:00 PM', 1, 'scheduled', 1, '2018-04-03', 1521263603, 0),
(123, 60, 246, 32, '', '8:45 AM', 2, 'scheduled', 1, '2018-03-31', 1521264149, 0),
(124, 60, 246, 1, '', '1:00 AM', 1, 'scheduled', 1, '2018-04-04', 1521264907, 0),
(125, 60, 237, 1, '', '1:00 AM', 2, 'scheduled', 1, '2018-04-04', 1521265150, 0),
(126, 60, 246, 67, '', '5:30 PM', 4, 'scheduled', 1, '2018-03-17', 1521265297, 0),
(127, 60, 237, 59, '', '3:30 PM', 6, 'scheduled', 1, '2018-03-20', 1521265815, 0),
(128, 60, 237, 84, '', '9:45 PM', 1, 'scheduled', 1, '2018-03-22', 1521265895, 0),
(129, 60, 237, 80, '', '8:45 PM', 1, 'scheduled', 1, '2018-03-29', 1521267522, 0),
(130, 60, 237, 36, '', '9:45 AM', 1, 'scheduled', 1, '2018-04-05', 1521283558, 0),
(131, 60, 237, 1, '', '1:00 AM', 1, 'scheduled', 1, '2018-04-11', 1521285454, 0),
(132, 60, 237, 73, '', '7:00 PM', 2, 'scheduled', 2, '2018-03-17', 1521289112, 0),
(133, 60, 258, 82, '', '9:15 PM', 3, 'scheduled', 1, '2018-03-17', 1521290323, 0),
(134, 60, 189, 88, '', '10:45 PM', 1, 'scheduled', 1, '2018-03-17', 1521290938, 0),
(135, 60, 247, 39, '', '10:30 AM', 5, 'scheduled', 3, '2018-03-17', 1521296279, 0),
(136, 60, 246, 49, '', '1:00 PM', 2, 'current', 1, '2018-03-20', 1521528064, 0),
(137, 60, 67, 51, '', '1:30 PM', 3, 'scheduled', 1, '2018-03-20', 1521528084, 0),
(138, 60, 258, 53, '', '2:00 PM', 4, 'scheduled', 1, '2018-03-20', 1521528099, 0),
(139, 60, 189, 57, '', '3:00 PM', 5, 'scheduled', 1, '2018-03-20', 1521528114, 0),
(140, 60, 86, 54, '', '2:15 PM', 1, 'attended', 1, '2018-03-20', 1521528130, 0),
(141, 60, 299, 65, '', '5:00 PM', 7, 'scheduled', 1, '2018-03-20', 1521528723, 0),
(142, 60, 302, 51, '', '1:30 PM', 3, 'scheduled', 3, '2018-03-21', 1521642574, 0),
(143, 60, 247, 40, '', '10:45 AM', 1, 'scheduled', 1, '2018-03-21', 1521642717, 0),
(144, 60, 256, 43, '', '11:30 AM', 2, 'scheduled', 2, '2018-03-21', 1521642857, 0),
(145, 60, 224, 59, '', '3:30 PM', 1, 'scheduled', 3, '2018-03-23', 1521809690, 0),
(146, 60, 224, 29, '', '8:00 AM', 2, 'scheduled', 3, '2018-04-11', 1523421337, 0),
(147, 60, 302, 55, '', '2:30 PM', 3, 'scheduled', 3, '2018-04-17', 1523986469, 0),
(148, 60, 224, 53, '', '2:00 PM', 1, 'scheduled', 1, '2018-04-17', 1523986545, 0),
(149, 60, 230, 54, '', '2:15 PM', 2, 'scheduled', 3, '2018-04-17', 1523986886, 0),
(150, 60, 67, 55, 'test', '2:30 PM', 1, 'scheduled', 2, '2018-04-20', 1524205315, 0),
(152, 60, 58, 51, 'asdas', '1:30 PM', 2, 'current', 1, '2018-04-23', 1524469218, 0),
(153, 60, 59, 52, 'dssdfsd', '1:45 PM', 3, 'scheduled', 1, '2018-04-23', 1524469253, 0),
(154, 60, 61, 54, '', '2:15 PM', 4, 'scheduled', 1, '2018-04-23', 1524469450, 0),
(155, 60, 67, 77, '', '8:00 PM', 4, 'scheduled', 1, '2018-04-23', 1524469500, 0),
(156, 60, 230, 89, '', '11:00 PM', 2, 'scheduled', 1, '2018-04-29', 1525052499, 0),
(157, 60, 224, 87, '', '10:30 PM', 1, 'scheduled', 3, '2018-04-29', 1525052693, 0),
(158, 60, 67, 85, 'testing', '10:00 PM', 1, 'scheduled', 1, '2018-05-03', 1525355668, 0),
(159, 316, 256, 53, '', '2:00 PM', 1, 'attended', 3, '2018-05-03', 1525367484, 0),
(160, 316, 257, 54, '', '2:15 PM', 2, 'current', 1, '2018-05-03', 1525367517, 0),
(161, 316, 224, 55, '', '2:30 PM', 3, 'scheduled', 4, '2018-05-03', 1525367550, 0),
(162, 316, 302, 56, '.', '2:45 PM', 4, 'scheduled', 2, '2018-05-03', 1525367595, 0),
(163, 316, 230, 58, '', '3:15 PM', 5, 'scheduled', 1, '2018-05-03', 1525367620, 0),
(164, 316, 319, 61, '', '4:00 PM', 6, 'scheduled', 1, '2018-05-03', 1525370940, 0),
(165, 316, 320, 63, '', '4:30 PM', 7, 'scheduled', 3, '2018-05-03', 1525372668, 0),
(166, 60, 230, 87, '', '10:30 PM', 1, 'current', 3, '2018-05-08', 1525824345, 0),
(167, 60, 230, 87, '', '10:30 PM', 1, 'attended', 3, '2018-05-10', 1525959881, 0),
(169, 316, 224, 39, '', '10:30 AM', 1, 'scheduled', 1, '2018-05-11', 1526043489, 0),
(170, 316, 230, 51, '', '1:30 PM', 2, 'scheduled', 1, '2018-05-11', 1526047370, 0),
(171, 60, 224, 62, '', '4:15 PM', 1, 'scheduled', 1, '2018-05-14', 1526306138, 0),
(172, 60, 256, 61, '', '4:00 PM', 2, 'scheduled', 3, '2018-05-14', 1526306237, 0),
(173, 60, 323, 43, '', '11:30 AM', 3, 'scheduled', 3, '2018-05-14', 1526306879, 0),
(174, 60, 324, 71, '', '6:30 PM', 4, 'scheduled', 1, '2018-05-14', 1526307916, 0),
(175, 60, 230, 55, '', '2:30 PM', 5, 'scheduled', 3, '2018-05-14', 1526311905, 0),
(176, 60, 230, 50, '', '1:15 PM', 1, 'scheduled', 3, '2018-05-16', 1526488787, 0),
(177, 60, 230, 92, '', '11:45 PM', 1, 'current', 3, '2018-05-19', 1526784146, 0),
(178, 60, 230, 68, '', '5:45 PM', 1, 'scheduled', 3, '2018-06-02', 1527972465, 0),
(179, 60, 331, 86, '', '10:15 PM', 1, 'current', 3, '2018-06-05', 1528244676, 0),
(180, 60, 230, 88, '', '10:45 PM', 2, 'scheduled', 1, '2018-06-05', 1528244824, 0),
(181, 60, 224, 69, '', '6:00 PM', 1, 'scheduled', 1, '2018-06-18', 1529352970, 0),
(183, 60, 230, 78, '', '8:15 PM', 2, 'scheduled', 4, '2018-06-18', 1529359355, 0),
(184, 60, 230, 85, '', '10:00 PM', 1, 'current', 1, '2018-06-19', 1529435195, 0),
(185, 60, 335, 77, '', '8:00 PM', 2, 'scheduled', 4, '2018-06-19', 1529449263, 0),
(186, 60, 230, 81, '', '9:00 PM', 1, 'scheduled', 3, '2018-06-22', 1529714021, 0),
(187, 60, 230, 56, '', '2:45 PM', 1, 'scheduled', 1, '2018-06-25', 1529933631, 0),
(188, 60, 331, 19, '', '5:30 AM', 1, 'scheduled', 2, '2018-07-25', 1530047465, 0),
(189, 60, 230, 20, '', '5:45 AM', 2, 'scheduled', 1, '2018-07-25', 1530047719, 0),
(190, 60, 336, 25, '', '7:00 AM', 3, 'scheduled', 1, '2018-07-25', 1530048590, 0),
(192, 60, 331, 1, '', '1:00 AM', 2, 'scheduled', 4, '2018-07-27', 1530058687, 0),
(193, 60, 331, 1, '', '1:00 AM', 1, 'scheduled', 4, '2018-07-07', 1530060154, 0),
(194, 60, 224, 5, '', '2:00 AM', 1, 'scheduled', 3, '2018-06-29', 1530244123, 0),
(195, 60, 256, 59, '', '3:30 PM', 2, 'scheduled', 1, '2018-06-29', 1530286525, 0),
(196, 60, 257, 62, '', '4:15 PM', 3, 'scheduled', 3, '2018-06-29', 1530286563, 0),
(197, 60, 331, 64, '', '4:45 PM', 4, 'scheduled', 4, '2018-06-29', 1530286774, 0),
(198, 60, 230, 63, '', '4:30 PM', 5, 'scheduled', 3, '2018-06-29', 1530287112, 0),
(199, 60, 246, 1, '', '1:00 AM', 0, 'attended', 1, '2018-07-26', 1532608884, 0),
(202, 60, 320, 60, '', '3:45 PM', 0, 'scheduled', 1, '2018-07-26', 1532613861, 1532628883),
(203, 60, 230, 41, '', '11:00 AM', 0, 'current', 1, '2018-07-26', 1532613894, 1532614093),
(204, 60, 224, 61, '', '4:00 PM', 0, 'scheduled', 1, '2018-07-26', 1532628913, 0),
(206, 57, 246, 92, '', '11:45 PM', 3, 'scheduled', 1, '2018-07-27', 1532685338, 0),
(207, 57, 67, 76, 'sdjhsdfb bjkd bjvbjk fhhf fd', '7:45 PM', 1, 'attended', 1, '2018-07-27', 1532688434, 0),
(208, 57, 184, 65, '', '5:00 PM', 1, 'scheduled', 1, '2018-07-27', 1532689202, 0),
(209, 60, 230, 58, '', '3:15 PM', 2, 'scheduled', 1, '2018-07-27', 1532716459, 0),
(210, 60, 342, 42, '', '11:15 AM', 1, 'attended', 1, '2018-07-30', 1532962520, 0),
(213, 60, 224, 68, '', '5:45 PM', 3, 'current', 1, '2018-07-30', 1532984001, 0),
(214, 60, 320, 82, '', '9:15 PM', 4, 'scheduled', 3, '2018-07-30', 1532984041, 0),
(215, 60, 257, 77, '', '8:00 PM', 5, 'scheduled', 4, '2018-07-30', 1532984072, 0),
(216, 60, 342, 57, '', '3:00 PM', 1, 'current', 1, '2018-07-31', 1533054915, 0),
(217, 60, 224, 58, '', '3:15 PM', 2, 'scheduled', 1, '2018-07-31', 1533054940, 0),
(218, 60, 320, 59, '', '3:30 PM', 4, 'scheduled', 1, '2018-07-31', 1533055002, 0),
(219, 60, 343, 54, '', '2:15 PM', 3, 'scheduled', 4, '2018-07-31', 1533055645, 1533055720),
(220, 60, 230, 62, '', '4:15 PM', 5, 'scheduled', 1, '2018-07-31', 1533062866, 0),
(221, 60, 224, 81, '', '9:00 PM', 1, 'attended', 1, '2018-08-01', 1533170681, 0),
(222, 60, 230, 82, '', '9:15 PM', 3, 'scheduled', 3, '2018-08-01', 1533170723, 0),
(223, 60, 342, 85, '', '10:00 PM', 2, 'current', 3, '2018-08-01', 1533170790, 0),
(224, 60, 320, 38, '', '10:15 AM', 2, 'attended', 3, '2018-08-10', 1533905199, 1533908886),
(225, 60, 343, 35, '', '9:30 AM', 1, 'attended', 4, '2018-08-10', 1533906211, 0),
(226, 60, 230, 43, '', '11:30 AM', 5, 'scheduled', 1, '2018-08-10', 1533908856, 0),
(228, 60, 246, 42, '', '11:15 AM', 3, 'current', 4, '2018-08-10', 1533912579, 0),
(229, 57, 59, 67, 'loren', '5:30 PM', 1, 'scheduled', 1, '2018-08-13', 1534157584, 0),
(230, 60, 230, 32, '', '8:45 AM', 1, 'scheduled', 1, '2018-08-13', 1534161326, 0),
(231, 60, 230, 67, '', '5:30 PM', 4, 'scheduled', 3, '2018-08-16', 1534452419, 0),
(232, 60, 342, 68, '', '5:45 PM', 1, 'attended', 1, '2018-08-16', 1534452531, 0),
(233, 60, 341, 72, '', '6:45 PM', 2, 'current', 2, '2018-08-16', 1534452598, 0),
(234, 60, 256, 79, '', '8:30 PM', 3, 'scheduled', 1, '2018-08-16', 1534452621, 0),
(235, 60, 246, 85, '', '10:00 PM', 1, 'scheduled', 1, '2018-08-19', 1534830213, 0),
(236, 60, 67, 91, '', '11:30 PM', 2, 'scheduled', 1, '2018-08-21', 1534854013, 0),
(237, 60, 67, 8, '', '2:45 AM', 1, 'current', 1, '2018-08-23', 1534854258, 0),
(238, 60, 67, 14, '', '4:15 AM', 1, 'scheduled', 1, '2018-09-20', 1534854311, 0),
(239, 60, 257, 58, '', '3:15 PM', 1, 'attended', 1, '2018-08-22', 1534950157, 0),
(240, 60, 224, 69, '', '6:00 PM', 2, 'attended', 3, '2018-08-22', 1534950198, 0),
(241, 60, 256, 68, '', '5:45 PM', 3, 'current', 1, '2018-08-22', 1534950296, 0),
(242, 60, 342, 73, '', '7:00 PM', 4, 'scheduled', 4, '2018-08-22', 1534950350, 0),
(243, 60, 247, 76, '', '7:45 PM', 5, 'scheduled', 2, '2018-08-22', 1534951687, 0),
(244, 60, 320, 86, '', '10:15 PM', 6, 'scheduled', 4, '2018-08-22', 1534951795, 0),
(247, 60, 230, 84, '', '9:45 PM', 7, 'scheduled', 1, '2018-08-22', 1534973625, 0),
(248, 60, 230, 85, '', '10:00 PM', 2, 'scheduled', 3, '2018-08-23', 1535031235, 0),
(249, 60, 320, 0, '', '', 0, 'scheduled', 1, '2018-08-24', 1535114950, 1535141025),
(250, 60, 230, 78, '', '8:15 PM', 2, 'scheduled', 1, '2018-08-24', 1535144885, 0),
(251, 60, 224, 82, '', '9:15 PM', 1, 'attended', 1, '2018-08-27', 1535415642, 0),
(252, 60, 256, 85, '', '10:00 PM', 2, 'current', 1, '2018-08-27', 1535415771, 0),
(253, 60, 257, 91, '', '11:30 PM', 3, 'scheduled', 2, '2018-08-27', 1535415797, 0),
(255, 60, 230, 84, '', '9:45 PM', 4, 'scheduled', 1, '2018-08-27', 1535417730, 0),
(263, 60, 342, 65, '', '5:00 PM', 1, 'attended', 1, '2018-08-28', 1535486889, 0),
(264, 60, 256, 66, '', '5:15 PM', 2, 'current', 1, '2018-08-28', 1535486909, 0),
(265, 60, 257, 67, '', '5:30 PM', 3, 'scheduled', 3, '2018-08-28', 1535486929, 0),
(266, 60, 320, 20, '', '5:45 AM', 4, 'scheduled', 1, '2018-08-28', 1535486945, 0),
(267, 60, 224, 69, '', '6:00 PM', 5, 'scheduled', 2, '2018-08-28', 1535487064, 0),
(268, 60, 230, 78, '', '8:15 PM', 6, 'scheduled', 1, '2018-08-28', 1535492575, 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `app_name` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT 'AndyRDH',
  `status` enum('Active','inActive') COLLATE latin1_general_ci NOT NULL DEFAULT 'inActive',
  `create_dt` int(11) NOT NULL,
  `update_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `parent_id`, `app_name`, `status`, `create_dt`, `update_dt`) VALUES
(1, 'Addiction psychiatrist', 0, 'doctorApp', 'Active', 0, 0),
(2, 'Allergist', 0, 'doctorApp', 'Active', 0, 0),
(3, 'Orthopaedics', 0, 'doctorApp', 'Active', 0, 0),
(4, 'Surgery', 0, 'doctorApp', 'Active', 0, 0),
(5, 'Orthopaedics', 1, 'doctorApp', 'Active', 0, 0),
(6, 'Surgery', 1, 'doctorApp', 'Active', 0, 0),
(7, 'Surgery', 1, 'doctorApp', 'Active', 0, 0),
(8, 'Surgery', 1, 'doctorApp', 'Active', 0, 0),
(9, 'Surgery', 2, 'doctorApp', 'Active', 0, 0),
(10, 'Surgery', 2, 'doctorApp', 'Active', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `chat_user_friend`
--

CREATE TABLE `chat_user_friend` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `friend_code` int(11) NOT NULL,
  `create_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_user_friend`
--

INSERT INTO `chat_user_friend` (`id`, `user_id`, `friend_id`, `friend_code`, `create_dt`) VALUES
(1, 60, 246, 4199, 1520405258),
(2, 60, 237, 7989, 1520577138),
(3, 60, 67, 8627, 1521092801),
(4, 60, 189, 4012, 1521004377),
(5, 60, 86, 7599, 0),
(6, 60, 258, 1269, 1521004485),
(7, 60, 256, 9006, 1521074104),
(8, 60, 257, 6112, 0),
(9, 60, 224, 6153, 1520951168),
(10, 60, 230, 8117, 1521117725),
(11, 60, 291, 7973, 0),
(12, 60, 261, 8850, 0),
(13, 60, 264, 3861, 0),
(14, 60, 292, 3948, 0),
(15, 60, 72, 4911, 1520948635),
(16, 60, 247, 4213, 1521141285),
(17, 57, 246, 4365, 0),
(18, 60, 299, 2826, 1521529104),
(19, 60, 302, 6007, 1521643849),
(20, 60, 58, 3490, 0),
(21, 60, 59, 4042, 0),
(22, 60, 61, 7295, 0),
(23, 316, 256, 8198, 0),
(24, 316, 257, 4565, 0),
(25, 316, 224, 7231, 0),
(26, 316, 302, 1551, 0),
(27, 316, 230, 6342, 1525367665),
(28, 316, 319, 6578, 0),
(29, 316, 320, 3597, 1525374087),
(30, 60, 323, 6385, 1526306905),
(31, 60, 324, 2732, 1526307961),
(32, 60, 331, 7436, 1528244716),
(33, 60, 335, 5550, 1529449286),
(34, 60, 336, 9551, 0),
(35, 60, 320, 6534, 1532612926),
(36, 57, 67, 9271, 0),
(37, 57, 184, 8538, 0),
(38, 60, 342, 4059, 0),
(39, 60, 341, 4944, 0),
(40, 60, 343, 8994, 1533055815),
(41, 60, 344, 3020, 1534157493),
(42, 57, 59, 6597, 0),
(43, 1, 342, 2489, 0),
(44, 1, 320, 9421, 1535486687),
(45, 1, 256, 6900, 0),
(46, 1, 257, 5341, 0),
(47, 1, 224, 5592, 0),
(48, 1, 230, 8046, 1535486718);

-- --------------------------------------------------------

--
-- Table structure for table `chat_user_message`
--

CREATE TABLE `chat_user_message` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `room_id` int(11) NOT NULL,
  `create_dt` int(11) NOT NULL,
  `chat_status` int(11) NOT NULL COMMENT '0- send, 1- received, 2- read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat_user_message_status`
--

CREATE TABLE `chat_user_message_status` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `chat_status` int(11) NOT NULL COMMENT '0- send, 1- received, 2- read',
  `st` text NOT NULL,
  `msg` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_user_message_status`
--

INSERT INTO `chat_user_message_status` (`id`, `room_id`, `user_id`, `chat_status`, `st`, `msg`) VALUES
(1, 1, 60, 0, '1--60', ''),
(2, 1, 246, 0, '1--246', ''),
(3, 2, 60, 0, '2--60', ''),
(4, 2, 237, 4, '2--237', ''),
(5, 3, 60, 0, '3--60', ''),
(6, 3, 67, 2, '3--67', ''),
(7, 4, 60, 0, '', ''),
(8, 4, 189, 1, '4--189', ''),
(9, 5, 60, 0, '', ''),
(10, 5, 86, 1, '5--86', ''),
(11, 6, 60, 0, '', ''),
(12, 6, 258, 0, '', ''),
(13, 7, 60, 0, '', ''),
(14, 7, 256, 0, '7--256', ''),
(15, 8, 60, 0, '', ''),
(16, 8, 257, 0, '', ''),
(17, 9, 60, 1, '9--60', 'Que tal'),
(18, 9, 224, 0, '9--224', 'hola'),
(19, 10, 60, 0, '10--60', ''),
(20, 10, 230, 0, '10--230', 'hola'),
(21, 11, 60, 0, '', ''),
(22, 11, 291, 0, '', ''),
(23, 12, 60, 0, '', ''),
(24, 12, 261, 0, '', ''),
(25, 13, 60, 0, '', ''),
(26, 13, 264, 0, '', ''),
(27, 14, 60, 0, '', ''),
(28, 14, 292, 0, '', ''),
(29, 15, 60, 0, '15--60', ''),
(30, 15, 72, 1, '15--72', ''),
(31, 16, 60, 0, '16--60', ''),
(32, 16, 247, 0, '16--247', 'estas?'),
(33, 17, 57, 0, '', ''),
(34, 17, 246, 0, '17--246', 'hi'),
(35, 18, 60, 0, '', ''),
(36, 18, 299, 0, '', ''),
(37, 19, 60, 0, '', ''),
(38, 19, 302, 0, '', ''),
(39, 20, 60, 0, '', ''),
(40, 20, 58, 0, '', ''),
(41, 21, 60, 0, '', ''),
(42, 21, 59, 0, '21--59', 'bhanu'),
(43, 22, 60, 0, '', ''),
(44, 22, 61, 0, '', ''),
(45, 23, 316, 0, '', ''),
(46, 23, 256, 0, '', ''),
(47, 24, 316, 0, '', ''),
(48, 24, 257, 0, '', ''),
(49, 25, 316, 0, '', ''),
(50, 25, 224, 0, '', ''),
(51, 26, 316, 0, '', ''),
(52, 26, 302, 0, '', ''),
(53, 27, 316, 0, '', ''),
(54, 27, 230, 0, '', ''),
(55, 28, 316, 0, '', ''),
(56, 28, 319, 0, '', ''),
(57, 29, 316, 0, '', ''),
(58, 29, 320, 0, '', ''),
(59, 30, 60, 0, '', ''),
(60, 30, 323, 0, '', ''),
(61, 31, 60, 0, '', ''),
(62, 31, 324, 0, '', ''),
(63, 32, 60, 0, '', ''),
(64, 32, 331, 0, '', ''),
(65, 33, 60, 0, '', ''),
(66, 33, 335, 0, '', ''),
(67, 34, 60, 0, '', ''),
(68, 34, 336, 0, '', ''),
(69, 35, 60, 0, '35--60', 'hola!! '),
(70, 35, 320, 0, '35--320', 'hola hola'),
(71, 36, 57, 0, '', ''),
(72, 36, 67, 0, '36--67', 'pramod'),
(73, 37, 57, 0, '', ''),
(74, 37, 184, 0, '37--184', 'suraj'),
(75, 38, 60, 0, '', ''),
(76, 38, 342, 0, '', ''),
(77, 39, 60, 0, '', ''),
(78, 39, 341, 0, '', ''),
(79, 40, 60, 0, '', ''),
(80, 40, 343, 0, '', ''),
(81, 41, 60, 0, '', ''),
(82, 41, 344, 0, '', ''),
(83, 42, 57, 0, '42--57', 'x bx'),
(84, 42, 59, 0, '42--59', 'Hi,, How Are you..'),
(85, 43, 1, 0, '', ''),
(86, 43, 342, 0, '', ''),
(87, 44, 1, 0, '', ''),
(88, 44, 320, 0, '', ''),
(89, 45, 1, 0, '', ''),
(90, 45, 256, 0, '', ''),
(91, 46, 1, 0, '', ''),
(92, 46, 257, 0, '', ''),
(93, 47, 1, 0, '', ''),
(94, 47, 224, 0, '', ''),
(95, 48, 1, 0, '', ''),
(96, 48, 230, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `clinic_doctor_management`
--

CREATE TABLE `clinic_doctor_management` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `doctor_status` enum('Available','Unavailable') NOT NULL DEFAULT 'Available',
  `doctor_name` varchar(60) NOT NULL,
  `doctor_phone_no` varchar(20) NOT NULL,
  `doctor_email_id` varchar(60) NOT NULL,
  `clinic_name` varchar(250) NOT NULL,
  `clinic_address` text NOT NULL,
  `lat` decimal(10,4) NOT NULL,
  `long` decimal(10,4) NOT NULL,
  `doctor_about_us` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clinic_doctor_management`
--

INSERT INTO `clinic_doctor_management` (`id`, `user_id`, `doctor_status`, `doctor_name`, `doctor_phone_no`, `doctor_email_id`, `clinic_name`, `clinic_address`, `lat`, `long`, `doctor_about_us`) VALUES
(1, 57, 'Unavailable', 'Dr Subodh Jain', '98989898981', 'rajesh@gmail.com1', 'synergy hospital indore', 'Las Vegas, NV, USA', '36.1699', '-115.1398', 'loren lipson1'),
(2, 60, 'Available', 'Dra. Linda Vázquez', '0731504051', 'doctor@gmail.com', 'Homs Santiago', 'Av. Presidente Antonio Guzmán Fernández, Universidad ISA, Santiago De Los Caballeros, Dominican Republic', '19.4408', '-70.7277', 'Especialista en reconstrucción de Cadera y Cintura'),
(3, 235, 'Available', 'Dr Sumit Jain', '0731 456456', 'sumit@gmail.com', 'apollo hospital indore', ' Scheme No. 74 C, Sector D, Vijay Nagar, Indore, Madhya Pradesh 452010', '0.0000', '0.0000', 'The Apollo Hospitals, Indore is a joint venture between Apollo Hospitals Enterprise Limited (AHEL) & Rajshree Hospital & Research Centre Pvt. Ltd. (RHRL). It offers high-end tertiary care across various medical disciplines, with special focus on Cardiology & Cardiothoracic surgery, Neurology and Neurosurgery, Nephrology and Urology, Orthopaedics, Gastroenterology, Emergency & Trauma.'),
(4, 241, 'Available', 'Dr Suraj Palsania', '454556456123123132', 'palsaniasuraj@abc.com', 'Medanta', '', '0.0000', '0.0000', 'c;bvcxv,l;'),
(5, 316, 'Available', 'Silma Peralta', '', 'Silmap11', 'Homs', 'Autopista Duarte 28, Santiago De Los Caballeros 51000, Dominican Republic', '0.0000', '0.0000', ''),
(6, 325, 'Available', 'Dra. Maria Diaz Arias', '8311', 'Consultorio311', 'Homs', 'Autopista Duarte 28, Santiago De Los Caballeros 51000, Dominican Republic', '0.0000', '0.0000', ''),
(7, 334, 'Available', 'Pedro Estevz', '', 'pestevez@gmail.com', 'HOMS', 'Autopista Duarte 28, Santiago De Los Caballeros 51000, República Dominicana', '19.4362', '-70.6611', 'El Dr. Pedro Estevez es un médico ortopedista y traumatólogo interesado y formado en el área de cirugía reconstructiva articular de Cadera y Rodilla. Realizó sus estudios en Buenos Aires Argentina, por espacio de 7 años.');

-- --------------------------------------------------------

--
-- Table structure for table `clinic_doctor_speciality`
--

CREATE TABLE `clinic_doctor_speciality` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clinic_doctor_time_management`
--

CREATE TABLE `clinic_doctor_time_management` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `time_slote_mon_morning` varchar(200) NOT NULL,
  `time_slote_mon_evening` varchar(200) NOT NULL,
  `time_slote_tue_morning` varchar(200) NOT NULL,
  `time_slote_tue_evening` varchar(200) NOT NULL,
  `time_slote_wed_morning` varchar(200) NOT NULL,
  `time_slote_wed_evening` varchar(200) NOT NULL,
  `time_slote_thu_morning` varchar(200) NOT NULL,
  `time_slote_thu_evening` varchar(200) NOT NULL,
  `time_slote_fri_morning` varchar(200) NOT NULL,
  `time_slote_fri_evening` varchar(200) NOT NULL,
  `time_slote_sat_morning` varchar(200) NOT NULL,
  `time_slote_sat_evening` varchar(200) NOT NULL,
  `time_slote_sun_morning` varchar(200) NOT NULL,
  `time_slote_sun_evening` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clinic_satff_management`
--

CREATE TABLE `clinic_satff_management` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `staff_name` varchar(60) NOT NULL,
  `staff_phone_no` varchar(20) NOT NULL,
  `gender` varchar(60) NOT NULL,
  `age` varchar(10) NOT NULL,
  `experience` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `consultation_association`
--

CREATE TABLE `consultation_association` (
  `associat_id` int(11) NOT NULL,
  `clinic_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `appointment_id` int(11) NOT NULL,
  `user_info_id` int(11) NOT NULL,
  `medicine_name` text NOT NULL,
  `background` text NOT NULL,
  `create_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultation_association`
--

INSERT INTO `consultation_association` (`associat_id`, `clinic_id`, `patient_id`, `appointment_id`, `user_info_id`, `medicine_name`, `background`, `create_dt`) VALUES
(1, 57, 246, 206, 1, 'medicine', 'background', 1532685582),
(2, 57, 67, 207, 3, 'fdfddfdffg', 'fdfffffdffd', 1532688526),
(3, 60, 320, 224, 4, 'Ninguno', 'Ninguno', 1533911555),
(4, 60, 246, 235, 5, 'current medicinse', 'background', 1534830346),
(5, 60, 320, 249, 4, 'Ninguno', 'Ninguno', 1535115321);

-- --------------------------------------------------------

--
-- Table structure for table `consultation_images`
--

CREATE TABLE `consultation_images` (
  `image_id` int(11) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `associat_id` int(11) NOT NULL,
  `consult_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultation_images`
--

INSERT INTO `consultation_images` (`image_id`, `image_name`, `associat_id`, `consult_id`) VALUES
(1, '1532685582698247.png', 1, 1),
(2, '153268852610097.', 2, 2),
(3, '1533911555319289.jpg', 3, 3),
(4, '1534830346789705.', 4, 4),
(5, '1535115321691350.', 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `consultation_info`
--

CREATE TABLE `consultation_info` (
  `consult_id` int(11) NOT NULL,
  `associat_id` int(11) NOT NULL,
  `consult_reason` text NOT NULL,
  `consult_analysis` text NOT NULL,
  `consult_diagnosis` text NOT NULL,
  `consult_treatment` text NOT NULL,
  `consult_status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `consult_create_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultation_info`
--

INSERT INTO `consultation_info` (`consult_id`, `associat_id`, `consult_reason`, `consult_analysis`, `consult_diagnosis`, `consult_treatment`, `consult_status`, `consult_create_dt`) VALUES
(1, 1, 'reason', 'analisis', 'diago', 'treat', 'active', 1532685582),
(2, 2, 'cxvffd', 'fdfdf', 'dsdsf', 'sdfdsfsdsdfsfdsdfdsf', 'active', 1532688526),
(3, 3, 'Dolor de estomago', 'Muestra de sangre', '', '', 'active', 1533911555),
(4, 4, 'Reason for consultation', 'analyist', '', '', 'active', 1534830346),
(5, 5, 'cefalea migrañosa', '', 'Migraña', 'inyeccion para el dolor de cabeza. Reposo', 'active', 1535115321);

-- --------------------------------------------------------

--
-- Table structure for table `consultation_user_info`
--

CREATE TABLE `consultation_user_info` (
  `user_info_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `clinic_id` int(11) NOT NULL,
  `info_name` varchar(100) NOT NULL,
  `info_age` int(11) NOT NULL,
  `info_blood_type` varchar(10) NOT NULL,
  `info_address` varchar(100) NOT NULL,
  `info_contact` varchar(15) NOT NULL,
  `info_dob` int(11) NOT NULL,
  `info_occuption` varchar(100) NOT NULL,
  `info_insurance_name` int(20) NOT NULL,
  `info_unique_id` varchar(50) NOT NULL,
  `info_status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `info_create_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultation_user_info`
--

INSERT INTO `consultation_user_info` (`user_info_id`, `patient_id`, `clinic_id`, `info_name`, `info_age`, `info_blood_type`, `info_address`, `info_contact`, `info_dob`, `info_occuption`, `info_insurance_name`, `info_unique_id`, `info_status`, `info_create_dt`) VALUES
(1, 246, 57, 'Bhanu', 23, 'A-', 'newyork', '8989642549', 931320000, 'dev', 0, '123nation', 'active', 1532664000),
(3, 67, 57, 'Mr Pramod Jain', 22, 'A-', 'indore', '9987654321', 1530590400, 'BSc.', 0, 'jfddffdfggf', 'active', 1532664000),
(4, 320, 60, 'Carolina Gomez Herrera', 23, 'O+', 'Santiago RD', '8296391202', 783662400, 'Ing. Telematica', 1, '40223559754', 'active', 1535083200),
(5, 246, 60, 'Bhanu', 25, 'AB+', 'indore', '8989642549', 744177600, 'devp', 23, 'IND12', 'active', 1534824000);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_specialties`
--

CREATE TABLE `doctor_specialties` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `specialties_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_specialties`
--

INSERT INTO `doctor_specialties` (`id`, `user_id`, `specialties_id`) VALUES
(7, 57, 2),
(9, 60, 5);

-- --------------------------------------------------------

--
-- Table structure for table `insurance_company`
--

CREATE TABLE `insurance_company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_status` enum('active','inactive') NOT NULL,
  `company_created_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance_company`
--

INSERT INTO `insurance_company` (`company_id`, `company_name`, `company_status`, `company_created_dt`) VALUES
(1, 'Primera ARS de Humano', 'active', 1534750660),
(2, 'ARS Palic Salud', 'active', 1534750660),
(3, 'ARS SeNaSa', 'active', 1534750660),
(4, 'ARS Universal', 'active', 1534750660),
(5, 'ARS Futuro', 'active', 1534750660),
(6, 'ARS Constitución', 'active', 1534750660),
(7, 'ARS Semma', 'active', 1534750660),
(8, 'ARS Salud Segura', 'active', 1534750660),
(9, 'ARS Monumental', 'active', 1534750660),
(10, 'ARS Reservas', 'active', 1534750660),
(11, 'ARS MetaSalud', 'active', 1534750660),
(12, 'Ars Simag', 'active', 1534750660),
(13, 'ARS GMA', 'active', 1534750660),
(14, 'ARS Plan Salud Banco Central', 'active', 1534750660),
(15, 'ARS Renacer', 'active', 1534750660),
(16, 'Medicard', 'active', 1534750660),
(17, 'ARS Yunén', 'active', 1534750660),
(18, 'ARS Asemap', 'active', 1534750660),
(19, 'Salud Dominicana', 'active', 1534750660),
(20, 'APS Asmar Planes de Salud', 'active', 1534750660),
(21, 'Serena Salud', 'active', 1534750660),
(22, 'ARS-CMD, Colegio Médico Dominicano', 'active', 1534750660),
(23, 'BMI Companies', 'active', 1534750660),
(24, 'Telemed', 'active', 1534750660),
(25, 'Pladent', 'active', 1534750660),
(26, 'Bupa Dominicana, S.A.', 'active', 1534750660),
(27, 'WorldWide Seguros', 'active', 1534750660);

-- --------------------------------------------------------

--
-- Table structure for table `manage_appointment_status`
--

CREATE TABLE `manage_appointment_status` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_appointment_status`
--

INSERT INTO `manage_appointment_status` (`id`, `name`, `status`) VALUES
(1, 'Booked', 'Active'),
(2, 'Approval Panding', 'Active'),
(3, 'Confirmed', 'Active'),
(4, 'Attended', 'Active'),
(5, 'Cancel', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `manage_doctor_specialties`
--

CREATE TABLE `manage_doctor_specialties` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_doctor_specialties`
--

INSERT INTO `manage_doctor_specialties` (`id`, `name`, `status`) VALUES
(1, 'Default Dr Citas Specialities', 'Active'),
(2, 'Dermatologa', 'Active'),
(3, 'Anesthesiology', 'Active'),
(4, 'Ortopeda', 'Active'),
(5, 'Pediatra', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `manage_subscription_plan`
--

CREATE TABLE `manage_subscription_plan` (
  `id` int(11) NOT NULL,
  `plan_name` varchar(200) NOT NULL,
  `plan_details` text NOT NULL,
  `amount` varchar(20) NOT NULL,
  `plan_duration` varchar(20) NOT NULL,
  `plan_days` int(11) NOT NULL,
  `status` enum('Active','inActive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_subscription_plan`
--

INSERT INTO `manage_subscription_plan` (`id`, `plan_name`, `plan_details`, `amount`, `plan_duration`, `plan_days`, `status`) VALUES
(1, 'started', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '$20', '10 Days', 10, 'Active'),
(2, 'Team', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '$50', '1 Month', 30, 'Active'),
(3, 'Premium', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '200', '90', 0, 'Active'),
(4, 'Test subscribe', 'Promotion offer', '1000', '30', 0, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `manage_treatment_type`
--

CREATE TABLE `manage_treatment_type` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_treatment_type`
--

INSERT INTO `manage_treatment_type` (`id`, `name`) VALUES
(1, 'Acuphase'),
(2, 'Antidepressants'),
(3, 'Antipsychotics'),
(4, 'Art Therapy'),
(5, 'Atypical Antipsychotics'),
(6, 'Behaviour therapy'),
(7, 'Benzodiazepines'),
(8, 'Carbamazepine'),
(9, 'Care programme approach'),
(10, 'Crisis intervention team'),
(11, 'Counselling'),
(12, 'Depot medication');

-- --------------------------------------------------------

--
-- Table structure for table `manage_visit_status`
--

CREATE TABLE `manage_visit_status` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_visit_status`
--

INSERT INTO `manage_visit_status` (`id`, `name`, `status`) VALUES
(1, 'Consultas', 'Active'),
(2, 'Visitador', 'Active'),
(3, 'Resultados', 'Active'),
(4, 'Procedimiento', 'Active'),
(5, 'Cancel', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `appointment_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `create_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `user_id`, `appointment_id`, `description`, `create_dt`) VALUES
(21, 224, 3, 'Hemos Reservado Su Cita', 1512998457),
(22, 224, 4, 'Hemos Reservado Su Cita', 1513000116),
(30, 72, 5, 'Hemos Reservado Su Cita', 1513775482),
(32, 224, 7, 'Hemos Reservado Su Cita', 1513777047),
(33, 224, 8, 'Hemos Reservado Su Cita', 1513777077),
(36, 247, 11, 'Hemos Reservado Su Cita', 1513806495),
(41, 249, 16, 'Hemos Reservado Su Cita', 1513857103),
(45, 224, 20, 'Hemos Reservado Su Cita', 1514144930),
(78, 224, 53, 'Hemos Reservado Su Cita', 1515262055),
(80, 256, 55, 'Hemos Reservado Su Cita', 1515262203),
(81, 257, 56, 'Hemos Reservado Su Cita', 1515262281),
(84, 256, 59, 'Hemos Reservado Su Cita', 1515456690),
(97, 224, 72, 'Hemos Reservado Su Cita', 1516025921),
(98, 247, 73, 'Hemos Reservado Su Cita', 1516043507),
(102, 117, 77, 'Hemos Reservado Su Cita', 1516108882),
(111, 189, 86, 'Your appointment has been booked', 1516183346),
(113, 258, 88, 'Your appointment has been booked', 1516191988),
(115, 247, 90, 'Hemos Reservado Su Cita', 1516231007),
(123, 258, 98, 'Your appointment has been booked', 1516367708),
(125, 247, 100, 'Hemos Reservado Su Cita', 1516392944),
(133, 224, 108, 'Hemos Reservado Su Cita', 1516710918),
(134, 247, 109, 'Hemos Reservado Su Cita', 1516756326),
(135, 224, 110, 'Hemos Reservado Su Cita', 1516756423),
(141, 258, 116, 'Your appointment has been booked', 1516862957),
(143, 258, 118, 'Your appointment has been booked', 1516874148),
(147, 258, 122, 'Hemos Reservado Su Cita', 1517376578),
(150, 86, 125, 'Hemos Reservado Su Cita', 1517376706),
(155, 258, 130, 'Your appointment has been booked', 1517576744),
(158, 189, 133, 'Your appointment has been booked', 1517576819),
(161, 263, 136, 'Hemos Reservado Su Cita', 1518805713),
(163, 224, 138, 'Hemos Reservado Su Cita', 1518987048),
(164, 224, 139, 'Hemos Reservado Su Cita', 1518987978),
(165, 256, 140, 'Hemos Reservado Su Cita', 1518989377),
(166, 224, 141, 'Hemos Reservado Su Cita', 1518989401),
(171, 189, 146, 'Your appointment has been booked', 1519040657),
(177, 189, 152, 'Your appointment has been booked', 1519105664),
(181, 258, 156, 'Hemos Reservado Su Cita', 1519130507),
(182, 224, 157, 'Hemos Reservado Su Cita', 1519131085),
(183, 224, 158, 'Hemos Reservado Su Cita', 1519158755),
(184, 224, 159, 'Hemos Reservado Su Cita', 1519161792),
(194, 189, 169, 'Hemos Reservado Su Cita', 1519295183),
(197, 266, 172, 'Hemos Reservado Su Cita', 1519307014),
(198, 224, 173, 'Hemos Reservado Su Cita', 1519410284),
(200, 189, 175, 'Your appointment has been booked', 1519642687),
(201, 86, 176, 'Your appointment has been booked', 1519642706),
(203, 224, 178, 'Hemos Reservado Su Cita', 1520188447),
(205, 256, 180, 'Hemos Reservado Su Cita', 1520213568),
(207, 224, 182, 'Your appointment has been booked', 1520235193),
(208, 256, 183, 'Your appointment has been booked', 1520235246),
(209, 189, 184, 'Your appointment has been booked', 1520255355),
(216, 224, 191, 'Hemos Reservado Su Cita', 1520339512),
(240, 198, 23, 'Your appointment has been booked', 1520578382),
(256, 67, 39, 'Your appointment has been booked', 1520601010),
(257, 258, 40, 'Your appointment has been booked', 1520601025),
(258, 189, 41, 'Your appointment has been booked', 1520601050),
(263, 189, 46, 'Your appointment has been booked', 1520665638),
(267, 67, 50, 'Your appointment has been booked', 1520831322),
(268, 258, 51, 'Your appointment has been booked', 1520831337),
(269, 189, 52, 'Your appointment has been booked', 1520831373),
(270, 86, 53, 'Your appointment has been booked', 1520831390),
(277, 258, 60, 'Your appointment has been booked', 1520853700),
(278, 189, 61, 'Your appointment has been booked', 1520853720),
(279, 256, 62, 'Your appointment has been booked', 1520853751),
(280, 257, 63, 'Your appointment has been booked', 1520853780),
(281, 224, 64, 'Hemos Reservado Su Cita', 1520854468),
(283, 291, 66, 'Your appointment has been booked', 1520855461),
(284, 261, 67, 'Your appointment has been booked', 1520855631),
(285, 264, 68, 'Your appointment has been booked', 1520855886),
(289, 258, 72, 'Your appointment has been booked', 1520861644),
(291, 224, 74, 'Hemos Reservado Su Cita', 1520940952),
(292, 292, 75, 'Your appointment has been booked', 1520946985),
(295, 72, 78, 'Your appointment has been booked', 1520948542),
(298, 258, 81, 'Your appointment has been booked', 1521004134),
(299, 189, 82, 'Your appointment has been booked', 1521004159),
(300, 86, 83, 'Your appointment has been booked', 1521004176),
(301, 224, 84, 'Your appointment has been booked', 1521031852),
(302, 247, 85, 'Your appointment has been booked', 1521031936),
(304, 256, 87, 'Your appointment has been booked', 1521031973),
(306, 67, 89, 'Your appointment has been booked', 1521090780),
(307, 258, 90, 'Your appointment has been booked', 1521090794),
(308, 86, 91, 'Your appointment has been booked', 1521095354),
(310, 256, 93, 'Your appointment has been booked', 1521119144),
(311, 257, 94, 'Your appointment has been booked', 1521119173),
(312, 247, 95, 'Your appointment has been booked', 1521119229),
(313, 224, 96, 'Your appointment has been booked', 1521119313),
(314, 72, 97, 'Hemos Reservado Su Cita', 1521176608),
(315, 224, 98, 'Your appointment has been booked', 1521177258),
(324, 246, 107, 'Your appointment has been booked', 1521204247),
(343, 246, 123, 'Your appointment has been booked', 1521264149),
(344, 246, 124, 'Your appointment has been booked', 1521264907),
(346, 246, 126, 'Your appointment has been booked', 1521265297),
(359, 258, 133, 'Hemos Reservado Su Cita', 1521290323),
(360, 189, 134, 'Hemos Reservado Su Cita', 1521290938),
(361, 247, 135, 'Hemos Reservado Su Cita', 1521296279),
(362, 246, 136, 'Your appointment has been booked', 1521528064),
(363, 67, 137, 'Your appointment has been booked', 1521528084),
(364, 258, 138, 'Your appointment has been booked', 1521528099),
(365, 189, 139, 'Your appointment has been booked', 1521528114),
(366, 86, 140, 'Your appointment has been booked', 1521528130),
(367, 299, 141, 'Hemos Reservado Su Cita', 1521528723),
(368, 302, 142, 'Hemos Reservado Su Cita', 1521642574),
(369, 247, 143, 'Hemos Reservado Su Cita', 1521642717),
(370, 256, 144, 'Hemos Reservado Su Cita', 1521642857),
(371, 224, 145, 'Hemos Reservado Su Cita', 1521809690),
(372, 224, 146, 'Hemos Reservado Su Cita', 1523421337),
(373, 302, 147, 'Hemos Reservado Su Cita', 1523986469),
(374, 224, 148, 'Hemos Reservado Su Cita', 1523986545),
(376, 67, 150, 'Your appointment has been booked', 1524205315),
(377, 67, 151, 'Your appointment has been booked', 1524468551),
(378, 67, 151, 'Your appointment has been booked', 1524468964),
(379, 58, 152, 'Your appointment has been booked', 1524469218),
(380, 59, 153, 'Your appointment has been booked', 1524469253),
(381, 61, 154, 'Your appointment has been booked', 1524469450),
(382, 67, 155, 'Your appointment has been booked', 1524469500),
(384, 224, 157, 'Hemos Reservado Su Cita', 1525052693),
(385, 67, 158, 'Your appointment has been booked', 1525355668),
(386, 256, 159, 'Hemos Reservado Su Cita', 1525367484),
(387, 257, 160, 'Hemos Reservado Su Cita', 1525367517),
(388, 224, 161, 'Hemos Reservado Su Cita', 1525367550),
(389, 302, 162, 'Hemos Reservado Su Cita', 1525367595),
(391, 319, 164, 'Hemos Reservado Su Cita', 1525370940),
(396, 224, 169, 'Hemos Reservado Su Cita', 1526043489),
(398, 224, 171, 'Hemos Reservado Su Cita', 1526306138),
(399, 256, 172, 'Hemos Reservado Su Cita', 1526306237),
(400, 323, 173, 'Hemos Reservado Su Cita', 1526306879),
(401, 324, 174, 'Hemos Reservado Su Cita', 1526307916),
(408, 224, 181, 'Hemos Reservado Su Cita', 1529352970),
(412, 335, 185, 'Hemos Reservado Su Cita', 1529449263),
(417, 336, 190, 'Hemos Reservado Su Cita', 1530048590),
(418, 331, 191, 'Hemos Reservado Su Cita', 1530048856),
(419, 331, 192, 'Hemos Reservado Su Cita', 1530058687),
(420, 331, 193, 'Hemos Reservado Su Cita', 1530060154),
(421, 224, 194, 'Hemos Reservado Su Cita', 1530244123),
(422, 256, 195, 'Hemos Reservado Su Cita', 1530286525),
(423, 257, 196, 'Hemos Reservado Su Cita', 1530286563),
(424, 331, 197, 'Hemos Reservado Su Cita', 1530286774),
(426, 246, 199, 'Your appointment has been booked', 1532608884),
(427, 320, 200, 'Your appointment has been booked', 1532612881),
(429, 320, 202, 'Your appointment has been booked', 1532613861),
(432, 320, 202, 'Hemos Reservado Su Cita', 1532628883),
(433, 224, 204, 'Hemos Reservado Su Cita', 1532628913),
(434, 67, 205, 'Hemos Reservado Su Cita', 1532684448),
(435, 67, 205, 'Hemos Reservado Su Cita', 1532684959),
(436, 246, 206, 'Your appointment has been booked', 1532685338),
(437, 67, 207, 'Your appointment has been booked', 1532688434),
(438, 184, 208, 'Your appointment has been booked', 1532689202),
(439, 230, 209, 'Hemos Reservado Su Cita', 1532716459),
(440, 342, 210, 'Hemos Reservado Su Cita', 1532962520),
(441, 341, 211, 'Hemos Reservado Su Cita', 1532963783),
(442, 230, 212, 'Hemos Reservado Su Cita', 1532963804),
(443, 224, 213, 'Hemos Reservado Su Cita', 1532984001),
(444, 320, 214, 'Hemos Reservado Su Cita', 1532984041),
(445, 257, 215, 'Hemos Reservado Su Cita', 1532984072),
(446, 342, 216, 'Hemos Reservado Su Cita', 1533054915),
(447, 224, 217, 'Hemos Reservado Su Cita', 1533054940),
(448, 320, 218, 'Hemos Reservado Su Cita', 1533055002),
(449, 343, 219, 'Hemos Reservado Su Cita', 1533055645),
(450, 343, 219, 'Hemos Reservado Su Cita', 1533055720),
(451, 230, 220, 'Hemos Reservado Su Cita', 1533062866),
(452, 224, 221, 'Hemos Reservado Su Cita', 1533170681),
(453, 230, 222, 'Hemos Reservado Su Cita', 1533170723),
(454, 342, 223, 'Hemos Reservado Su Cita', 1533170790),
(455, 320, 224, 'Your appointment has been booked', 1533239127),
(456, 320, 224, 'Your appointment has been booked', 1533239283),
(457, 320, 224, 'Hemos Reservado Su Cita', 1533905199),
(458, 343, 225, 'Hemos Reservado Su Cita', 1533906211),
(459, 230, 226, 'Your appointment has been booked', 1533908856),
(460, 320, 224, 'Your appointment has been booked', 1533908886),
(461, 344, 227, 'Hemos Reservado Su Cita', 1533909916),
(462, 246, 228, 'Your appointment has been booked', 1533912579),
(463, 59, 229, 'Your appointment has been booked', 1534157584),
(464, 230, 230, 'Hemos Reservado Su Cita', 1534161326),
(465, 230, 231, 'Hemos Reservado Su Cita', 1534452419),
(466, 342, 232, 'Hemos Reservado Su Cita', 1534452531),
(467, 341, 233, 'Hemos Reservado Su Cita', 1534452598),
(468, 256, 234, 'Hemos Reservado Su Cita', 1534452621),
(469, 246, 235, 'Your appointment has been booked', 1534830213),
(470, 67, 236, 'Your appointment has been booked', 1534854013),
(471, 67, 237, 'Your appointment has been booked', 1534854258),
(472, 67, 238, 'Your appointment has been booked', 1534854311),
(473, 257, 239, 'Hemos Reservado Su Cita', 1534950157),
(474, 224, 240, 'Hemos Reservado Su Cita', 1534950198),
(475, 256, 241, 'Hemos Reservado Su Cita', 1534950296),
(476, 342, 242, 'Hemos Reservado Su Cita', 1534950350),
(477, 247, 243, 'Hemos Reservado Su Cita', 1534951687),
(478, 320, 244, 'Hemos Reservado Su Cita', 1534951795),
(479, 230, 245, 'Hemos Reservado Su Cita', 1534954112),
(480, 230, 246, 'Hemos Reservado Su Cita', 1534971701),
(481, 230, 247, 'Hemos Reservado Su Cita', 1534973625),
(482, 230, 248, 'Hemos Reservado Su Cita', 1535031235),
(483, 320, 249, 'Hemos Reservado Su Cita', 1535114950),
(484, 320, 249, 'Your appointment has been booked', 1535141007),
(485, 320, 249, 'Your appointment has been booked', 1535141025),
(486, 230, 250, 'Hemos Reservado Su Cita', 1535144885),
(487, 224, 251, 'Hemos Reservado Su Cita', 1535415642),
(488, 256, 252, 'Hemos Reservado Su Cita', 1535415771),
(489, 257, 253, 'Hemos Reservado Su Cita', 1535415797),
(490, 230, 254, 'Hemos Reservado Su Cita', 1535415943),
(491, 230, 255, 'Hemos Reservado Su Cita', 1535417730),
(492, 230, 256, 'Hemos Reservado Su Cita', 1535484322),
(493, 342, 257, 'Hemos Reservado Su Cita', 1535486155),
(494, 320, 258, 'Hemos Reservado Su Cita', 1535486186),
(495, 256, 259, 'Hemos Reservado Su Cita', 1535486211),
(496, 257, 260, 'Hemos Reservado Su Cita', 1535486230),
(497, 224, 261, 'Hemos Reservado Su Cita', 1535486274),
(498, 230, 262, 'Hemos Reservado Su Cita', 1535486689),
(499, 342, 263, 'Hemos Reservado Su Cita', 1535486889),
(500, 256, 264, 'Hemos Reservado Su Cita', 1535486909),
(501, 257, 265, 'Hemos Reservado Su Cita', 1535486929),
(502, 320, 266, 'Hemos Reservado Su Cita', 1535486945),
(503, 224, 267, 'Hemos Reservado Su Cita', 1535487064),
(504, 230, 268, 'Hemos Reservado Su Cita', 1535492575);

-- --------------------------------------------------------

--
-- Table structure for table `patient_doctor_relation`
--

CREATE TABLE `patient_doctor_relation` (
  `r_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `status` enum('active','deactive','delete') NOT NULL DEFAULT 'active',
  `create_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_doctor_relation`
--

INSERT INTO `patient_doctor_relation` (`r_id`, `patient_id`, `doctor_id`, `status`, `create_dt`) VALUES
(1, 67, 60, 'active', 1534854258),
(2, 257, 60, 'active', 1534950157),
(3, 224, 60, 'active', 1534950198),
(4, 256, 60, 'active', 1534950296),
(5, 342, 60, 'active', 1534950350),
(6, 247, 60, 'active', 1534951687),
(7, 320, 60, 'active', 1534951795),
(8, 230, 60, 'active', 1534954112),
(9, 342, 1, 'active', 1535486155),
(10, 320, 1, 'active', 1535486186),
(11, 256, 1, 'active', 1535486211),
(12, 257, 1, 'active', 1535486230),
(13, 224, 1, 'active', 1535486274),
(14, 230, 1, 'active', 1535486689);

-- --------------------------------------------------------

--
-- Table structure for table `static_content`
--

CREATE TABLE `static_content` (
  `id` int(11) NOT NULL,
  `content_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `degree` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_img` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `content_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `static_content`
--

INSERT INTO `static_content` (`id`, `content_title`, `title_alias`, `name`, `degree`, `user_img`, `content`, `content_image`, `created`, `updated`, `status`) VALUES
(1, 'About Us', 'about-us', 'Andy codding', 'R.H.D', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-05-24 06:31:56', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `user_role` int(11) NOT NULL DEFAULT '3',
  `full_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `picture_url` text COLLATE utf8_unicode_ci NOT NULL,
  `device_token` text COLLATE utf8_unicode_ci NOT NULL,
  `device_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notifications` int(11) NOT NULL DEFAULT '1',
  `chat_setting` int(11) NOT NULL COMMENT '0 : trun off, 1 on appoint day only, 2: open for any time',
  `clinical_specialist` text COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','inActive','Delete') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `create_dt` int(11) NOT NULL,
  `update_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `parent_id`, `user_role`, `full_name`, `phone_number`, `email_address`, `password`, `designation`, `picture_url`, `device_token`, `device_type`, `notifications`, `chat_setting`, `clinical_specialist`, `activation_code`, `status`, `create_dt`, `update_dt`) VALUES
(1, 0, 1, 'Admin', '8989898989', 'admin@gmail.com', '123456', '', '', '', 'Android', 1, 0, '', '841325893', 'Active', 1505763917, 1507017340),
(57, 0, 2, 'synergy hospital indore', '998146282111', 'synergy@consagous.com', '123456', '', '1510034454.jpg', '', '', 1, 2, 'At The Synergy Hospital in Vijay Nagar, the various modes of payment accepted are Cash, Cheques, . You can reach them at Opposite Meghdoot Garden,Scheme Number 74 C,Sector B,Vijay Nagar-452010. The contact number of this hospital is +(91)-731-2550400. You can view 11 photos of this establishment as well. This establishment has been rated a 4.0 from a total of 300+ ratings. This listing is also listed in Hospitals, Gynaecologist & Obstetrician Doctors, Neurologists. ', '440243', 'Active', 1506412952, 1534858865),
(58, 0, 3, 'Mr Jain', '8889996735', 'pramodjain@consagous.com', '123456', '', '', '', 'Android', 1, 0, '', '271055', 'Active', 1506520369, 1509517583),
(59, 0, 3, 'bhanu', '9632587412', 'bhanu@gmail.com', '123456', '', '1506521796.', 'ceIJzcvr8bI:APA91bGB1XEtVy20xi4Zle6fHGmIuN1d2ycbzQqpy4S6HWcBywxDptSlJIy-6C_BuGEyQj63l6i6dMuW5xvijci3CHXV8AGK1_BUwsgkFl9lgOTk5Mwfng2pB6DgjHI0A3mbL2JTvYdnkt1JANKaBbS1VYwLC5uI7g', 'Android', 1, 0, '', '', 'Active', 1506412952, 1506521796),
(60, 0, 2, 'Homs Santiago', '8092477210', 'indore_clinic@gmail.com', '123456', '', '1507557765.jpg', '', '', 1, 2, 'The Department of Nuclear Medicine offers a comprehensive range of clinical services in radioisotope imaging and therapy. ', '', 'Active', 1506596088, 1535145234),
(61, 0, 3, 'mr jain', '9898989898', '1pramodjain@consagous.com', '123456', '', '', 'fHPRetaXaPs:APA91bHBZnBFWK_cHwnuidUpiHCsrRaP_P2IAPrOxXSbIicwB5uFUVLzDlKh8lE3ZtX_HPROle6rkctCqv0Mu0-TJeA999llIMMHeYdLpM1CKMtprqymJv0pdrABFk44JDUo9X2WfZXa', 'Android', 1, 0, '', '', 'Active', 1506665433, 0),
(62, 60, 4, 'staff', '', 'staff@gmail.com', '123456', 'staff member', '', '', '', 1, 2, '', '', 'Active', 1507028178, 1507028178),
(66, 0, 3, 'Krishnapal Singh Thakur', '7772900912', 'krish_niceguy13@yahoo.co.in', '123456', '', '', 'eoPle5Bf_2w:APA91bHDi4XJ2mbq8lm63EVQ-a2p8DZc5kqS3KkDLjn-uy4QJGuXhW8GqRblKJzeE3jCdVXc6Yiuk1S1wp_G2kHtGvCV2w8-i6Oqgfld_qTOvwz0gblkdm5I2_GtpzPbzGO5iLnNIEe6', 'Android', 1, 0, '', '599767', 'Active', 1506678918, 0),
(67, 0, 3, 'mr pramod jain', '9987654321', 'testuser3@mailinator.com', '123456', '', '1515159813.png', 'fcuQ7qPrayI:APA91bGbg5Z26-8dkPfqi-PYMevGasU9nlTMB32HfNJ5TVeRfuokMoEs0_xSDz-7cngjm8xzOA7u-A2k428dwVGeZVoKOYDJwSuhZe-T9kYmVZiV2zntT2iM8m_uyayBgH6vbATZ4cHj', 'iOS', 1, 0, '', '962483', 'Active', 1506680457, 1515159813),
(71, 0, 3, 'demo', '9876543210', 'demo@s.com', '123456', '', '1519308958.png', '', 'Android', 1, 0, '', '439916', 'Active', 1507100972, 1519308958),
(72, 0, 3, 'suraj palsania', '9981462821', 'suraj.palsania@consagous.com', '123456', '', '', '', 'Android', 1, 0, '', '689648', 'Active', 1507190810, 0),
(86, 0, 3, 'raghu kumar', '1112223334', 'rk@consagous.com', '123456', '', '', '', '', 1, 0, '', '439862', 'Active', 1507212788, 0),
(87, 0, 3, 'uncle sam', '1234567890', 'santa@gmail.com ', '667618', '', '', '', '', 1, 0, '', '', 'Active', 1507212956, 1507271650),
(115, 0, 3, 'Test User1', '9988765432', 'testuser1@mailinator.com', '123456', '', '1508822907.png', '', 'Android', 1, 0, '', '670004', 'Active', 1508743070, 1508822907),
(117, 0, 3, 'swsalman', '9806910910', 's@s.com', '123456', '', '1509716593.png', 'c6cPQ-1NGiU:APA91bG7z8BwvRVxHyFd7TA8Ogt5CihUfNqPWpbSXjM8OqdDA2Km1hGbv-tK-2S-rxeT0yTTbv5RorSmosp1cJ0gi0s0XL2kx1MvyVjIK7WmdB_eKMl5CSfJ_qybfCDbz_7n6TmS_4-Z', 'Android', 1, 0, '', '924264', 'Active', 1508744866, 1510753271),
(184, 0, 3, 'suraj', '1234555555', 'sss@gmail.com', '123456', '', '1508845124.png', 'cedaOeCNvKE:APA91bGSVgkQFlRFWWxv2VxrXPbg4_tD1ialxUNd3jsc6PkUC-hjZ1DkYVL9OcDsdpGIqzPbe3gfPmtaFu3ZMSOL5INBthO4xxYN3qZqeJj_J8ZiFHJ0BeDaBOlCG-E57E3qUUq5yOkI', '', 1, 0, '', '123456', 'Active', 1508844976, 1508845124),
(189, 0, 3, 'suraj palsania', '4567891233', 'surajpalsania@gmail.com', '123456', '', '', '', 'Android', 1, 0, '', '123456', 'Active', 1508850671, 0),
(192, 0, 3, 'a', '9876532140', 'a@l.com', '123456', '', '', '', '', 1, 0, '', '123456', 'Active', 1508910014, 0),
(193, 0, 3, 's', '1234695870', 's@p.com', '123456', '', '', '', '', 1, 0, '', '123456', 'Active', 1508910300, 0),
(194, 0, 3, 's', '9865471230', 's@o.com', '123456', '', '', '', '', 1, 0, '', '123456', 'Active', 1508910457, 0),
(195, 0, 3, 'f', '7896541230', 'f@d.com', '123456', '', '', '', '', 1, 0, '', '123456', 'Active', 1508910534, 0),
(196, 0, 3, 'm', '1256437908', 'm@s.com', '123456', '', '', '', '', 1, 0, '', '123456', 'Active', 1508910633, 0),
(197, 0, 3, 's', '9865654712', 'sk@k.com', '123456', '', '', '', '', 1, 0, '', '123456', 'Active', 1508910873, 0),
(198, 0, 3, 'acc', '6985321475', 's@c.com', '123456', '', '', '', '', 1, 0, '', '123456', 'Active', 1508911220, 0),
(199, 0, 3, 's', '9865742315', 's@u.com', '123456', '', '', '', '', 1, 0, '', '123456', 'Active', 1508911348, 0),
(200, 0, 3, 'd', '9632558714', 'd@d.com', '123456', '', '', '', '', 1, 0, '', '123456', 'Active', 1508911498, 0),
(201, 0, 3, 'j', '6985326544', 'j@jc.com', '1234568', '', '', 'fOrWFafkG4k:APA91bGtw2zlMe1CFjuFKVeP01578UY25HTIXNCfwB--WMYMDdljt1bfgSPs8Z6bWiaz63kzLiP9ThhVQNmI-San3fr3ZoeN9zxAo2_qj8TqIUReDBNu2rEKhwljfinHnX6mD5r2By5I', '', 1, 0, '', '123456', 'Active', 1508911697, 1508912154),
(202, 0, 3, 'Suraj', '7894561237', 'test@gmail.com', '260057', '', '1508918331.png', '', '', 1, 0, '', '123456', 'Active', 1508917194, 1508918331),
(208, 0, 3, 'Suraj', '1234567899', 'testaa@gmail.com', '779328', '', '1508923997.png', '', '', 1, 0, '', '123456', 'Active', 1508923900, 1508923997),
(213, 0, 3, 'suraj', '7897897890', 'stt@gmail.com', '1234567', '', '1508930178.png', '', '', 1, 0, '', '123456', 'Active', 1508930036, 1508930178),
(214, 0, 3, 'ggkj', '4566544563', 'gggh@bb.com', '740103', '', '1508930296.png', 'cXyq-DmM7iY:APA91bHB3Fo0etVF0uJJGAUS63am0bt8JQuP_j3deGY1LtuLqCG5cuANpCNTstbPBAR66J70HtvYnR7UHyYa1cBDSE5W0EnbLcL9BcKTPNhNSM9dHQFPLaSOS3zCcieg725rM11aNW2A', '', 1, 0, '', '123456', 'Active', 1508930250, 1508930296),
(215, 0, 3, 'gsjdj', '3232323232', 'sff@gmail.com', '450876', '', '', '', '', 1, 0, '', '123456', 'Active', 1508931297, 1508931340),
(216, 0, 3, 'jxohxjx', '4545454545', 'sxx@gmail.com', '778410', '', '', '', '', 1, 0, '', '123456', 'Active', 1508931450, 0),
(217, 0, 3, 'hdhjdjd', '5656565656', 'gsgds@gmail.com', '268372', '', '', '', '', 1, 0, '', '123456', 'Active', 1508931724, 0),
(218, 0, 3, 'hfjdj', '1212121212', 'sgzgd@gmail.com', '600524', '', '', '', '', 1, 0, '', '123456', 'Active', 1508931906, 0),
(219, 0, 3, 'as', '3131313131', 's@klk.com', '123456', '', '1508936887.png', '', '', 1, 0, '', '123456', 'Active', 1508932293, 1508936887),
(220, 0, 3, 'hshjd', '7878787878', 'gzgg@gmail.com', '136641', '', '', '', '', 1, 0, '', '123456', 'Active', 1508934158, 0),
(221, 0, 3, 'hdjdjk', '1599511599', 's45@gmail.com', '290497', '', '', 'cnKItba_DCs:APA91bERb2OD3xftdJ6PNcgGBBYLk5hIEISA3d_kNn9e94R5RVq43ayq1p6y0VGZYTTaBjwp8-jDtLz3DM3qOD-cfzlecA43dKIhggY-nKMy8wJLpSZZajpi7czikZxiD5RzWZM1paJp', '', 1, 0, '', '123456', 'Active', 1508934363, 0),
(223, 0, 3, 'sss', '9191919191', 'ss@k.com', '123456', '', '', '', '', 1, 0, '', '123456', 'Active', 1509344480, 0),
(224, 0, 3, 'Manuel Enrique', '8097475092', 'mebautistad@hotmail.com', '123456', '', '1509650670.png', 'fT88XDYUEqo:APA91bE6HxPVbz4D3h1ZC1uQIOcIXECRZom2ZA7Uyo_XnTo7kvpGGxVYQUWsOLysv-iqdswXO69q853peFh9SJv81AuR5ERaPBq1m_owqfbSkaMtPXUlMijun74k8cpVnBd6AKRmzS7R', 'Android', 1, 0, '', '123456', 'Active', 1509380912, 1523986812),
(225, 0, 3, 'fggh', '9522746245', 'az@gmail.com', 'qwerty', '', '', '', '', 1, 0, '', '123456', 'Active', 1509692344, 0),
(226, 0, 3, 'vsabchdah as hbbahxb ash bash ', '4444445590', 's88@gmail.com', '123456', '', '', 'fm72bxcmG9g:APA91bGOA3tVWriEAHcEqA-IKezq9Wt5iSDQTsU-_IRSKuwmBoyyEUGFZzsZy_p2popcDyEMzyS_WLb3RLoYqhPojqjcnpCvMmCgEVdYxOpjfDlwQ8aOLFm3iALhVgwD92GLJf4yyOI3', '', 1, 0, '', '123456', 'Active', 1509692962, 0),
(230, 0, 3, 'Manu Enrique IPhone ', '8492076377', 'enrique_jr3@hotmail.com', '123456', '', '1516234847.png', 'ceRJstCdHMw:APA91bHIICQWCx-4SNFuvgXUSuOfO66h3AzXTQIFByii1fPhGAyWt7W2m6Te6AvtLfHP2du6zk63x73TCDMwDADIizo-twvW_awa8o-MRss2lA04u18HH5FK3REwM1OmrZjdGjXsPdRB', 'iOS', 1, 0, '', '123456', 'Active', 1509723723, 1516234847),
(235, 0, 2, 'apollo hospital indore', '0731 244 5566', 'apollo@gmail.com', '123456', '', '1510048250.jpg', '', '', 1, 0, '', '', 'Active', 1510048250, 1510048250),
(236, 0, 3, '', '9826645789', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1510724324, 0),
(237, 0, 3, 'Suraj', '9879879879', 'spals@example.com', '123456', '', '1519297298.png', 'eTsNMjDnV0M:APA91bHIwnwAeKqH187m_oH6moQQWekYArEL9ONOcaPThzVZqk7fY38gjHfNCEFOe-vtQde35DgAUTUUNCFKhF8dUlnGEOSAzLeXBnKX9pqM2eMAxN--63fnSEWzi710p_nAlffayHW1', 'iOS', 1, 0, '', '123456', 'Active', 1510750419, 1519297298),
(238, 0, 3, '', '9865321479', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1510750604, 0),
(239, 0, 3, '', '4848139948', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1510750719, 0),
(240, 0, 3, 'Marshal', '6546546546', 'mpal@example.com', 'qwerty', '', '1510755347.png', 'foewZPjNIuk:APA91bFfo7KnhkCLmUXkmhTbQSDKi0sjXugEDExXHLCzgeC1_ARxDqIBp_vYSBB_STeFw3m-3n_OYe3KvxHHX8HU2ifwW2eQ9FWA0O6SbKAKGIEJiuH-TWWM6LlXMyPL_6ky7Wud9OOW', 'Android', 1, 0, '', '123456', 'Active', 1510754893, 1510755347),
(241, 0, 2, 'Medanta', 'm,dfgggflglf`', 'medanta@gmail.com', '123456', '', '1511519331.jpg', '', '', 1, 0, '', '', 'Active', 1510921194, 1511519331),
(242, 0, 3, 'maverick', '3693693699', 'mav@ex.com', '123456789', '', '1510922966.png', 'focvMoc_SM8:APA91bGqciYbP-JNa7UFbpQ9K5lf2eo-HPe3Y_CBs2YRRoDf_tkFsJ7UOlVNVxE3hYpNu_kANZ0sibQQ8yiO193Gt-JPV3LC3wYz7CaZ-pRWuQqxnP2LHMhSbsPJMg52M1n_seg2QlHh', 'Android', 1, 0, '', '123456', 'Active', 1510922909, 1510922966),
(243, 0, 3, '', '9806910910', '', '', '', '', '', '', 1, 0, '', '', 'Active', 0, 0),
(244, 0, 3, '', '7854848454', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1511268036, 0),
(245, 0, 3, '', '5784848848', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1511268049, 0),
(246, 0, 3, 'bhanu', '8989642549', 'bhanu@text.com', '123456', '', '', 'cubL_JhSG58:APA91bHcxtH8EOkfH_3mA3JRQ-e6gP7dj9kOef9MAFCuxvZUni8UnOR-vzc06xEg8h2EBGjUD9Q18O6LWVrVdIDNBXyglqCxtsLFhJXsC7vPKVpMJ9h4yRAmN_n2qJJPZz44CpHgF6nD', 'Android', 1, 0, '', '123456', 'Active', 1511950717, 0),
(247, 0, 3, 'trinamuguerza', '8293050527', 'trinagnm@gmail.com', '123456', '', '', 'fsg47jkAiks:APA91bHWoaoY8FQlTCMMeum6wDDpvHdbGPN16GnZSaiQgfrWJigpHqrPJSbiL8TIAHTfqEYNTGIFZ2k6DlfHzFk1ngQVzMaSsFwmyujZweKtiOLrh7PdsBFFto-vbcVV9DXcftr6PA7x', 'Android', 1, 0, '', '123456', 'Active', 1513803548, 1513806367),
(248, 0, 3, '', '99865328965', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1513856924, 0),
(249, 0, 3, 'jaiswal', '9753593517', '', '1566618504', '', '', '', '', 1, 0, '', '1566618504', 'Active', 1513857086, 1513857086),
(250, 0, 3, '', '9584727775', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1514878157, 0),
(251, 0, 3, '', '9179608554', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1515069904, 0),
(253, 0, 3, 'anil', '9691234310', 'anil.consa@gmail.com', '123456', '', '1515229109.png', '', 'Android', 1, 0, '', '123456', 'Active', 1515137261, 1515229109),
(254, 0, 3, 'amit', '9522587507', 'amil@gmail.com', '123456', '', '1515390761.png', 'c4JI_ZkSWNU:APA91bFvntD1bq96ArSdsBFvH7V86wSzelRQHRNU41XeEyqAhf8ei4_0KmD1aesVR9T2uFaOgomnwVxkvfenoyXIWpX3uHlS6VWhVY0gJxwWUBZGT25vpWuoL4oP1FnL-OaGmbq0M8IR', 'Android', 1, 0, '', '123456', 'Active', 1515138566, 1515390761),
(255, 0, 3, 'Te', '9998765432', 'test@test.com', '123456', '', '1515569698.png', '', 'iOS', 1, 0, '', '123456', 'Active', 1515214111, 1515569698),
(256, 0, 3, 'Pedro Manuel Bautista Mendez', '8097238649', 'mrbautista26@hotmail.com', '123456', '', '', 'd1JfWL_97aQ:APA91bFKO6SPad_ZO03sbdiBlWpuaFF9qe0ZLdSKZUcJIwR6dP1MOhX62PNn2JMKczJfYYS3i81kHz1_RUR6kVD0vFbvpMURh6DhHjoMVs7xx8b6cJ1I-wILPusVr83jkGBNqru48F94', 'Android', 1, 0, '', '123456', 'Active', 1515261171, 1515456427),
(257, 0, 3, 'Rene Osvaldo Bautista Dominguez', '8097090759', 'osvaldo_bdd@hotmail.com', '538405', '', '1515261453.', '', '', 1, 0, '', '', 'Active', 1515261453, 1515261453),
(258, 0, 3, 'Anuj', '9907470198', 'anuj@consagous.com', '123456', '', '', '', 'iOS', 1, 0, '', '123456', 'Active', 1515482492, 0),
(259, 0, 3, '', '7987160473', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1515483916, 0),
(260, 0, 3, '', '9630396996', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1515493505, 0),
(261, 0, 3, '', '8434976497', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1515497410, 0),
(262, 0, 3, 'anuj', '78945612345', '', '443694359', '', '', '', '', 1, 0, '', '443694359', 'Active', 1516082131, 1516082131),
(263, 0, 3, 'sarah', '9392445319', 'sarahdguez98@gmail.com', 'p1rates1', '', '', 'cZzix8tRHyM:APA91bFcsZp2ZgWiOg_BzcrI38wSwuposegKZdy66VpbHDH31k3-fhBBY0OJ7BFOjfyNAUGN3VoEiE7BM_Eo308_9i2PP3SfquP5qqtkldyHfW8vqAo60iEJbxKDhZkkmgyJLY7sNw6u', 'Android', 1, 0, '', '123456', 'Active', 1518803833, 0),
(264, 0, 3, '', '8319910537', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1519218742, 0),
(265, 0, 3, '', '9827091479', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1519219218, 0),
(266, 0, 3, 'anil', '9876543211', 'anil.cona@gmail.com', '123456', '', '1519304571.png', '', 'Android', 1, 0, '', '123456', 'Active', 1519304436, 1519304571),
(267, 0, 3, '', '53453453534', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1519392554, 0),
(268, 0, 3, '', '3454354443', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1519392836, 0),
(269, 0, 3, '', '5435345345', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1519550190, 0),
(270, 0, 3, '', '34534435345', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1519551517, 0),
(271, 0, 3, '', '2342342344', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1519551798, 0),
(272, 0, 3, '', '8099645359', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1519661574, 0),
(273, 0, 3, '', '9879877978', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520234288, 0),
(274, 0, 3, '', '9876595656', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520234563, 0),
(275, 0, 3, '', '9691234311', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520499024, 0),
(276, 0, 3, '', '8080808080', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520499259, 0),
(277, 0, 3, '', '9999876543', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520565417, 0),
(278, 0, 3, '', '542481815133', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520594939, 0),
(279, 0, 3, '', '9856562062', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520686205, 0),
(280, 0, 3, '', '9876454655', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520687122, 0),
(281, 0, 3, '', '9874897845', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520687222, 0),
(282, 0, 3, '', '9878944564', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520687290, 0),
(283, 0, 3, '', '9877897847', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520687327, 0),
(284, 0, 3, '', '9877889789', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520687495, 0),
(285, 0, 3, '', '9858648454', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520687653, 0),
(286, 0, 3, '', '9525962626', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520687741, 0),
(287, 0, 3, '', '9889787845', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520687957, 0),
(288, 0, 3, '', '8798765465', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520688019, 0),
(289, 0, 3, '', '9897897984', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520688615, 0),
(290, 0, 3, 'aman', '9000030000', 'aman.jain@consagous.com', '123456', '', '', '', 'Android', 1, 0, '', '123456', 'Active', 1520845238, 0),
(291, 0, 3, 'john', '8992076377', '', '372594604', '', '', '', '', 1, 0, '', '372594604', 'Active', 1520855433, 1520855433),
(292, 0, 3, 'Kaushalendra', '9826966016', '', '482138434', '', '', '', '', 1, 0, '', '482138434', 'Active', 1520946924, 1520946924),
(293, 0, 3, '', '9835345464', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520996737, 0),
(294, 0, 3, '', '5683294659', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520996902, 0),
(295, 0, 3, '', '9968574321', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520997715, 0),
(296, 0, 3, '', '9965874321', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1520997923, 0),
(297, 0, 3, '', '5863265554', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1521023622, 0),
(298, 0, 3, '', '1231231231', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1521095767, 0),
(299, 0, 3, 'ios user', '9876556789', 'ios@ios.com', '123456', '', '', '', 'iOS', 1, 0, '', '', 'Active', 1506412952, 0),
(300, 0, 3, 'ios user', '9876556789', 'ios@ios.com', '123456', '', '', '', '', 1, 0, '', '', 'Active', 1506412952, 0),
(301, 0, 3, 'Fly Flyerson ', '4084066195', 'flyflyerson@gmail.com', 'Apple123', '', '', 'dLYL58lX4gs:APA91bFhSol7W4UEwAmGN2PhIabYm1IM1DQ-n7UqPBDOhi4iKuJRRJwFcDqG5m-YY4mJfVF2oMHbhOatWI1UIBUyviSQ_ZF3Ok8NLxnLKfsSxkUvYeOuDvSM_0JcynWUSubVREAxPTew', 'iOS', 1, 0, '', '123456', 'Active', 1521581130, 0),
(302, 0, 3, 'DomingoValdez', '8097473309', 'dvaldez01@gmail.com', '12345678', '', '', 'ekST12IlNEo:APA91bF50KiGNvpkKjtM3RZUMU15fuNRZziJKBpcrwroq5c75hwjijUcplrJiNSVNtYMMKHpPPgNhVue0hrj0Z02drv1MybFE-QLpkXRv7Qgk7tG52Q_dGzgOa3vgHozyT6nM3GOfMVo', 'Android', 1, 0, '', '123456', 'Active', 1521634038, 0),
(303, 0, 3, 'Mario Rafael Gutirrez', '8294190386', 'mario-rafaelgutierrez@hotmail.com', 'mario0386', '', '', '', 'Android', 1, 0, '', '123456', 'Active', 1521637506, 0),
(304, 0, 3, 'Francisco', '8098642007', 'fragy.895@gmail.com', '123456', '', '', 'ceK36yzLnUY:APA91bEd46M_SY8BVoGuVRNQZmjBfIoGE5TAdDptpGL8EqliYAxtIzRsomUF4ZnDYJ_nD2_hUIqGTrCW4Shivkv5UXFKoYkybaeR1ycuXgERJRa8TGh9pKea0IDpVLLZR9eOOoFBJ8cw', 'Android', 1, 0, '', '123456', 'Active', 1521649670, 0),
(305, 0, 3, 'percyperez', '8095125789', 'percyperezm@gmail.com', 'martinez18', '', '', 'cDsJJIxfgNc:APA91bHUcDNXMC9J3rqRRNxysb_uK5clqptpAXAug6yZR3NOBdgrSa7DbLl1z8xFfe4A3vs_BG9QXkTY6wO_zvDgqSPT41IWVxkc4om47HIonX49IQfKTGTlnGTkh4jsBAHSc8yXN-0p', 'Android', 1, 0, '', '123456', 'Active', 1521650574, 0),
(306, 0, 3, '', '99968532565', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1521716470, 0),
(307, 0, 3, '', '9638527410', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1521716542, 0),
(308, 0, 3, '', '9654789521', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1521716695, 0),
(309, 0, 3, '', '8888888886', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1521818668, 0),
(310, 0, 3, '', '1566666666', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1521895164, 0),
(311, 0, 3, '', '8962122866', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1523532204, 0),
(312, 0, 3, '', '8962122896', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1523532288, 0),
(313, 0, 3, 'test', '14084066195', 'tes@Test.com', 'apple123', '', '', 'ctOqhncD5Bc:APA91bEnEBWIfEzekc0Ji_Wrm2tYD4uJMNnws_ck5wqAaKxDadPfI9GN02qTSwidoxfKqFksWFS04OAyoAZZasGiCUICIK96n9j80cxE8_7CVcHyXw86eKjMBSjUhr9GkT-368rpc2E3', 'iOS', 1, 0, '', '123456', 'Active', 1524245860, 0),
(314, 0, 3, 'sgfja', '7033893858', 'nazgulnone@yahoo.com', 'Forthelichking', '', '', '', 'Android', 1, 0, '', '123456', 'Active', 1524712020, 0),
(315, 0, 3, 'Silma Peralta Pena', '8099795934', 'silmap11@gmail.com', '.1122.', '', '', 'dUiLPpMoGnQ:APA91bE_ecZZuD0psmODLCcc9sH88EbPDv85wt-FhHNVr2a38UJv3B51kS207bHFLz3-h-53_Qn2THp5uKtAIqZAP68D3Sb-gUzxDjK2oPbrV4ibHlM3AmF08ef6BD45wo5H2BS20y9r', 'Android', 1, 0, '', '123456', 'Active', 1525125911, 0),
(316, 0, 2, 'Homs', '89924189090', 'Silmap11@gmail.com', '123456', '', '1525127603.jpeg', '', '', 1, 0, '', '', 'Active', 1525127543, 1525127618),
(317, 0, 3, 'Jhonattan Elias Valerio Vargas', '8492549023', 'valerio_elias22@hotmail.com', 'lilwayne01', '', '1525271618.png', 'fHK-oSMrp3A:APA91bFfDuTVWWJ3ymcsKgTtv6VBBU5QySoG4BsvWexAOD2G5mcMSUwFn6pbUI9NhX4P5SW23RoX8evQlPOOXrHZQEP_19_1vmU2I2ZTIkv3vR3K3BZSjjm9l4ZqAsIdwKoOucIKjTDn', 'iOS', 1, 0, '', '123456', 'Active', 1525271455, 1525271618),
(318, 0, 3, 'Francis R', '7328299967', 'francisrrivas@yahoo.com', '1dtpunsw5', '', '', 'eyFRWfy7_-s:APA91bEqfc1-_c7lIqIjDmZRTYc3_MD3VJdcP39hsaB2zaHzEnW_87diohXETVipkNs8whuEnOG01Uv1wj_rkKg21o6at3etOokDtO7WcDCW7e8q2TM5KXfG0tranK22f1WIEVXHSgXI', 'iOS', 1, 0, '', '123456', 'Active', 1525369106, 0),
(319, 0, 3, 'Rubén Dario alvarez Peralta ', '8099865269', 'ellove_525@hotmail.com', 'love986', '', '', 'cFIW32R3dnk:APA91bHCsF9StoDqvy0um7C9SFbPJapY1s-TPiZUDOMiRj-eYmiON_ImNZsNAGaG3DHReclJUwx5XXOPAT2UHHqSij7lnH6iul-8S_2c9WuNlpmX2KPphF86ipqwO6comZzYTV4aDhZT', 'iOS', 1, 0, '', '123456', 'Active', 1525370756, 0),
(320, 0, 3, 'Carolina Gomez Herrera', '8296391202', 'cgomezh@hotmail.com', 'carolina20120361', '', '', 'dMoZZFfQXLU:APA91bF_4QwI3oHeNOURqfm3gcwW3wq_LIAiAZTpkU29RPqRs7us9qJ5wIN7EsSxZpPHTw96foG3x6NhTX3ALXta4eQnLI8Dfnduadrjk99_eIpv01zGFxlQjW9uyZYJ-Xq49OIRGdYB', 'Android', 1, 0, '', '123456', 'Active', 1525371819, 0),
(321, 0, 3, '', '8297630700', '', '', '', '', '', '', 1, 0, '', '123456', 'inActive', 1525373245, 0),
(322, 0, 3, 'Melvin Cruz Dominguez', '8096534652', 'Melvin_cruz14@hotmail.com', '123456', '', '1526061653.png', 'd4MFFpb0hMA:APA91bG3IFc29MRna9XncthMWwLFjexRVR4pT2RvqETAgvNo-UNqvINfZqUA_HHfqVNsWRIA28m4-lGkZMQT9nQbEBwNVkvtXo22b_b6jg8Iicuj6IfSzIOVbv8LuSq5hzfw6rD6JE4j', 'iOS', 1, 0, '', '123456', 'Active', 1526061425, 1526061653),
(323, 0, 3, 'Genesis Altagracia Gonzalez Tineo', '8295432820', 'gonzalezrosmery03@gmail.com', 'genesisgonzalez', '', '', 'cIjx7rAge3w:APA91bElFeKq8Uf44LIJ5l55-UPDL5cxLaTOYmSZaBJ1YXinWmresAi5Yb85fU88_pDhNildsuQpUdKMKftufD5cB75akHebtGy0WVWpDCp9QvFDQZoQh9wSoJGypTNMpmUlNtNKQsCD', 'Android', 1, 0, '', '123456', 'Active', 1526306244, 0),
(324, 0, 3, 'isaurys torres', '8297446280', 'sterlyn1@outlook.com', 'Tekieromucho13', '', '', '', 'Android', 1, 0, '', '123456', 'Active', 1526307706, 0),
(325, 0, 2, 'Homs', '8299472222', 'consultorio311@homs.com.do', '123456', '', '1526309868.png', '', '', 1, 0, '', '', 'Active', 1526309868, 1526309896),
(326, 0, 3, 'Franz Zierlein', '0050904438', 'zierleinf@gmail.com', 'enero2418', '', '', 'czMrmY57ac8:APA91bHx_OVDd17-zQT8lrJoC6v8IRxYP8OAKQfSMGZqjsTWRvCdZp173snJO7WDSxeALbTae08tLfOgBo7nFUyOG--YBIW2W43n6fSqo6xoI9WpeGSkM9gCoGdB2rwJw5hEbIBbRnu9', 'Android', 1, 0, '', '123456', 'Active', 1526997329, 0),
(327, 0, 3, 'Zuleika Veras Fernande', '8097055592', 'z_veras23@hotmail.com', '456123', '', '', 'fChEYHjHU90:APA91bFlFNuOQa9WAbImyKQ70vfH0cEQCGcdxpuPWjpjS_ePcfFl6yG_HJaPFi2xrKdCdiPl7WIVTF_5z0uN98VqSangadjhdujqp_e__8nUmXQ9j_OfHYT9AxW1wBYdv-j_wo1faWjs', 'iOS', 1, 0, '', '123456', 'Active', 1527044335, 0),
(328, 0, 3, 'Denia Cabrera', '8492073328', 'dniacabreraq@hotmail.com', 'jeanmarco', '', '', 'cTpUhXIdeI0:APA91bF81vUvV-K3TDdaRilr1cu0Am246jtyHTO1-2ZXTekq8DQMc3SV1iR1yS_IbG-BiGWlKm5hnSxPRSJpQZpMVYfrgOkF5gaSIdwjwN9fyBaTBsmVW58doE9wMF9QPIGG8bHTKCtV', 'iOS', 1, 0, '', '123456', 'Active', 1527046965, 0),
(329, 0, 3, 'Juan Polanco', '7873448605', 'akuneli@gmail.com', '123456', '', '', 'cJ4OcyCrFmo:APA91bGFOKuU6aJ5NYFX0x68eqsC7bo1OrQkufNHwZJeoz4__WcNu-loHrUv9SX_DTElISE1n28a35WxNYYVtx8sbI7_EPkgRBqBYF01wgGfXCkUlx-0fYt-3Bu6SJJuqFLvZaW3uD3K', 'Android', 1, 0, '', '123456', 'Active', 1527174712, 0),
(330, 0, 3, 'Karina ', '8297129260', 'kittycat027@gmail.com', 'kassey08', '', '', 'fGi8jnm4N3k:APA91bGUnLWNKvVngcjhWW9LTKwhCVkQHlmd4NDgTGscpxEYcx5O_iK6pyvSIG6K9vaeVEyi-QN4r1SN0zYqBxjUCIK1YUTQ0J-ty6d3Kh68wJhPoB89QtEqklp2oJ1lnUBOyvY4jF_D', 'iOS', 1, 0, '', '123456', 'Active', 1527302248, 0),
(331, 0, 3, 'Pedrogabriel', '8099959731', 'pedrogabrielestevez@gmail.com', 'papalolo', '', '', 'c9IrceYpWaA:APA91bEGWq06oE5aEhGy-7Kc4FZwRGOwJOzCfSo81N-gboLnI4FldxqRRBjLf8hxzed9R4DEW4SmDDsKBRYGEpLo9IWCS1c9BC7Zw_w6Y7qhpdkIULgwYf3oTIm7JQ0UMs6D3viK-Mf8', 'iOS', 1, 0, '', '123456', 'Active', 1528175811, 0),
(332, 0, 3, 'manuela estevez', '8097477756', 'manuelaestevez00@hotmail.com', 'corazon21', '', '', '', 'iOS', 1, 0, '', '123456', 'Active', 1528259385, 0),
(333, 0, 3, 'Alicia Estévez', '8294569700', 'aliciaestevez1309@gmail.com', '121314', '', '', 'dySssYgkads:APA91bE9wZRpLWvE-HQX_W5RKpoT5g0hkLmtcXHQJoAkgwc7he1r66l5xEBM1z2Ue6erPNHBqd75d3ebEbtWIkPLa6R2dFqKaqN0Z6yud5VgTORp5biayYkCyLr2jHSX_cHf9X-V7-PH', 'iOS', 1, 0, '', '123456', 'Active', 1528393025, 0),
(334, 0, 2, 'HOMS', '8092109090', 'pestevez@gmail.com', '123456', '', '1528743780.jpeg', '', '', 1, 0, '', '', 'Active', 1528743780, 1528743780),
(335, 0, 3, 'soraya Peralta', '8299741023', 'soraya_peralta_13@hotmail.com', 'sory13', '', '', 'd_fXljsV0HE:APA91bHyE07ng2Kq32U8SIiQKGw0CrlhPa_ezsRfqSCAOpQj8czlTFAaeXFO1NFtJARbAf6u4NxsIv3_eUhSQbw4Nme7Y1-KmtIiU5atMDLmOCVQiCHMun28REo0MpL6zFRrFYfvYf6ML4v8m7Lnhnjk2O5Gt5jjYw', 'iOS', 1, 0, '', '123456', 'Active', 1529449048, 0),
(336, 0, 3, 'luisalmanzar ', '8292682932', 'luima_23@hotmail.com', 'jorgeluis', '', '', 'cvPC5hOZLkk:APA91bG8fahIVKjDZWWrIYVqXyq29eIgQEhEl1-yP3fDOaMY4xcdPcGB4PiFyraJOT5zpCO_v29LPB0PzFFI-6skzlOKX5DeVwefhWCyPGWHOv_lJNUee3ss9hvUlYq-uGr-QDfr9HLlhLCT11Uqkdi-OCxYXcURXg', 'iOS', 1, 0, '', '123456', 'Active', 1530048459, 0),
(337, 0, 3, 'Steffany Cerda', '8492207188', 'steffanycerda@hotmail.com', 'beba26', '', '', 'c3bjFej-LzE:APA91bE1qa0drrY_LJhXne_PvKxukl8ExezAisORs31eYsQDuk2qyRw1mIASnnwe9fu-77a7IUu_1ctlw-y5kqOT0bfD74S9ZRm6SQOmQdJtljGZSPQO7HtSepuXd_WD_LzxvjQmbsMsa0oJbwRNjSdpgqVqGxyH6w', 'iOS', 1, 0, '', '123456', 'Active', 1530111569, 0),
(338, 0, 3, 'Gilbert Jesus Collado', '8496341484', 'glbrtclld@gmail.com', 'naramango105', '', '', 'cFCWYTh2CiE:APA91bG_zrQruVi6YeT0Wb9exOV-NecdkcXkPXvUFpyhsLXWLcBrJuf4XXnymkNSZzWDY5qf0uy26b7mOmqcP6HgCq5mK6DEWJKeqTEpWAs_KNYTLHbZGdwNR4Tot12zGSVfLPmt2z6wlGiSotw7MFXnQDvx2BBdJg', 'iOS', 1, 0, '', '123456', 'Active', 1530224274, 0),
(339, 0, 3, 'brady de las nueces', '8299175245', 'bdln20@gmail.com', 'bradywashere94', '', '', 'eLz-eEn1nb4:APA91bEVKWP4E0fLufwCSHncvLmjX81hwZTYUNeGaVsChbIfKB6dzGRLmA3aLX-nj5HNrosOPemp7sWsZFp4sgD_wntcoRv3btaMj5FWyed1VRFNfLZ6_XytLv8ySCjpuELJYFN4YT89gasXqWoX1tJqb1zOzX3_hA', 'iOS', 1, 0, '', '123456', 'Active', 1531935507, 0),
(340, 0, 3, 'Jose Omar Marte Espinal', '8295806367', 'joseomarmarteespinal2@gmail.com', '30311309', '', '', 'e2cirbS3BjM:APA91bGq0vYwPn3ynTK3cT6w7feaMPW7m6CjFi5XBF32wNYManHKdQJGvxmP8LdFyu7ioid9QJnVap-IqG9n1VKa1c1JqVGw0lnDMalXhWOlpVhpMtOM0AxXn9NviQL5Q-7_aBqAxTmazHyNC5v29JwwqBia9wx3Jw', 'Android', 1, 0, '', '123456', 'Active', 1532380252, 0),
(341, 0, 3, 'Carolina Crush', '03104859479', 'crush@hotmail.com', '123456', '', '1532612569.', '', '', 1, 0, '', '', 'Active', 1532612569, 1532612569),
(342, 0, 3, 'Juan Perez', '03104859478', '', '563269604', '', '', '', '', 1, 0, '', '563269604', 'Active', 1532962357, 1532962357),
(343, 0, 3, 'Ivan Dario', '40223559754', '', '952411522', '', '', '', '', 1, 0, '', '952411522', 'Active', 1533055510, 1533055510),
(344, 0, 3, 'Ms. Toribio', '8296656839', '', '1964744199', '', '', '', '', 1, 0, '', '1964744199', 'Active', 1533909889, 1533909889),
(345, 0, 3, 'Juan', 'sdfsadfafxdf', '', '2114877701', '', '', '', '', 1, 0, '', '2114877701', 'Active', 1535134102, 1535134102),
(346, 0, 3, 'juan pablo duarte', 'sdfsadfafdse', '', '1240670162', '', '', '', '', 1, 0, '', '1240670162', 'Active', 1535134199, 1535134199);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'Doctor'),
(3, 'Patient'),
(4, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `user_subscription_plan`
--

CREATE TABLE `user_subscription_plan` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `subscription_start_dt` int(11) NOT NULL,
  `subscription_end_dt` int(11) NOT NULL,
  `create_dt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_subscription_plan`
--

INSERT INTO `user_subscription_plan` (`id`, `user_id`, `subscription_id`, `subscription_start_dt`, `subscription_end_dt`, `create_dt`) VALUES
(1, 57, 2, 1507638582, 1510165800, 1507638582),
(2, 60, 3, 1507638588, 1507638588, 1507638588),
(3, 57, 4, 1510754235, 1510754235, 1510754235),
(4, 60, 4, 1510754241, 1510754241, 1510754241),
(5, 235, 4, 1510754246, 1510754246, 1510754246),
(6, 235, 1, 1511272237, 1512066600, 1511272237),
(7, 60, 2, 1511527568, 1514053800, 1511527568);

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_token`
--

INSERT INTO `user_token` (`id`, `user_id`, `token`) VALUES
(772, 254, '53398794'),
(789, 252, '2055293342'),
(834, 263, '488986968'),
(1094, 61, '1574124878'),
(1221, 256, '6505317'),
(1357, 301, '1291579188'),
(1360, 302, '365893486'),
(1361, 247, '2053645755'),
(1363, 305, '169686458'),
(1364, 304, '2127784253'),
(1403, 224, '1281904631'),
(1406, 246, '574984829'),
(1413, 313, '390997041'),
(1427, 67, '1788067922'),
(1432, 315, '1947648026'),
(1437, 317, '1811629452'),
(1440, 62, '1378833458'),
(1443, 318, '1149115098'),
(1444, 319, '1053576544'),
(1448, 320, '1127390450'),
(1457, 322, '42124086'),
(1459, 323, '1853540957'),
(1472, 326, '1220220365'),
(1473, 327, '1772132860'),
(1474, 328, '284820648'),
(1475, 329, '1016002035'),
(1476, 330, '1167711856'),
(1485, 230, '504413677'),
(1491, 331, '1801364182'),
(1494, 333, '1501596827'),
(1501, 335, '1206084167'),
(1503, 237, '632788356'),
(1508, 336, '494483239'),
(1509, 337, '1549051066'),
(1511, 338, '1588807012'),
(1515, 339, '1929604995'),
(1516, 340, '1384398003'),
(1572, 59, '1655052618'),
(1589, 57, '336751004'),
(1618, 60, '1186304771');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads_associates_doctor`
--
ALTER TABLE `ads_associates_doctor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads_associates_specialties`
--
ALTER TABLE `ads_associates_specialties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads_type`
--
ALTER TABLE `ads_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_user_friend`
--
ALTER TABLE `chat_user_friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_user_message`
--
ALTER TABLE `chat_user_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_user_message_status`
--
ALTER TABLE `chat_user_message_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clinic_doctor_management`
--
ALTER TABLE `clinic_doctor_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clinic_doctor_speciality`
--
ALTER TABLE `clinic_doctor_speciality`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clinic_doctor_time_management`
--
ALTER TABLE `clinic_doctor_time_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clinic_satff_management`
--
ALTER TABLE `clinic_satff_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consultation_association`
--
ALTER TABLE `consultation_association`
  ADD PRIMARY KEY (`associat_id`);

--
-- Indexes for table `consultation_images`
--
ALTER TABLE `consultation_images`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `consultation_info`
--
ALTER TABLE `consultation_info`
  ADD PRIMARY KEY (`consult_id`);

--
-- Indexes for table `consultation_user_info`
--
ALTER TABLE `consultation_user_info`
  ADD PRIMARY KEY (`user_info_id`);

--
-- Indexes for table `doctor_specialties`
--
ALTER TABLE `doctor_specialties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_company`
--
ALTER TABLE `insurance_company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `manage_appointment_status`
--
ALTER TABLE `manage_appointment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manage_doctor_specialties`
--
ALTER TABLE `manage_doctor_specialties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manage_subscription_plan`
--
ALTER TABLE `manage_subscription_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manage_treatment_type`
--
ALTER TABLE `manage_treatment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manage_visit_status`
--
ALTER TABLE `manage_visit_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_doctor_relation`
--
ALTER TABLE `patient_doctor_relation`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `static_content`
--
ALTER TABLE `static_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_subscription_plan`
--
ALTER TABLE `user_subscription_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ads_associates_doctor`
--
ALTER TABLE `ads_associates_doctor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `ads_associates_specialties`
--
ALTER TABLE `ads_associates_specialties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `ads_type`
--
ALTER TABLE `ads_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=316;

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `chat_user_friend`
--
ALTER TABLE `chat_user_friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `chat_user_message`
--
ALTER TABLE `chat_user_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_user_message_status`
--
ALTER TABLE `chat_user_message_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `clinic_doctor_management`
--
ALTER TABLE `clinic_doctor_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `clinic_doctor_speciality`
--
ALTER TABLE `clinic_doctor_speciality`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clinic_doctor_time_management`
--
ALTER TABLE `clinic_doctor_time_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clinic_satff_management`
--
ALTER TABLE `clinic_satff_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `consultation_association`
--
ALTER TABLE `consultation_association`
  MODIFY `associat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `consultation_images`
--
ALTER TABLE `consultation_images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `consultation_info`
--
ALTER TABLE `consultation_info`
  MODIFY `consult_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `consultation_user_info`
--
ALTER TABLE `consultation_user_info`
  MODIFY `user_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `doctor_specialties`
--
ALTER TABLE `doctor_specialties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `insurance_company`
--
ALTER TABLE `insurance_company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `manage_appointment_status`
--
ALTER TABLE `manage_appointment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `manage_doctor_specialties`
--
ALTER TABLE `manage_doctor_specialties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `manage_subscription_plan`
--
ALTER TABLE `manage_subscription_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `manage_treatment_type`
--
ALTER TABLE `manage_treatment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `manage_visit_status`
--
ALTER TABLE `manage_visit_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=505;

--
-- AUTO_INCREMENT for table `patient_doctor_relation`
--
ALTER TABLE `patient_doctor_relation`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `static_content`
--
ALTER TABLE `static_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=347;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_subscription_plan`
--
ALTER TABLE `user_subscription_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1620;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
