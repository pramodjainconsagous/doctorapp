// =================================================================
// get the packages we need ========================================
// =================================================================
var express   = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var session = require('express-session');
var mongoose = require('mongodb');
var ObjectId = require('mongodb').ObjectID;
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file
var crypto = require('crypto');
var multer = require('multer');
var fs = require("fs");
var path = require('path');
//routes

var port = process.env.PORT || 8000; 
//var server = app.listen(port);

app.use(express.static(__dirname)); // set the static files location /public/img will be /img for users
app.use(session({secret: 'secret',saveUninitialized: true,resave: true}));
// =================================================================
// configuration ===================================================
// =================================================================
var http = require('http');
var server = http.createServer(app);
var io = require('socket.io').listen(server);
var usernames = {};
var room = []
var rooms = [];
io.sockets.on('connection', function(socket) {

 socket.on('addUser', function(blockID) {
       // store the room name in the socket session for this client
       socket.room = blockID;
       socket.join(blockID);
       getChatHistory(blockID, function(history) {
        io.sockets.in(blockID).emit('updatechat', history);
        socket.broadcast.to(blockID).emit('updatechat', history);
      });
     });

 socket.on('sendchat', function(data, userName, user_id,receiver_id, block_id) {
   console.log(data, userName, user_id, block_id, block_id);
            // we tell the client to execute 'updatechat' with 4 parameters
            addChatInDatabase(data, userName, user_id,receiver_id, block_id);
 });


 function addChatInDatabase(data, userName, user_id,receiver_id, block_id) {
  var record =
  {
    sender_id : user_id,
    msg_status : "send",
    text : data,
    sendtimestamp :Date.now(),
    recievetimestamp :Date.now(),
    readtimestamp :Date.now(),
    receiver_id : receiver_id,
    chatroom_id : block_id,
    is_deleted : 0
  }
  db.collection('message').insert(record , function(err,rows){
    if(err){
     console.log(err); 
   }else{
    getChatHistory(block_id, function(rowsdata) {
      io.sockets.in(block_id).emit('updatechat', rowsdata);
      socket.broadcast.to(block_id).emit('updatechat', rowsdata);
    });    
  }
})
}

});

socket.on('disconnect', function() {
           // remove the username from global usernames list
           socket.leave(socket.room);
         });


function getChatHistory(block_id, callback) {
  console.log(block_id);
  var result = {};
  db.collection('message').find({"chatroom_id":block_id}).toArray(function(err, rows) {
   if (err) {
     console.log(err);
     callback([]);
   }else{
     callback(rows);
   }
 });
}

// used to create, sign, and verify tokens
mongoose.connect(config.database, function(err,database) {
  console.log(config.database);
  if(err) {
    console.error(err);
  }else{
    db = database;
  } 
});
//mongoose.connect('mongodb://localhost:27017/textapp'); // connect to database
app.set('superSecret', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));
// ---------------------------------------------------------
// get an instance of the router for api routes
// ---------------------------------------------------------
var apiRoutes = express.Router(); 
server.listen(port);
console.log('Magic happens at http://localhost:' + port);